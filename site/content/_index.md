---
url: "index.md"
comments: false
---

MOMO is a multi-objective mixed integer optimisation approach for enumerating knockout reactions leading to the overproduction and/or inhibition of specific compounds in a metabolic network.

### Download link

The MOMO application is distributed as source code and as an AppImage file for the Linux platform.

- [AppImage for x64 Linux](page/download/)
- [Source code](download/momo-src-1.0.0.tar.gz)

### Source code repository (INRIA Forge)

You can download the source code also directly from the Inria FORGE git repository: 

` git clone https://scm.gforge.inria.fr/anonscm/git/momo-sysbio/momo-sysbio.git `
