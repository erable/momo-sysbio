+++
title = "Downloading MOMO"
#outputs = ["PHP"]
+++

MOMO is distributed under the [ZIB Academic License](../../download/zib.txt).
This means that you can download a copy of MOMO for research purposes as a member of a noncommercial and academic institution.

In this page you can download a binary version of the software and all dependencies compiled in a single file.
Just enter your information in the form below and confirm you agree with the license.

{{< php_code >}}

<?php
include '../../php/functions.php'
?>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
<table class="noBorder">
<tr>
  <td class="noBorder">Name:</td> <td class="noBorder"> <input type="text" name="name" value="<?php echo $name;?>"></td>
  <td class="noBorder"><span class="error"> <?php echo $nameErr;?></span></td>
</tr>
<tr>
  <td class="noBorder">E-mail:</td> <td class="noBorder"><input type="text" name="email" value="<?php echo $email;?>"></td>
  <td class="noBorder"><span class="error"> <?php echo $emailErr;?></span></td>
</tr>
<tr>
  <td class="noBorder">Institution:</td> <td class="noBorder"><input type="text" name="institution" value="<?php echo $institution;?>"></td>
  <td class="noBorder"><span class="error"><?php echo $institutionErr;?></span></td>
</tr>
<tr>
  <td class="noBorder">City:</td><td class="noBorder"><input type="text" name="city" value="<?php echo $city;?>"></td>
  <td class="noBorder"><span class="error"> <?php echo $cityErr;?></span></td>
</tr>
<tr>
  <td class="noBorder">Country:</td><td class="noBorder"><input type="text" name="country" value="<?php echo $country;?>"></td>
  <td class="noBorder"><span class="error"> <?php echo $countryErr;?></span></td>
</tr>
<tr>
  <td class="noBorder"><input type="checkBox" name="license" value="license" <?php if ($license == "license") echo "checked";?>></td><td class="noBorder">  I certify that I will use the software only as a member of a noncommercial and academic institute and that I have read and accepted the ZIB Academic License.</td>
  <td class="noBorder"><span class="error"> <?php echo $licenseErr;?></span></td>
</tr>
<tr>
  <td class="noBorder"><input type="submit" name="download" value="Download"></td>
</tr>
</table>
</form>

<?php
if ( $license=="license" &&
     strlen($licenseErr) == 0 &&
     strlen($countryErr) == 0 &&
     strlen($cityErr) == 0 &&
     strlen($institutionErr) == 0 &&
     strlen($emailErr) == 0 &&
     strlen($nameErr) == 0) {

echo "<meta http-equiv=\"refresh\" content=\"3;url=../../download/MOMO-x86_64.AppImage\">";
echo "<p style=\"color:#696969;\">Thank you! The download will start in 3 seconds.</p>";
}
?>

{{< /php_code >}}
