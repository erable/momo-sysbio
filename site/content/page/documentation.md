+++
title = "Documentation"
comments = false
+++
You can download a binary version of MOMO, ready to run in a Linux environment, in our [Download](../download/) section.
Once downloaded, you have to make the file executable: `chmod a+x MOMO-x86_64.AppImage`
And then you can run it through command line:`./MOMO-x86_64.AppImage`

The MOMO software has several running modes that can be used by
passing different combinations of flags and parameters by command
line.

Running `./MOMO-x86_64.AppImage -h` shows the help with all the possible flags:

~~~html
-h [ --help ]                         produce help message. 
-N [ --network-file ] arg             input network file (MANDATORY).
-B [ --bounds-file ] arg              input file with the bounds for the 
-o [ --output-file ] arg (=solutions.txt)
                                      output file.
-t [ --optimizer-timeout ] arg (=3600)
                                      Timeout for the optimization procedure 
                                      (in seconds).
--maximize arg                        Reactions to maximize.
--minimize arg                        Reactions to minimize.
--reactions-to-fix arg                Reactions to fix. Format 
                                      reactionID=flux[,reactionID=flux] 
                                      (useful for the enumeration).
--number-deletions arg (=0)           Consider deletions sets of informed 
                                      size.
--enumerate                           Enumerates deletions. In this case the 
                                      objective functions are ignored.
-D [ --deletions-candidates-file ] arg
                                      input file with the reactions that 
                                      should be considered for deletion.
-R [ --reference-flux-file ] arg      input file with the fluxes that will be
                                     used as reference for the MOMA approach
                                      during enumeration.
-m [ --optimizer-internal-memory ] arg (=2048)
                                      Internal memory used by the optimizer 
                                      before using disk space (in Mb).
~~~

The network file (`-N` flag) and the file with the upper and lower bounds (`-B` flag)  should be always provided when invoking MOMO.


#### Optimise a flux

In order to optimise one flux (for example, the biomass reaction) you should invoke MOMO with at least the following flags:

 * `--maximize` (or `--minimize`, depending how you want to optimize).
The argument for this flag should be the id of the reaction that you want to optimize according to the SBML file used.

*Example*:
`
 ./MOMO-x86_64.AppImage -N yeast_5.01_model.xml -B bounds.txt --maximize=r_2110 -o fba.out
`

In this example we set the metabolic network as the file "yeast_5.01_model.xml", the bounds for each reactio of the SBML file are given by the file "bounds.txt", we set the optimization function to be the reaction with the id r_2110 and we set the output file to be "fba.out".


#### Optimise several fluxes considering deletions

In order to optimise multiple fluxes (for example, the biomass and ethanol productions) you should invoke MOMO with at least the following flags:

* `--maximize`- (or `---minimize`-, depending how you want to optimize);
The argument for this flag should be the ids of the reactions that you want to optimize according to the SBML file used, separated by commas.
* `--number-deletions`;
The argument for this flag should be an integer >= 1, representing the size of the deletion set. So if the parameter is 1, we have single deletions, if it is 2 then we have pairs of deletions, and so on.

*Example*:
`
./MOMO-x86_64.AppImage -N yeast_5.01_model.xml -B bounds.txt --maximize=r_1761,r_2110 --number-deletions=1 -R fba.out -D candidateReactions.txt -o pareto.out
`

#### Enumerate deletions

In order to enumerate all deletions for a fixed production state (for example, given that biomass is equal to 0.4 and that the ethanol is equal to 6) should invoke MOMO with at least the following flags:

* `--enumerate`
* `--number-deletions`;
The argument for this flag should be an integer >= 1, representing the size of the deletion set. So if the parameter is 1, we have single deletions, if it is 2 then we have pairs of deletions, and so on.

**Important**
`--maximize` and `--minimize` should not be used in the enumeration mode.

*Example*:
`
./MOMO-x86_64.AppImage -N yeast_5.01_model.xml -B bounds.txt -D candidateReactions.txt -R
fba.out -o enumeration.out --number-deletions=1 --reactions-to-fix
r_2110=0.1,r_1761=17.4 --enumerate
`

In this example we set the metabolic network as the file "yeast_5.01_model.xml", the bounds for each reactio of the SBML file are given by the file "bounds.txt", we selected to enumerate single deletions (--enumerate and --number-deletions=1) and we fix the reaction r_2110 (biomass) to 0.1 and the reaction r_1761 (ethanol) to 17.4.
This means we want to enumerate all single deletions that enables a biomass of 0.1 and an ethanol production of 17.4.

### Experiments
Here you can download the data to reproduce the experiments from the paper XXXYYYZZZ.
In this [file](../../download/experiments2.tar.gz) you will find: 

* A copy of the Yeats 5.01 metabolic network in SBML file (yeast_5.01_model.xml).
* A file with the bounds used in our experiments (yeast_5.01_model.inputs.bounds.txt)
* A file with a list of deletion (reaction) candidates (deletionsCandidates.txt)
* A text file with the full commands to run MOMO (commands.txt).

### Compiling the source code
In order to compile the source code you will have to install the following dependencies:

* glib (with glibmm)
* SCIP and PolySCIP versions 3.2.1 (SCIP Optimization Suite v. 3.2.1)
* libxml version 2.6
* libboost_program_options

You will also need the more recent version of GNU G++ compiler and Make.
Except for SCIP and PolySCIP all other dependencies can be easily installed using the software manager of yout favorite Linux distribution.
In Ubuntu for example, it suffice to install the packages 

For SCIP Optimization Suite you will have to download the source code from [scip.zib.de](http://scip.zib.de/) and follow their instructions to compile SCIP and PolySCIP.
After all this you have to generate a library from PolySCIP code and finally compile MOMO.
This part is a little bit technical but in [this file](../../download/CompilingMOMOInUbuntu.txt) you can find all instructions to reproduce this steps and compile MOMO in a Ubuntu machine (tested with 18.04 and 19.04).

### References
Ricardo Andrade, Mahdi Doostmohammadi, João L. Santos, Marie-France Sagot, Nuno P. Mira, Susana Vinga. Momo – Multi-Objective Metabolic mixed integer Optimization: application to yeast strain engineering

bioRxiv 476689; doi: [10.1101 / 476689](https://doi.org/10.1101/476689)
 
