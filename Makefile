GLIBMM_INC_DIR := /opt/local/include/glibmm-2.4
GLIBMM_LIB_DIR := /opt/local/lib/glibmm-2.4
GLIB_LIB_DIR := /opt/local/lib/glib-2.0
GLIB_INC_DIR := /opt/local/include/glib-2.0
LIBXML_INC_DIR := /opt/local/include/libxml++-2.6
LIBXML_LIB_DIR := /opt/local/lib/libxml++-2.6/include
CPLEX_DIR := /Users/randrade/Applications/IBM/ILOG/CPLEX_Studio1263

CPP := /usr/bin/g++
CPP_FLAGS := -std=c++0x -DILOUSESTL -I$(GLIBMM_INC_DIR) -I$(GLIBMM_INC_DIR)/glibmm -I$(GLIBMM_LIB_DIR)/include -I$(GLIB_LIB_DIR) -I$(GLIB_LIB_DIR)/include -I$(GLIB_INC_DIR) -I$(LIBXML_INC_DIR) -I$(LIBXML_LIB_DIR) -I$(CPLEX_DIR)/concert/include -I$(CPLEX_DIR)/cplex/include -O0 -g3 -w -c -fmessage-length=0 -fpermissive -fPIC

LINK_FLAGS := -L/opt/local/lib -L$(CPLEX_DIR)/concert/lib/x86-64_osx/static_pic -L$(CPLEX_DIR)/cplex/bin/x86-64_osx -L$(CPLEX_DIR)/cplex/lib/x86-64_osx/static_pic 

OBJS=bin/CplexFBAModeler.o bin/hypergraph/Decomposition.o bin/hypergraph/HEdge.o bin/hypergraph/HGraph.o bin/hypergraph/HNode.o bin/read_bounds.o bin/read_writeFile.o bin/main.o
LIBS=-lxml++-2.6.2 -lglib-2.0.0 -lglibmm-2.4 -lconcert -lcplex -lilocplex

all: dirs FBA 

dirs:
	mkdir -p bin/hypergraph
	mkdir -p bin/cplex_interface

FBA: $(OBJS)
	@echo 'Building target: $@'
	@echo 'Invoking: MacOS X C++ Linker'
	$(CPP) $(LINK_FLAGS) -o "$@" $(OBJS) $(USER_OBJS) $(LIBS)
	@echo 'Finished building target: $@'
	@echo ' '

bin/%.o: src/%.cpp 
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(CPP) $(CPP_FLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

clean:
	rm -rf FBA 
	rm -rf bin
