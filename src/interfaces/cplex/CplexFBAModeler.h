/*
 * CplexFBAModeler.h
 *
 *  Created on: 9 juil. 2016
 *      Author: randrade
 */

#ifndef SRC_CPLEX_INTERFACE_CPLEXFBAMODELER_H_
#define SRC_CPLEX_INTERFACE_CPLEXFBAMODELER_H_

using namespace std;

#include <cmath>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <string>
#include <ilcplex/ilocplex.h>
#include "../../hypergraph/HGraph.h"
#include "../../io/Bounds.h"
#include "../../io/read_bounds.h"


// ===========================================================================
//                             Definitions
// ===========================================================================
#define Optimal 2
typedef IloArray<IloNumVarArray>  IloNumVarMatrix;
typedef IloArray<IloNumVarMatrix>  IloNumVarMatrix2;

class CplexFBAModeler {
public:
   CplexFBAModeler(const HGraph&  graph);
   vector<double> doFBA(vector<HEdge> reactionsToProduce,vector<Bounds> reactionBounds);
   void doFBAFixingBiomassAndEthanol(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds,double biomassProduction, double ethanolProduction);
   void doMOMAFixingBiomassAndEthanolExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, double biomassProduction, double ethanolProduction, vector<double>  scores);
   void doFBAFixingBiomassAndEthanolExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, double biomassProduction, double ethanolProduction, vector<double>  scores);
   void doFBAFixingBiomassEthanolAndGlycerol(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds,double biomassProduction, double ethanolProduction, double glycerolProduction);
   void doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, double biomassProduction, double ethanolProduction, double glycerolProduction, vector<double>  scores);
   void doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, double biomassProduction, double ethanolProduction, double glycerolProduction, vector<double>  scores);
   vector<double> doMultiFBADeletingReactions(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<HEdge> reactionsToDelete);

   void generateMPSFormat(string name, vector<Bounds> bounds, vector<std::string> reactionsToFix);
   virtual ~CplexFBAModeler();

private:
   const IloEnv _env;
   const HGraph _graph;
   IloNumArray _zArray;
   IloNumArray _oArray;
};


#endif /* SRC_CPLEX_INTERFACE_CPLEXFBAMODELER_H_ */
