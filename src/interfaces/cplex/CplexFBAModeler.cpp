/*
 * CplexFBAModeler.cpp
 *
 *  Created on: 9 juil. 2016
 *      Author: randrade
 */

#include "CplexFBAModeler.h"
#include <ilcplex/ilocplex.h>

#include<set>

CplexFBAModeler::CplexFBAModeler(const HGraph&  graph) : _env(IloEnv()), _graph(graph)
{
   IloNumArray zArray(this->_env,graph.getNbEdges());
   IloNumArray oArray(this->_env,graph.getNbEdges());
   for(int i=0;i<graph.getNbEdges();++i)
   {
      zArray[i] = 0.0;
      oArray[i] = 1.0;
   }
   this->_zArray = zArray;
   this->_oArray = oArray;

}

CplexFBAModeler::~CplexFBAModeler()
{
   //this->_env.end();
   //this->_zArray.clear();
}

vector<double> CplexFBAModeler::doFBA(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds)
{
   // Number of lines and columns
   const unsigned int ncolsS = _graph.getNbEdges();
   const unsigned int nlinesS = _graph.getNbNodes();
   cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   // Return vector
   std::vector<double> fluxesSol = std::vector<double>(ncolsS);

   IloNumArray inputDeltas(_env,nlinesS);
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      inputDeltas[id] = 0.0;
   }
   for (const int id : _graph.getBlackNodes())
   {
      inputDeltas[id] = 0.0;
   }
   IloModel hyperstory(_env);
   IloNumVarArray fluxArray(_env,ncolsS);

   try{
      // Model variables
      // For each flux
      for(unsigned int i=0;i<ncolsS;++i)
      {
         std::string vname = "v_"+std::to_string(i);
         fluxArray[i] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());
      }

      // Constraints
      for(unsigned int i=0;i<nlinesS;++i)
      {
         bool addSSConstraint = false;
         IloNumExpr variationOfCompound(_env);
         for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
         {
            int edge_id = edge_it->first;
            HEdge edge = edge_it->second;
            const list<int> sources = edge.getTailNodes();
            const list<double> sourcesStoich = edge.getTailStoichiometry();
            list<int>::const_iterator it1;
            list<double>::const_iterator it2;
            for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound -= nodeStoich*fluxArray[edge_id];
               }
            }
            list<int> products = edge.getHeadNodes();
            list<double> productsStoich = edge.getHeadStoichiometry();
            for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int) i)
               {
                  addSSConstraint = true;
                  variationOfCompound += nodeStoich*fluxArray[edge_id];
               }
            }
         }
         // Sv[i] = b[i]
         if (addSSConstraint)
         {
            if (!this->_graph.getNode(i).isBoundary())
            {
               std::string svbname = "(Sv=b)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
               auto svconstraint = hyperstory.add(variationOfCompound == 0);
               svconstraint.setName(svbname.c_str());
            }
            //            else
            //            {
            //
            //               std::string svbname = "(Sv=b)_"+std::to_string(i);
            //               auto svconstraint = hyperstory.add(variationOfCompound >= 0);
            //               svconstraint.setName(svbname.c_str());
            //            }
         }

      }

      set<int> constrainedReactions;
      // Constraints for reactions
      for (Bounds b : reactionBounds)
      {
         unsigned int j = b.id;
         assert(j<ncolsS);

         IloNumExpr lBound(_env);
         IloNumExpr uBound(_env);

         lBound += b.lb;
         uBound += b.ub;
         //cout << "Bounds: " << lBound << " " << _graph.getEdge(b.id).getName() << " " << uBound << endl;
         std::string blbname = "b_lo_bound_"+std::to_string(j);
         auto lbconstraint = hyperstory.add(lBound <= fluxArray[j]);
         lbconstraint.setName(blbname.c_str());
         std::string bubname = "b_up_bound_"+std::to_string(j);
         auto ubconstraint = hyperstory.add(uBound >= fluxArray[j]);
         ubconstraint.setName(bubname.c_str());
         constrainedReactions.insert(j);
      }

      for(unsigned int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string vname = "v_"+std::to_string(j);
            fluxArray[j] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());

            IloNumExpr fluxLoBound(_env);
            fluxLoBound += fluxArray[j];
            std::string flbname = "flux_lo_bound_"+std::to_string(j);
            auto flbconstraint = hyperstory.add(fluxLoBound >= -100000);
            flbconstraint.setName(flbname.c_str());

            IloNumExpr fluxGeBound(_env);
            fluxGeBound += fluxArray[j];
            std::string fubname = "flux_up_bound_"+std::to_string(j);
            auto fubconstraint = hyperstory.add(fluxGeBound <= 100000);
            fubconstraint.setName(fubname.c_str());
         }
      }

      // Objective function
      IloNumExpr objective(_env);
      for (HEdge e : reactionsToProduce)
      {
         objective += fluxArray[e.getId()];
      }
      //hyperstory.add(objective >= 0.01);
      hyperstory.add(IloMaximize(_env, objective));
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   try{
      IloCplex solver(_env);
      solver.setOut(_env.getNullStream());
      solver.setWarning(_env.getNullStream());
      solver.setParam(IloCplex::WorkDir,".");
      solver.setParam(IloCplex::WorkMem,5000);
      solver.setParam(IloCplex::NodeFileInd,3);
      solver.setParam(IloCplex::TiLim, 21600);
      solver.setParam(IloCplex::EpGap, 1e-7);
      //solver.setParam(IloCplex::PreInd, false);
      solver.exportModel("/tmp/fba.lp");

      //cout << "[###] The model has (without presolve):" << endl;
      //cout << "[###] " << numVars << " variables;" << endl;
      //cout << "[###] " << numRestricoes << " constraints;" << endl;

      //      IloExtractable fubconstraint;
      //      for (auto it : this->_graph.getEdges())
      //      {
      //         HEdge e = it.second;
      //         int j = e.getId();
      //         IloNumExpr fluxGeBound(_env);
      //         fluxGeBound += fluxArray[j];
      //         fubconstraint = hyperstory.add(fluxGeBound <= 0.2);

      solver.extract(hyperstory);
      solver.exportModel("/tmp/fba.lp");
      solver.solve();

      //cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

      if ( solver.getStatus() == Optimal )
      {
         // Get the solution
         int amountOfZeroFluxes = 0;
         string reactionsToFixAtZero = "";

         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l){
            fluxesSol[l] = fluxesSolTmp[l];
            if (fluxesSol[l] == 0)
            {
               ++amountOfZeroFluxes;
               HEdge e = _graph.getEdge(l);
               //cout << e.getName() << " " << e.getDescription() << endl;
            }
            //cout << this->_graph.getEdge(l).getName() << "\t" << fluxesSol[l] << endl;
         }
         //           if (fluxesSol[1807] > 0  && fluxesSol[2108] > 0 )
         //           {
         //cout << e.getName() << " does the trick (Alcohol: " << fluxesSol[1760] << "Glycerol: " << fluxesSol[1807] << ", Biomass: " << fluxesSol[2108] << ")" <<   endl;
         cout << " Solution found (Alcohol: " << fluxesSol[1760] << "Glycerol: " << fluxesSol[1807] << ", Biomass: " << fluxesSol[2108] << ")" <<   endl;
         cout << " Fluxes at zero: " << amountOfZeroFluxes << endl;

         //           }

         //            hyperstory.remove(fubconstraint);
         //         }
      }
      solver.exportModel("/tmp/fba.mps");
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   return fluxesSol;
}

void CplexFBAModeler::generateMPSFormat(std::string name, vector<Bounds> bounds, vector<std::string> reactionsToFix)
{
   std::cout << "NAME          " << name << endl;
   std::cout << "OBJSENSE" << std::endl;
   std::cout << " MIN" << std::endl;
   std::cout << "ROWS" << std::endl;
   std::cout << " N OBJ1" << std::endl;
   std::cout << " N OBJ2" << std::endl;
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      if(!this->_graph.getNode(id).isBoundary())
      {
         std::string compoundName = "C_" + std::to_string(id);
         std::cout << " E " << compoundName  << endl;
      }
   }
   for (Bounds b : bounds)
   {
      std::string reactionName = this->_graph.getEdge(b.id).getName();
      std::string constraintName = "UB_" + reactionName;
      std::cout << " L " << constraintName  << endl;
      constraintName = "LB_" + reactionName;
      std::cout << " G " << constraintName  << endl;
   }
   std::cout << " E KNOCK1" << std::endl;
   // Fixing those deletions to zero so they are never a candidate.
   //1542,1545,1546,1547,1548,1549,1550,1551,1552,1553,1554,1563,1564,1565,1566,1571,1572,1577,1580,1581,1586,1589,1598,1604,1621,1625,1627,1629,1630,1631,1634,1639,1641,1643,1648,1649,1650,1651,1654,1663,1671,1672,1683,1687,1702,1705,1706,1709,1710,1711,1712,1714,1715,1716,1718,1727,1730,1749,1753,1757,1761,1764,1765,1767,1788,1791,1792,1793,1798,1799,1800,1806,1807,1808,1810,1814,1815,1818,1820,1831,1833,1840,1842,1846,1860,1861,1864,1865,1866,1869,1872,1874,1877,1878,1879,1880,1882,1885,1888,1890,1892,1895,1896,1898,1899,1901,1902,1903,1905,1908,1910,1911,1912,1913,1914,1915,1916,1917,1930,1932,1946,1948,1950,1951,1961,1966,1967,1983,1984,1986,1988,1991,1992,1993,1998,1999,2000,2004,2008,2018,2019,2023,2027,2032,2037,2042,2043,2045,2048,2050,2051,2054,2055,2057,2059,2060,2061,2065,2066,2067,2072,2075,2082,2089,2090,2091,2099,2101,2103,2105,2109,2110
   //vector<std::string> reactionsToFix = {"r_1542","r_1545","r_1546","r_1547","r_1548","r_1549","r_1550","r_1551","r_1552","r_1553","r_1554","r_1563","r_1564","r_1565","r_1566","r_1571","r_1572","r_1577","r_1580","r_1581","r_1586","r_1589","r_1598","r_1604","r_1621","r_1625","r_1627","r_1629","r_1630","r_1631","r_1634","r_1639","r_1641","r_1643","r_1648","r_1649","r_1650","r_1651","r_1654","r_1663","r_1671","r_1672","r_1683","r_1687","r_1702","r_1705","r_1706","r_1709","r_1710","r_1711","r_1712","r_1714","r_1715","r_1716","r_1718","r_1727","r_1730","r_1749","r_1753","r_1757","r_1761","r_1764","r_1765","r_1767","r_1788","r_1791","r_1792","r_1793","r_1798","r_1799","r_1800","r_1806","r_1807","r_1808","r_1810","r_1814","r_1815","r_1818","r_1820","r_1831","r_1833","r_1840","r_1842","r_1846","r_1860","r_1861","r_1864","r_1865","r_1866","r_1869","r_1872","r_1874","r_1877","r_1878","r_1879","r_1880","r_1882","r_1885","r_1888","r_1890","r_1892","r_1895","r_1896","r_1898","r_1899","r_1901","r_1902","r_1903","r_1905","r_1908","r_1910","r_1911","r_1912","r_1913","r_1914","r_1915","r_1916","r_1917","r_1930","r_1932","r_1946","r_1948","r_1950","r_1951","r_1961","r_1966","r_1967","r_1983","r_1984","r_1986","r_1988","r_1991","r_1992","r_1993","r_1998","r_1999","r_2000","r_2004","r_2008","r_2018","r_2019","r_2023","r_2027","r_2032","r_2037","r_2042","r_2043","r_2045","r_2048","r_2050","r_2051","r_2054","r_2055","r_2057","r_2059","r_2060","r_2061","r_2065","r_2066","r_2067","r_2072","r_2075","r_2082","r_2089","r_2090","r_2091","r_2099","r_2101","r_2103","r_2105","r_2109","r_2110"};
   for (string reactionName : reactionsToFix)
   {
      string cName = "FIX_" + reactionName;
      std::cout << " E ";
      std::cout.width(10);
      std::cout << std::left << cName << endl;
   }

   std::cout << "COLUMNS" << endl;
   std::cout << "    MARK0000  \'MARKER\'                 \'INTORG\'" <<std::endl;
   for (Bounds b : bounds)
   {
      HEdge edge = this->_graph.getEdge(b.id);
      std::string reactionName = edge.getName();
      std::string variableName = "Y_" + reactionName;
      string bname;
      if (std::find(reactionsToFix.begin(), reactionsToFix.end(),reactionName) != reactionsToFix.end())
      {
         bname = "FIX_" + reactionName;
         std::cout << "    ";
         std::cout.width(10);
         std::cout <<  std::left << variableName;
         std::cout.width(10);
         std::cout << std::left << bname;
         std::cout.width(12);
         std::cout << std::right << "1" << endl;
      }
      bname = "KNOCK1";
      std::cout << "    ";
      std::cout.width(10);
      std::cout <<  std::left << variableName;
      std::cout.width(10);
      std::cout << std::left << bname;
      std::cout.width(12);
      std::cout << std::right << "1" << endl;
      if (b.ub != 0)
      {
         bname = "UB_" + reactionName;
         std::cout << "    ";
         std::cout.width(10);
         std::cout <<  std::left << variableName;
         std::cout.width(10);
         std::cout << std::left << bname;
         std::cout.width(12);
         std::cout << std::right << b.ub << endl;
      }
      if (b.lb != 0)
      {
         bname = "LB_" + reactionName;
         std::cout << "    ";
         std::cout.width(10);
         std::cout <<  std::left << variableName;
         std::cout.width(10);
         std::cout << std::left << bname;
         std::cout.width(12);
         std::cout << std::right << b.lb << endl;
      }
   }
   std::cout << "    MARK0000  \'MARKER\'                 \'INTEND\'" <<std::endl;
   for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
   {
      int edge_id = edge_it->first;
      HEdge edge = edge_it->second;
      std:: string rname = edge.getName();
      if (rname.length() > 8)
      {
         rname = "R_" + std::to_string(edge_id);
      }
      const list<int> sources = edge.getTailNodes();
      const list<double> sourcesStoich = edge.getTailStoichiometry();
      list<int>::const_iterator it1;
      list<double>::const_iterator it2;
      for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
      {
         int nodeId = *it1;
         double nodeStoich = -(*it2);
         if(!this->_graph.getNode(nodeId).isBoundary())
         {
            string cname = "C_" + std::to_string(nodeId);
            std::cout << "    ";
            std::cout.width(10);
            std::cout <<  std::left << rname;
            std::cout.width(10);
            std::cout << std::left << cname;
            std::cout.width(12);
            std::cout << std::right << nodeStoich << endl;
         }
      }
      list<int> products = edge.getHeadNodes();
      list<double> productsStoich = edge.getHeadStoichiometry();
      for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
      {
         int nodeId = *it1;
         double nodeStoich = *it2;
         if(!this->_graph.getNode(nodeId).isBoundary())
         {
            string cname = "C_" + std::to_string(nodeId);
            std::cout << "    ";
            std::cout.width(10);
            std::cout <<  std::left << rname;
            std::cout.width(10);
            std::cout << std::left << cname;
            std::cout.width(12);
            std::cout << std::right << nodeStoich << endl;
         }
      }
      if (edge.getName().compare("r_2110") == 0) // IF BIOMASS, ADD TO OBJ
      {
         std::cout << "    ";
         std::cout.width(10);
         std::cout <<  std::left << rname;
         std::cout << "OBJ1                -1" << std::endl;
      }
      else if (edge.getName().compare("r_1761") == 0) // IF ETHANOL, ADD TO OBJ
      {
         std::cout << "    ";
         std::cout.width(10);
         std::cout <<  std::left << rname;
         std::cout << "OBJ2                -1" << std::endl;
      }

      for (Bounds b : bounds)
      {
         if (b.id == edge.getId())
         {
            std::string reactionName = edge.getName();
            string bname = "UB_" + reactionName;
            std::cout << "    ";
            std::cout.width(10);
            std::cout <<  std::left << rname;
            std::cout.width(10);
            std::cout << std::left << bname;
            std::cout.width(12);
            std::cout << std::right << "1" << endl;
            bname = "LB_" + reactionName;
            std::cout << "    ";
            std::cout.width(10);
            std::cout <<  std::left << rname;
            std::cout.width(10);
            std::cout << std::left << bname;
            std::cout.width(12);
            std::cout << std::right << "1" << endl;
         }
      }
   }
   cout << "RHS" << endl;
   for (Bounds b : bounds)
   {
      HEdge edge = this->_graph.getEdge(b.id);
      std::string reactionName = edge.getName();
      string bname;
      if (b.ub != 0)
      {
         bname = "UB_" + reactionName;
         std::cout << "    ";
         std::cout.width(10);
         std::cout <<  std::left << "RHS";
         std::cout.width(10);
         std::cout << std::left << bname;
         std::cout.width(12);
         std::cout << std::right << b.ub << endl;
      }
      if (b.lb != 0)
      {
         bname = "LB_" + reactionName;
         std::cout << "    ";
         std::cout.width(10);
         std::cout <<  std::left << "RHS";
         std::cout.width(10);
         std::cout << std::left << bname;
         std::cout.width(12);
         std::cout << std::right << b.lb << endl;
      }
   }
   cout << "    RHS       KNOCK1               1" << std::endl;
   std::cout << "BOUNDS" << endl;
   for (Bounds b : bounds)
   {
      HEdge reaction = this->_graph.getEdge(b.id);
      std::string reactionName = reaction.getName();
      string bname = "BND";
      std::cout << " MI ";
      std::cout.width(10);
      std::cout << std::left << bname;
      std::cout.width(10);
      std::cout << std::left << reactionName << endl;
      std::cout << " PL ";
      std::cout.width(10);
      std::cout << std::left << bname;
      std::cout.width(10);
      std::cout << std::left << reactionName << endl;
   }
   for (Bounds b : bounds)
   {
      HEdge reaction = this->_graph.getEdge(b.id);
      std::string variableName = "Y_" + reaction.getName();
      string bname = "BND";
      std::cout << " LO ";
      std::cout.width(10);
      std::cout << std::left << bname;
      std::cout.width(10);
      std::cout << std::left << variableName;
      std::cout.width(12);
      std::cout << std::right << "0" << endl;
      std::cout << " UP ";
      std::cout.width(10);
      std::cout << std::left << bname;
      std::cout.width(10);
      std::cout << std::left << variableName;
      std::cout.width(12);
      std::cout << std::right << "1" << endl;
   }
   cout << "ENDATA" << endl;

}

void CplexFBAModeler::doFBAFixingBiomassAndEthanol(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds,double biomassProduction, double ethanolProduction)
{
   // Number of lines and columns
   const unsigned int ncolsS = _graph.getNbEdges();
   const unsigned int nlinesS = _graph.getNbNodes();
   cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   IloNumArray inputDeltas(_env,nlinesS);
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      inputDeltas[id] = 0.0;
   }
   for (const int id : _graph.getBlackNodes())
   {
      inputDeltas[id] = 0.0;
   }
   IloModel hyperstory(_env);
   IloNumVarArray fluxArray(_env,ncolsS);
   IloNumVarArray deletionsArray(_env,ncolsS);


   try{
      // Model variables
      // For each flux
      for(unsigned int i=0;i<ncolsS;++i)
      {
         std::string vname = "v_"+std::to_string(i);
         fluxArray[i] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());
         std::string yname = "y_"+std::to_string(i);
         deletionsArray[i] = IloNumVar(_env,0,1,ILOBOOL,yname.c_str());
      }

      // Constraints
      // Fixing hte number of fixed deletion to one
      IloNumExpr numberOfDeletions(_env);
      for(unsigned int i=0;i<ncolsS;++i)
      {
         numberOfDeletions += deletionsArray[i];
      }
      std::string svbname = "NumberOfSingleDeletions";
      auto delconstraint = hyperstory.add(numberOfDeletions == 1);
      delconstraint.setName(svbname.c_str());

      // Fixing the rates of biomass and ethanol production
      HEdge biomass = *this->_graph.getEdge("r_2110");
      //auto bioConstraint = hyperstory.add(fluxArray[biomass.getId()] >= biomassProduction);
      //bioConstraint.setName("BiomassProd");
      HEdge ethanol = *this->_graph.getEdge("r_1761");
      auto ethanolConstraint = hyperstory.add(fluxArray[ethanol.getId()] == ethanolProduction);
      ethanolConstraint.setName("EthanolProd");
      //HEdge gly = *this->_graph.getEdge("r_1808");
      //auto glyConstraint = hyperstory.add(fluxArray[gly.getId()] == 0);
      //glyConstraint.setName("GlycerolProd");

      // Fixing some Y to zero because we don't want to delete those reactions
      // old: std::string reactionsToFix[] = {"r_1542","r_1545","r_1546","r_1547","r_1548","r_1549","r_1550","r_1551","r_1552","r_1553","r_1554","r_1563","r_1564","r_1565","r_1566","r_1571","r_1572","r_1577","r_1580","r_1581","r_1586","r_1589","r_1598","r_1604","r_1621","r_1625","r_1627","r_1629","r_1630","r_1631","r_1634","r_1639","r_1641","r_1643","r_1648","r_1649","r_1650","r_1651","r_1654","r_1663","r_1671","r_1672","r_1683","r_1687","r_1702","r_1705","r_1706","r_1709","r_1710","r_1711","r_1712","r_1714","r_1715","r_1716","r_1718","r_1727","r_1730","r_1749","r_1753","r_1757","r_1761","r_1764","r_1765","r_1767","r_1788","r_1791","r_1792","r_1793","r_1798","r_1799","r_1800","r_1806","r_1807","r_1808","r_1810","r_1814","r_1815","r_1818","r_1820","r_1831","r_1833","r_1840","r_1842","r_1846","r_1860","r_1861","r_1864","r_1865","r_1866","r_1869","r_1872","r_1874","r_1877","r_1878","r_1879","r_1880","r_1882","r_1885","r_1888","r_1890","r_1892","r_1895","r_1896","r_1898","r_1899","r_1901","r_1902","r_1903","r_1905","r_1908","r_1910","r_1911","r_1912","r_1913","r_1914","r_1915","r_1916","r_1917","r_1930","r_1932","r_1946","r_1948","r_1950","r_1951","r_1961","r_1966","r_1967","r_1983","r_1984","r_1986","r_1988","r_1991","r_1992","r_1993","r_1998","r_1999","r_2000","r_2004","r_2008","r_2018","r_2019","r_2023","r_2027","r_2032","r_2037","r_2042","r_2043","r_2045","r_2048","r_2050","r_2051","r_2054","r_2055","r_2057","r_2059","r_2060","r_2061","r_2065","r_2066","r_2067","r_2072","r_2075","r_2082","r_2089","r_2090","r_2091","r_2099","r_2101","r_2103","r_2105","r_2109","r_2110"};
      std::string reactionsToFix[] = {"r_1542", "r_1545", "r_1546", "r_1547", "r_1548", "r_1549", "r_1550", "r_1551", "r_1552", "r_1553", "r_1554", "r_1563", "r_1564", "r_1565", "r_1566", "r_1571", "r_1572", "r_1577", "r_1580", "r_1581", "r_1586", "r_1589", "r_1598", "r_1604", "r_1621", "r_1625", "r_1627", "r_1629", "r_1630", "r_1631", "r_1634", "r_1639", "r_1641", "r_1643", "r_1648", "r_1649", "r_1650", "r_1651", "r_1654", "r_1663", "r_1671", "r_1672", "r_1683", "r_1687", "r_1702", "r_1705", "r_1706", "r_1709", "r_1710", "r_1711", "r_1712", "r_1714", "r_1715", "r_1716", "r_1718", "r_1727", "r_1730", "r_1749", "r_1753", "r_1757", "r_1761", "r_1764", "r_1765", "r_1767", "r_1788", "r_1791", "r_1792", "r_1793", "r_1798", "r_1799", "r_1800", "r_1806", "r_1807", "r_1808", "r_1810", "r_1814", "r_1815", "r_1818", "r_1820", "r_1832", "r_1834", "r_1841", "r_1843", "r_1847", "r_1861", "r_1862", "r_1865", "r_1866", "r_1867", "r_1870", "r_1873", "r_1875", "r_1878", "r_1879", "r_1880", "r_1881", "r_1883", "r_1886", "r_1889", "r_1891", "r_1893", "r_1896", "r_1897", "r_1899", "r_1900", "r_1902", "r_1903", "r_1904", "r_1906", "r_1909", "r_1911", "r_1912", "r_1913", "r_1914", "r_1915", "r_1916", "r_1917", "r_1918", "r_1931", "r_1933", "r_1947", "r_1949", "r_1951", "r_1952", "r_1962", "r_1967", "r_1968", "r_1984", "r_1985", "r_1987", "r_1989", "r_1992", "r_1993", "r_1994", "r_1999", "r_2000", "r_2001", "r_2005", "r_2009", "r_2019", "r_2020", "r_2024", "r_2028", "r_2033", "r_2038", "r_2043", "r_2044", "r_2046", "r_2049", "r_2051", "r_2052", "r_2055", "r_2056", "r_2058", "r_2060", "r_2061", "r_2062", "r_2066", "r_2067", "r_2068", "r_2073", "r_2076", "r_2083", "r_2090", "r_2091", "r_2092", "r_2100", "r_2102", "r_2104", "r_2106", "r_2110", "r_2111"};
      //for (int i : { 1542,1545,1546,1547,1548,1549,1550,1551,1552,1553,1554,1563,1564,1565,1566,1571,1572,1577,1580,1581,1586,1589,1598,1604,1621,1625,1627,1629,1630,1631,1634,1639,1641,1643,1648,1649,1650,1651,1654,1663,1671,1672,1683,1687,1702,1705,1706,1709,1710,1711,1712,1714,1715,1716,1718,1727,1730,1749,1753,1757,1761,1764,1765,1767,1788,1791,1792,1793,1798,1799,1800,1806,1807,1808,1810,1814,1815,1818,1820,1831,1833,1840,1842,1846,1860,1861,1864,1865,1866,1869,1872,1874,1877,1878,1879,1880,1882,1885,1888,1890,1892,1895,1896,1898,1899,1901,1902,1903,1905,1908,1910,1911,1912,1913,1914,1915,1916,1917,1930,1932,1946,1948,1950,1951,1961,1966,1967,1983,1984,1986,1988,1991,1992,1993,1998,1999,2000,2004,2008,2018,2019,2023,2027,2032,2037,2042,2043,2045,2048,2050,2051,2054,2055,2057,2059,2060,2061,2065,2066,2067,2072,2075,2082,2089,2090,2091,2099,2101,2103,2105,2109,2110 }) {
      //   HEdge e = _graph.getEdge(i-1);
      //   cout << "\"" << e.getName() << "\", ";
      //}
      //cout << endl;
      //exit(-1);
      // New reaction to fix (Attempt 1 - Joint work with Nuno Dropbox)
      // ids = { 6,58,74,83,90,93,99,142,195,210,213,226,231,243,358,396,423,424,425,426,427,428,429,430,431,511,539,540,568,667,675,721,770,833,858,900,901,908,916,953,954,955,956,967,974,975,976,977,978,979,980,981,1040,1051,1085,1086,1172,1191,1260,2109,1761 };
      //std::string reactionsToFix[] = {"r_0006", "r_0058", "r_0074", "r_0083", "r_0090", "r_0093", "r_0099", "r_0142", "r_0195", "r_0210", "r_0213", "r_0226", "r_0231", "r_0243", "r_0358", "r_0396", "r_0423", "r_0424", "r_0425", "r_0426", "r_0427", "r_0428", "r_0429", "r_0430", "r_0431", "r_0511", "r_0539", "r_0540", "r_0568", "r_0667", "r_0675", "r_0721", "r_0770", "r_0833", "r_0858", "r_0900", "r_0901", "r_0908", "r_0916", "r_0953", "r_0954", "r_0955", "r_0956", "r_0967", "r_0974", "r_0975", "r_0976", "r_0977", "r_0978", "r_0979", "r_0980", "r_0981", "r_1040", "r_1051", "r_1085", "r_1086", "r_1172", "r_1191", "r_1260", "r_2110", "r_1761"};
      // Attempt 2 - Dropbox
      // ids = { 6,9,58,74,83,90,93,99,142,195,210,213,226,231,243,358,396,423,424,425,426,427,428,429,430,431,511,539,540,568,667,675,721,770,833,858,900,901,908,916,953,954,955,956,967,974,975,976,977,978,979,980,981,1040,1051,1085,1086,1172,1191,1260,2103,2104,2108,2109,1761 };
      //std::string reactionsToFix[] = {"r_0006", "r_0009", "r_0058", "r_0074", "r_0083", "r_0090", "r_0093", "r_0099", "r_0142", "r_0195", "r_0210", "r_0213", "r_0226", "r_0231", "r_0243", "r_0358", "r_0396", "r_0423", "r_0424", "r_0425", "r_0426", "r_0427", "r_0428", "r_0429", "r_0430", "r_0431", "r_0511", "r_0539", "r_0540", "r_0568", "r_0667", "r_0675", "r_0721", "r_0770", "r_0833", "r_0858", "r_0900", "r_0901", "r_0908", "r_0916", "r_0953", "r_0954", "r_0955", "r_0956", "r_0967", "r_0974", "r_0975", "r_0976", "r_0977", "r_0978", "r_0979", "r_0980", "r_0981", "r_1040", "r_1051", "r_1085", "r_1086", "r_1172", "r_1191", "r_1260", "r_2104", "r_2105", "r_2109", "r_2110", "r_1761"};
      for (std::string reactionName : reactionsToFix)
      {
         HEdge edge = *this->_graph.getEdge(reactionName);
         auto fixconstraint = hyperstory.add(deletionsArray[edge.getId()] == 0);
         std::string cname = "FIX_"+edge.getName();
         fixconstraint.setName(cname.c_str());
      }
      for(unsigned int i=0;i<nlinesS;++i)
      {
         bool addSSConstraint = false;
         IloNumExpr variationOfCompound(_env);
         for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
         {
            int edge_id = edge_it->first;
            HEdge edge = edge_it->second;
            const list<int> sources = edge.getTailNodes();
            const list<double> sourcesStoich = edge.getTailStoichiometry();
            list<int>::const_iterator it1;
            list<double>::const_iterator it2;
            for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound -= nodeStoich*fluxArray[edge_id];
               }
            }
            list<int> products = edge.getHeadNodes();
            list<double> productsStoich = edge.getHeadStoichiometry();
            for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound += nodeStoich*fluxArray[edge_id];
               }
            }
         }
         // Sv[i] = b[i]
         if (addSSConstraint)
         {
            if (!this->_graph.getNode(i).isBoundary())
            {
               std::string svbname = "(Sv=b)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
               auto svconstraint = hyperstory.add(variationOfCompound == 0);
               svconstraint.setName(svbname.c_str());
            }
            //            else
            //            {
            //
            //               std::string svbname = "(Sv=b)_"+std::to_string(i);
            //               auto svconstraint = hyperstory.add(variationOfCompound >= 0);
            //               svconstraint.setName(svbname.c_str());
            //            }
         }

      }

      set<int> constrainedReactions;
      // Constraints for reactions
      for (Bounds b : reactionBounds)
      {
         unsigned int j = b.id;
         assert(j<ncolsS);

         IloNumExpr lBound(_env);
         IloNumExpr uBound(_env);

         lBound += b.lb;
         uBound += b.ub;
         std::string blbname = "b_lo_bound_"+std::to_string(j);
         auto lbconstraint = hyperstory.add(lBound*(1-deletionsArray[j]) <= fluxArray[j]);
         lbconstraint.setName(blbname.c_str());
         std::string bubname = "b_up_bound_"+std::to_string(j);
         auto ubconstraint = hyperstory.add(uBound*(1-deletionsArray[j]) >= fluxArray[j]);
         ubconstraint.setName(bubname.c_str());
         constrainedReactions.insert(j);
      }

      for(unsigned int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string vname = "v_"+std::to_string(j);
            fluxArray[j] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());

            IloNumExpr fluxLoBound(_env);
            fluxLoBound += fluxArray[j];
            std::string flbname = "flux_lo_bound_"+std::to_string(j);
            auto flbconstraint = hyperstory.add(fluxLoBound >= -100000*(1-deletionsArray[j]));
            flbconstraint.setName(flbname.c_str());

            IloNumExpr fluxGeBound(_env);
            fluxGeBound += fluxArray[j];
            std::string fubname = "flux_up_bound_"+std::to_string(j);
            auto fubconstraint = hyperstory.add(fluxGeBound <= 100000*(1-deletionsArray[j]));
            fubconstraint.setName(fubname.c_str());
         }
      }

      // Objective function
      IloNumExpr objective(_env);
      for (HEdge e : reactionsToProduce)
      {
         objective += fluxArray[e.getId()];
      }
      //hyperstory.add(objective >= 0.01);
      hyperstory.add(IloMaximize(_env, objective));
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   try{
      IloCplex solver(_env);
      solver.setOut(_env.getNullStream());
      solver.setWarning(_env.getNullStream());
      solver.setParam(IloCplex::WorkDir,".");
      solver.setParam(IloCplex::WorkMem,5000);
      solver.setParam(IloCplex::NodeFileInd,3);
      solver.setParam(IloCplex::TiLim, 21600);
      solver.setParam(IloCplex::EpGap, 1e-7);
      //solver.setParam(IloCplex::PreInd, false);

      //cout << "[###] The model has (without presolve):" << endl;
      //cout << "[###] " << numVars << " variables;" << endl;
      //cout << "[###] " << numRestricoes << " constraints;" << endl;

      //      IloExtractable fubconstraint;
      //      for (auto it : this->_graph.getEdges())
      //      {
      //         HEdge e = it.second;
      //         int j = e.getId();
      //         IloNumExpr fluxGeBound(_env);
      //         fluxGeBound += fluxArray[j];
      //         fubconstraint = hyperstory.add(fluxGeBound <= 0.2);

      solver.extract(hyperstory);
      solver.exportModel("/tmp/fbaBiomassEthanol.lp");
      solver.solve();

      //cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

      double glycerolProduction = -1;
      if ( solver.getStatus() == Optimal )
      {
         // Get the solution

         std::vector<double> fluxesSol = std::vector<double>(ncolsS);
         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = fluxesSolTmp[l];
            //cout << this->_graph.getEdge(l).getName() << "\t" << fluxesSol[l] << endl;
         }
         cout << "Biomass: " << fluxesSol[2108] << ", Ethanol: " << fluxesSol[1760] << "Glycerol: " << fluxesSol[1807] <<   endl;
         glycerolProduction = fluxesSol[1807];
         /*
         for(unsigned int l=0;l<ncolsS;++l)
            {
               if (fluxesSol[l] != 0 )
               {
                  HEdge edge = this->_graph.getEdge(l);
                  cout << edge.getDescription() << " (" << edge.getName() <<") = " << fluxesSol[l] << endl;
               }
            }
         */
      }

      std::string outFilename = "/tmp/point_" + std::to_string(biomassProduction) + "b_" + std::to_string(ethanolProduction) + "e_" + std::to_string(glycerolProduction) + "g.txt";
      FILE *solutionFile = fopen (outFilename.c_str(), "w");
      while ( solver.getStatus() == Optimal )
      {
         int idToFixToZero = -1;
         // Get the solution
         IloNumArray deletionsSolTmp(_env,ncolsS);
         solver.getValues(deletionsSolTmp,deletionsArray);
         for(unsigned int l=0;l<ncolsS;++l){
            if (deletionsSolTmp[l] + 0.5 > 1){
               HEdge e = this->_graph.getEdge(l);
               std::cout << "Delete reaction " << e.getName() << std::endl;
               std::string solutionLine = "(" + e.getName() + ") " + e.getDescription() + "\n";
               fprintf (solutionFile,solutionLine.c_str(),"%s");
               idToFixToZero = l;
            }
         }
         // Add the constraint
         assert(idToFixToZero >= 0);
         hyperstory.add(deletionsArray[idToFixToZero] == 0);
         solver.extract(hyperstory);
         solver.solve();
      }
      fclose(solutionFile);

   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }
}

void CplexFBAModeler::doFBAFixingBiomassEthanolAndGlycerol(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds,double biomassProduction, double ethanolProduction, double glycerolProduction)
{
   // Number of lines and columns
   const unsigned int ncolsS = _graph.getNbEdges();
   const unsigned int nlinesS = _graph.getNbNodes();
   cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   IloNumArray inputDeltas(_env,nlinesS);
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      inputDeltas[id] = 0.0;
   }
   for (const int id : _graph.getBlackNodes())
   {
      inputDeltas[id] = 0.0;
   }
   IloModel hyperstory(_env);
   IloNumVarArray fluxArray(_env,ncolsS);
   IloNumVarArray deletionsArray(_env,ncolsS);


   try{
      // Model variables
      // For each flux
      for(unsigned int i=0;i<ncolsS;++i)
      {
         std::string vname = "v_"+std::to_string(i);
         fluxArray[i] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());
         std::string yname = "y_"+std::to_string(i);
         deletionsArray[i] = IloNumVar(_env,0,1,ILOBOOL,yname.c_str());
      }

      // Constraints
      // Fixing hte number of fixed deletion to one
      IloNumExpr numberOfDeletions(_env);
      for(unsigned int i=0;i<ncolsS;++i)
      {
         numberOfDeletions += deletionsArray[i];
      }
      std::string svbname = "NumberOfSingleDeletions";
      auto delconstraint = hyperstory.add(numberOfDeletions == 1);
      delconstraint.setName(svbname.c_str());

      // Fixing the rates of biomass and ethanol production
      HEdge biomass = *this->_graph.getEdge("r_2110");
      auto bioConstraint = hyperstory.add(fluxArray[biomass.getId()] >= biomassProduction);
      bioConstraint.setName("BiomassProd");
      HEdge ethanol = *this->_graph.getEdge("r_1761");
      auto ethanolConstraint = hyperstory.add(fluxArray[ethanol.getId()] >= ethanolProduction);
      ethanolConstraint.setName("EthanolProd");
      HEdge gly = *this->_graph.getEdge("r_1808");
      auto glyConstraint = hyperstory.add(fluxArray[gly.getId()] >= glycerolProduction);
      glyConstraint.setName("GlycerolProd");

      // Fixing some Y to zero because we don't want to delete those reactions
      //std::string reactionsToFix[] = {"r_1542","r_1545","r_1546","r_1547","r_1548","r_1549","r_1550","r_1551","r_1552","r_1553","r_1554","r_1563","r_1564","r_1565","r_1566","r_1571","r_1572","r_1577","r_1580","r_1581","r_1586","r_1589","r_1598","r_1604","r_1621","r_1625","r_1627","r_1629","r_1630","r_1631","r_1634","r_1639","r_1641","r_1643","r_1648","r_1649","r_1650","r_1651","r_1654","r_1663","r_1671","r_1672","r_1683","r_1687","r_1702","r_1705","r_1706","r_1709","r_1710","r_1711","r_1712","r_1714","r_1715","r_1716","r_1718","r_1727","r_1730","r_1749","r_1753","r_1757","r_1761","r_1764","r_1765","r_1767","r_1788","r_1791","r_1792","r_1793","r_1798","r_1799","r_1800","r_1806","r_1807","r_1808","r_1810","r_1814","r_1815","r_1818","r_1820","r_1831","r_1833","r_1840","r_1842","r_1846","r_1860","r_1861","r_1864","r_1865","r_1866","r_1869","r_1872","r_1874","r_1877","r_1878","r_1879","r_1880","r_1882","r_1885","r_1888","r_1890","r_1892","r_1895","r_1896","r_1898","r_1899","r_1901","r_1902","r_1903","r_1905","r_1908","r_1910","r_1911","r_1912","r_1913","r_1914","r_1915","r_1916","r_1917","r_1930","r_1932","r_1946","r_1948","r_1950","r_1951","r_1961","r_1966","r_1967","r_1983","r_1984","r_1986","r_1988","r_1991","r_1992","r_1993","r_1998","r_1999","r_2000","r_2004","r_2008","r_2018","r_2019","r_2023","r_2027","r_2032","r_2037","r_2042","r_2043","r_2045","r_2048","r_2050","r_2051","r_2054","r_2055","r_2057","r_2059","r_2060","r_2061","r_2065","r_2066","r_2067","r_2072","r_2075","r_2082","r_2089","r_2090","r_2091","r_2099","r_2101","r_2103","r_2105","r_2109","r_2110"};
      vector<std::string> reactionsToFix = {"r_0001","r_0002","r_0003","r_0004","r_0006","r_0009","r_0010","r_0011","r_0012","r_0013","r_0017","r_0019","r_0021","r_0022","r_0025","r_0026","r_0028","r_0030","r_0031","r_0033","r_0034","r_0035","r_0036","r_0037","r_0042","r_0043","r_0044","r_0045","r_0046","r_0047","r_0048","r_0049","r_0050","r_0051","r_0052","r_0053","r_0054","r_0055","r_0056","r_0057","r_0058","r_0059","r_0062","r_0063","r_0064","r_0066","r_0067","r_0068","r_0069","r_0070","r_0071","r_0072","r_0073","r_0074","r_0075","r_0076","r_0077","r_0078","r_0081","r_0082","r_0083","r_0084","r_0085","r_0086","r_0087","r_0088","r_0089","r_0090","r_0092","r_0093","r_0094","r_0095","r_0098","r_0099","r_0100","r_0101","r_0102","r_0104","r_0105","r_0106","r_0107","r_0108","r_0111","r_0112","r_0113","r_0114","r_0116","r_0117","r_0119","r_0120","r_0121","r_0122","r_0123","r_0124","r_0125","r_0126","r_0127","r_0128","r_0129","r_0130","r_0131","r_0132","r_0133","r_0134","r_0135","r_0136","r_0137","r_0138","r_0139","r_0140","r_0141","r_0143","r_0145","r_0146","r_0147","r_0149","r_0150","r_0155","r_0157","r_0158","r_0159","r_0161","r_0162","r_0164","r_0165","r_0166","r_0167","r_0168","r_0169","r_0170","r_0171","r_0172","r_0173","r_0174","r_0176","r_0177","r_0178","r_0179","r_0180","r_0181","r_0182","r_0183","r_0184","r_0185","r_0186","r_0187","r_0188","r_0189","r_0190","r_0191","r_0192","r_0193","r_0194","r_0196","r_0197","r_0198","r_0199","r_0200","r_0201","r_0204","r_0205","r_0206","r_0209","r_0210","r_0212","r_0213","r_0216","r_0218","r_0220","r_0221","r_0222","r_0223","r_0224","r_0227","r_0228","r_0229","r_0230","r_0233","r_0245","r_0246","r_0247","r_0248","r_0249","r_0251","r_0252","r_0253","r_0254","r_0255","r_0256","r_0257","r_0259","r_0260","r_0261","r_0262","r_0264","r_0265","r_0266","r_0268","r_0270","r_0271","r_0272","r_0273","r_0274","r_0275","r_0276","r_0277","r_0281","r_0282","r_0283","r_0284","r_0285","r_0286","r_0287","r_0288","r_0289","r_0290","r_0291","r_0292","r_0293","r_0294","r_0295","r_0296","r_0297","r_0298","r_0299","r_0301","r_0303","r_0304","r_0305","r_0306","r_0308","r_0312","r_0313","r_0314","r_0315","r_0317","r_0318","r_0319","r_0320","r_0321","r_0322","r_0323","r_0324","r_0325","r_0327","r_0328","r_0329","r_0331","r_0332","r_0333","r_0334","r_0335","r_0336","r_0337","r_0338","r_0341","r_0342","r_0343","r_0345","r_0346","r_0347","r_0348","r_0350","r_0351","r_0354","r_0356","r_0357","r_0358","r_0359","r_0360","r_0363","r_0365","r_0367","r_0368","r_0369","r_0370","r_0371","r_0372","r_0373","r_0374","r_0375","r_0376","r_0377","r_0378","r_0379","r_0380","r_0381","r_0382","r_0383","r_0384","r_0385","r_0386","r_0387","r_0388","r_0390","r_0392","r_0396","r_0399","r_0400","r_0401","r_0402","r_0403","r_0404","r_0407","r_0408","r_0409","r_0410","r_0412","r_0413","r_0414","r_0415","r_0416","r_0417","r_0418","r_0419","r_0420","r_0421","r_0422","r_0423","r_0424","r_0425","r_0426","r_0427","r_0428","r_0429","r_0430","r_0431","r_0435","r_0436","r_0437","r_0440","r_0441","r_0442","r_0443","r_0444","r_0445","r_0446","r_0447","r_0448","r_0449","r_0450","r_0453","r_0454","r_0455","r_0456","r_0457","r_0458","r_0459","r_0460","r_0461","r_0463","r_0464","r_0465","r_0469","r_0470","r_0472","r_0473","r_0475","r_0477","r_0478","r_0479","r_0480","r_0482","r_0484","r_0485","r_0487","r_0488","r_0490","r_0492","r_0493","r_0494","r_0496","r_0497","r_0498","r_0500","r_0501","r_0503","r_0504","r_0506","r_0507","r_0508","r_0509","r_0511","r_0512","r_0513","r_0515","r_0516","r_0517","r_0518","r_0519","r_0520","r_0521","r_0522","r_0523","r_0524","r_0526","r_0527","r_0529","r_0530","r_0531","r_0532","r_0533","r_0535","r_0539","r_0540","r_0541","r_0543","r_0544","r_0547","r_0550","r_0551","r_0552","r_0553","r_0554","r_0555","r_0556","r_0557","r_0560","r_0561","r_0562","r_0567","r_0568","r_0571","r_0572","r_0573","r_0574","r_0575","r_0576","r_0577","r_0578","r_0579","r_0580","r_0581","r_0582","r_0583","r_0585","r_0586","r_0587","r_0588","r_0589","r_0590","r_0591","r_0592","r_0593","r_0595","r_0596","r_0597","r_0598","r_0599","r_0600","r_0601","r_0602","r_0603","r_0604","r_0605","r_0606","r_0607","r_0608","r_0609","r_0610","r_0611","r_0612","r_0613","r_0614","r_0615","r_0616","r_0617","r_0618","r_0619","r_0620","r_0621","r_0622","r_0623","r_0624","r_0625","r_0626","r_0627","r_0628","r_0629","r_0630","r_0631","r_0632","r_0633","r_0634","r_0635","r_0636","r_0637","r_0638","r_0639","r_0640","r_0641","r_0642","r_0643","r_0644","r_0645","r_0646","r_0647","r_0648","r_0649","r_0650","r_0651","r_0652","r_0653","r_0654","r_0655","r_0657","r_0659","r_0660","r_0661","r_0665","r_0666","r_0668","r_0670","r_0671","r_0672","r_0673","r_0675","r_0676","r_0678","r_0679","r_0680","r_0681","r_0682","r_0683","r_0684","r_0685","r_0686","r_0687","r_0688","r_0689","r_0690","r_0691","r_0692","r_0693","r_0694","r_0695","r_0696","r_0697","r_0700","r_0701","r_0702","r_0703","r_0704","r_0705","r_0706","r_0707","r_0708","r_0709","r_0710","r_0711","r_0712","r_0714","r_0715","r_0717","r_0718","r_0719","r_0720","r_0721","r_0724","r_0728","r_0729","r_0730","r_0731","r_0733","r_0734","r_0735","r_0737","r_0738","r_0740","r_0741","r_0742","r_0743","r_0744","r_0745","r_0746","r_0747","r_0748","r_0749","r_0750","r_0751","r_0752","r_0753","r_0754","r_0756","r_0760","r_0761","r_0762","r_0763","r_0764","r_0765","r_0766","r_0768","r_0769","r_0772","r_0774","r_0775","r_0776","r_0777","r_0778","r_0779","r_0780","r_0781","r_0782","r_0783","r_0784","r_0785","r_0786","r_0787","r_0788","r_0789","r_0790","r_0791","r_0793","r_0794","r_0795","r_0796","r_0797","r_0798","r_0799","r_0801","r_0802","r_0803","r_0804","r_0805","r_0807","r_0808","r_0809","r_0810","r_0812","r_0814","r_0815","r_0817","r_0819","r_0830","r_0833","r_0834","r_0835","r_0836","r_0837","r_0838","r_0839","r_0840","r_0841","r_0842","r_0843","r_0844","r_0845","r_0846","r_0847","r_0848","r_0849","r_0850","r_0852","r_0853","r_0854","r_0856","r_0857","r_0859","r_0860","r_0861","r_0862","r_0863","r_0864","r_0865","r_0866","r_0867","r_0868","r_0869","r_0870","r_0871","r_0872","r_0873","r_0875","r_0876","r_0877","r_0879","r_0881","r_0882","r_0884","r_0885","r_0886","r_0890","r_0894","r_0895","r_0896","r_0897","r_0898","r_0899","r_0903","r_0905","r_0906","r_0907","r_0920","r_0921","r_0923","r_0924","r_0925","r_0926","r_0927","r_0928","r_0929","r_0930","r_0931","r_0932","r_0933","r_0934","r_0935","r_0936","r_0937","r_0940","r_0941","r_0942","r_0943","r_0944","r_0945","r_0946","r_0947","r_0948","r_0949","r_0950","r_0951","r_0952","r_0953","r_0954","r_0955","r_0956","r_0960","r_0963","r_0964","r_0965","r_0966","r_0969","r_0970","r_0971","r_0972","r_0975","r_0976","r_0977","r_0979","r_0980","r_0981","r_0983","r_0985","r_0987","r_0991","r_0992","r_0994","r_0995","r_0998","r_0999","r_1000","r_1001","r_1002","r_1003","r_1004","r_1005","r_1006","r_1007","r_1008","r_1009","r_1011","r_1013","r_1015","r_1016","r_1018","r_1019","r_1020","r_1023","r_1024","r_1025","r_1028","r_1029","r_1030","r_1031","r_1032","r_1033","r_1034","r_1035","r_1036","r_1037","r_1039","r_1040","r_1042","r_1043","r_1044","r_1046","r_1047","r_1056","r_1057","r_1058","r_1059","r_1060","r_1061","r_1062","r_1064","r_1065","r_1066","r_1067","r_1068","r_1069","r_1070","r_1071","r_1073","r_1074","r_1075","r_1076","r_1077","r_1078","r_1079","r_1080","r_1081","r_1082","r_1083","r_1085","r_1086","r_1089","r_1090","r_1091","r_1092","r_1093","r_1094","r_1095","r_1096","r_1097","r_1098","r_1100","r_1101","r_1102","r_1103","r_1104","r_1107","r_1108","r_1109","r_1111","r_1112","r_1113","r_1114","r_1118","r_1119","r_1120","r_1121","r_1122","r_1123","r_1124","r_1125","r_1129","r_1131","r_1132","r_1133","r_1134","r_1135","r_1136","r_1139","r_1140","r_1141","r_1142","r_1143","r_1146","r_1147","r_1149","r_1150","r_1151","r_1154","r_1155","r_1156","r_1157","r_1158","r_1159","r_1160","r_1161","r_1162","r_1163","r_1164","r_1165","r_1167","r_1168","r_1169","r_1170","r_1173","r_1174","r_1175","r_1176","r_1177","r_1178","r_1179","r_1180","r_1181","r_1182","r_1183","r_1184","r_1185","r_1186","r_1187","r_1188","r_1189","r_1190","r_1191","r_1192","r_1193","r_1195","r_1196","r_1197","r_1198","r_1199","r_1200","r_1201","r_1202","r_1203","r_1204","r_1205","r_1206","r_1207","r_1208","r_1209","r_1210","r_1211","r_1212","r_1213","r_1214","r_1215","r_1216","r_1217","r_1218","r_1219","r_1220","r_1221","r_1222","r_1223","r_1224","r_1225","r_1227","r_1228","r_1229","r_1230","r_1231","r_1232","r_1233","r_1234","r_1235","r_1236","r_1238","r_1240","r_1241","r_1242","r_1243","r_1246","r_1247","r_1248","r_1249","r_1252","r_1253","r_1254","r_1255","r_1257","r_1258","r_1261","r_1262","r_1263","r_1264","r_1267","r_1268","r_1269","r_1270","r_1271","r_1272","r_1273","r_1274","r_1275","r_1276","r_1278","r_1279","r_1280","r_1281","r_1282","r_1283","r_1284","r_1285","r_1286","r_1287","r_1288","r_1289","r_1290","r_1291","r_1292","r_1293","r_1294","r_1295","r_1296","r_1297","r_1299","r_1300","r_1301","r_1302","r_1303","r_1304","r_1305","r_1306","r_1307","r_1308","r_1309","r_1310","r_1311","r_1312","r_1313","r_1314","r_1315","r_1316","r_1317","r_1318","r_1319","r_1320","r_1321","r_1322","r_1323","r_1324","r_1325","r_1326","r_1327","r_1328","r_1329","r_1330","r_1331","r_1332","r_1333","r_1334","r_1335","r_1336","r_1337","r_1338","r_1339","r_1340","r_1341","r_1342","r_1343","r_1344","r_1345","r_1346","r_1347","r_1348","r_1349","r_1350","r_1351","r_1352","r_1353","r_1354","r_1355","r_1356","r_1357","r_1360","r_1361","r_1362","r_1363","r_1364","r_1365","r_1366","r_1367","r_1368","r_1369","r_1370","r_1371","r_1372","r_1373","r_1374","r_1375","r_1376","r_1377","r_1378","r_1379","r_1380","r_1381","r_1382","r_1383","r_1384","r_1385","r_1386","r_1387","r_1388","r_1389","r_1390","r_1391","r_1392","r_1393","r_1394","r_1395","r_1396","r_1397","r_1398","r_1399","r_1400","r_1401","r_1402","r_1403","r_1404","r_1405","r_1406","r_1407","r_1408","r_1409","r_1410","r_1411","r_1412","r_1413","r_1414","r_1415","r_1416","r_1417","r_1418","r_1419","r_1420","r_1421","r_1422","r_1423","r_1424","r_1425","r_1426","r_1427","r_1428","r_1429","r_1430","r_1431","r_1432","r_1433","r_1434","r_1435","r_1436","r_1437","r_1438","r_1439","r_1440","r_1441","r_1442","r_1443","r_1444","r_1445","r_1447","r_1448","r_1449","r_1450","r_1451","r_1452","r_1453","r_1454","r_1455","r_1456","r_1457","r_1458","r_1459","r_1460","r_1461","r_1462","r_1463","r_1464","r_1465","r_1466","r_1467","r_1468","r_1469","r_1470","r_1471","r_1472","r_1473","r_1474","r_1475","r_1476","r_1477","r_1478","r_1479","r_1480","r_1481","r_1482","r_1483","r_1484","r_1485","r_1486","r_1488","r_1489","r_1490","r_1491","r_1492","r_1493","r_1494","r_1495","r_1496","r_1497","r_1498","r_1499","r_1500","r_1501","r_1502","r_1503","r_1504","r_1505","r_1506","r_1507","r_1508","r_1509","r_1510","r_1511","r_1512","r_1513","r_1514","r_1515","r_1516","r_1518","r_1519","r_1520","r_1521","r_1522","r_1523","r_1524","r_1525","r_1526","r_1527","r_1528","r_1529","r_1530","r_1531","r_1532","r_1533","r_1534","r_1535","r_1536","r_1537","r_1538","r_1539","r_1540","r_1541","r_1542","r_1544","r_1545","r_1546","r_1547","r_1548","r_1549","r_1550","r_1551","r_1552","r_1553","r_1554","r_1555","r_1556","r_1557","r_1560","r_1561","r_1562","r_1563","r_1564","r_1565","r_1566","r_1568","r_1569","r_1570","r_1571","r_1572","r_1573","r_1574","r_1575","r_1576","r_1577","r_1578","r_1579","r_1580","r_1581","r_1582","r_1583","r_1586","r_1587","r_1589","r_1590","r_1591","r_1592","r_1593","r_1594","r_1595","r_1597","r_1598","r_1599","r_1600","r_1601","r_1602","r_1603","r_1604","r_1605","r_1606","r_1607","r_1608","r_1609","r_1610","r_1611","r_1612","r_1613","r_1614","r_1615","r_1616","r_1617","r_1618","r_1619","r_1620","r_1621","r_1622","r_1623","r_1624","r_1625","r_1626","r_1627","r_1628","r_1629","r_1630","r_1631","r_1633","r_1634","r_1635","r_1637","r_1638","r_1639","r_1640","r_1641","r_1642","r_1643","r_1644","r_1645","r_1647","r_1648","r_1649","r_1650","r_1651","r_1653","r_1656","r_1657","r_1658","r_1659","r_1660","r_1661","r_1663","r_1664","r_1666","r_1668","r_1670","r_1671","r_1673","r_1674","r_1675","r_1676","r_1677","r_1679","r_1680","r_1681","r_1682","r_1683","r_1684","r_1685","r_1686","r_1687","r_1688","r_1689","r_1690","r_1693","r_1695","r_1699","r_1702","r_1703","r_1704","r_1705","r_1706","r_1707","r_1709","r_1710","r_1711","r_1712","r_1713","r_1715","r_1716","r_1717","r_1718","r_1719","r_1720","r_1721","r_1722","r_1723","r_1724","r_1725","r_1726","r_1727","r_1728","r_1730","r_1731","r_1732","r_1733","r_1734","r_1735","r_1736","r_1737","r_1738","r_1739","r_1740","r_1743","r_1744","r_1745","r_1746","r_1749","r_1750","r_1751","r_1752","r_1753","r_1757","r_1763","r_1764","r_1765","r_1766","r_1767","r_1770","r_1771","r_1772","r_1773","r_1774","r_1775","r_1776","r_1777","r_1780","r_1781","r_1782","r_1783","r_1784","r_1785","r_1786","r_1787","r_1788","r_1789","r_1790","r_1791","r_1792","r_1794","r_1796","r_1797","r_1798","r_1799","r_1800","r_1802","r_1804","r_1805","r_1806","r_1807","r_1809","r_1810","r_1811","r_1812","r_1813","r_1814","r_1815","r_1816","r_1817","r_1818","r_1819","r_1820","r_1821","r_1822","r_1823","r_1827","r_1829","r_1831","r_1834","r_1835","r_1836","r_1837","r_1839","r_1840","r_1841","r_1842","r_1843","r_1844","r_1845","r_1846","r_1847","r_1848","r_1849","r_1850","r_1851","r_1852","r_1853","r_1854","r_1855","r_1856","r_1857","r_1858","r_1859","r_1860","r_1861","r_1862","r_1863","r_1864","r_1865","r_1866","r_1867","r_1868","r_1869","r_1870","r_1871","r_1872","r_1873","r_1875","r_1876","r_1877","r_1878","r_1879","r_1880","r_1881","r_1882","r_1883","r_1884","r_1885","r_1886","r_1888","r_1889","r_1890","r_1891","r_1892","r_1893","r_1894","r_1895","r_1896","r_1897","r_1899","r_1900","r_1902","r_1903","r_1904","r_1905","r_1906","r_1908","r_1909","r_1910","r_1911","r_1912","r_1913","r_1914","r_1915","r_1916","r_1917","r_1918","r_1919","r_1920","r_1921","r_1922","r_1923","r_1924","r_1925","r_1926","r_1927","r_1928","r_1929","r_1930","r_1931","r_1933","r_1934","r_1935","r_1936","r_1937","r_1938","r_1939","r_1940","r_1941","r_1942","r_1943","r_1944","r_1945","r_1946","r_1947","r_1949","r_1950","r_1951","r_1952","r_1953","r_1954","r_1959","r_1960","r_1965","r_1967","r_1968","r_1969","r_1970","r_1971","r_1972","r_1974","r_1975","r_1976","r_1980","r_1981","r_1982","r_1983","r_1984","r_1985","r_1986","r_1987","r_1988","r_1989","r_1990","r_1991","r_1993","r_1994","r_1996","r_1997","r_1998","r_1999","r_2000","r_2001","r_2002","r_2003","r_2004","r_2006","r_2007","r_2008","r_2009","r_2010","r_2013","r_2015","r_2018","r_2019","r_2020","r_2021","r_2022","r_2023","r_2024","r_2025","r_2026","r_2027","r_2028","r_2029","r_2031","r_2033","r_2035","r_2036","r_2037","r_2038","r_2039","r_2040","r_2041","r_2043","r_2044","r_2046","r_2047","r_2049","r_2050","r_2051","r_2052","r_2055","r_2056","r_2057","r_2058","r_2059","r_2061","r_2062","r_2064","r_2065","r_2066","r_2067","r_2068","r_2069","r_2070","r_2071","r_2072","r_2073","r_2074","r_2075","r_2076","r_2077","r_2078","r_2079","r_2080","r_2082","r_2083","r_2084","r_2085","r_2086","r_2087","r_2088","r_2089","r_2090","r_2091","r_2092","r_2095","r_2097","r_2098","r_2099","r_2101","r_2102","r_2103","r_2104","r_2105","r_2106","r_2107","r_2109"};
      for (std::string reactionName : reactionsToFix)
      {
         HEdge edge = *this->_graph.getEdge(reactionName);
         auto fixconstraint = hyperstory.add(deletionsArray[edge.getId()] == 0);
         std::string cname = "FIX_"+edge.getName();
         fixconstraint.setName(cname.c_str());
      }
      for(unsigned int i=0;i<nlinesS;++i)
      {
         bool addSSConstraint = false;
         IloNumExpr variationOfCompound(_env);
         for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
         {
            int edge_id = edge_it->first;
            HEdge edge = edge_it->second;
            const list<int> sources = edge.getTailNodes();
            const list<double> sourcesStoich = edge.getTailStoichiometry();
            list<int>::const_iterator it1;
            list<double>::const_iterator it2;
            for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound -= nodeStoich*fluxArray[edge_id];
               }
            }
            list<int> products = edge.getHeadNodes();
            list<double> productsStoich = edge.getHeadStoichiometry();
            for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound += nodeStoich*fluxArray[edge_id];
               }
            }
         }
         // Sv[i] = b[i]
         if (addSSConstraint)
         {
            if (!this->_graph.getNode(i).isBoundary())
            {
               std::string svbname = "(Sv=b)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
               auto svconstraint = hyperstory.add(variationOfCompound == 0);
               svconstraint.setName(svbname.c_str());
            }
            //            else
            //            {
            //
            //               std::string svbname = "(Sv=b)_"+std::to_string(i);
            //               auto svconstraint = hyperstory.add(variationOfCompound >= 0);
            //               svconstraint.setName(svbname.c_str());
            //            }
         }

      }

      set<int> constrainedReactions;
      // Constraints for reactions
      for (Bounds b : reactionBounds)
      {
         unsigned int j = b.id;
         assert(j<ncolsS);

         IloNumExpr lBound(_env);
         IloNumExpr uBound(_env);

         lBound += b.lb;
         uBound += b.ub;
         std::string blbname = "b_lo_bound_"+std::to_string(j);
         auto lbconstraint = hyperstory.add(lBound*(1-deletionsArray[j]) <= fluxArray[j]);
         lbconstraint.setName(blbname.c_str());
         std::string bubname = "b_up_bound_"+std::to_string(j);
         auto ubconstraint = hyperstory.add(uBound*(1-deletionsArray[j]) >= fluxArray[j]);
         ubconstraint.setName(bubname.c_str());
         constrainedReactions.insert(j);
      }

      for(unsigned int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string vname = "v_"+std::to_string(j);
            fluxArray[j] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());

            IloNumExpr fluxLoBound(_env);
            fluxLoBound += fluxArray[j];
            std::string flbname = "flux_lo_bound_"+std::to_string(j);
            auto flbconstraint = hyperstory.add(fluxLoBound >= -100000*(1-deletionsArray[j]));
            flbconstraint.setName(flbname.c_str());

            IloNumExpr fluxGeBound(_env);
            fluxGeBound += fluxArray[j];
            std::string fubname = "flux_up_bound_"+std::to_string(j);
            auto fubconstraint = hyperstory.add(fluxGeBound <= 100000*(1-deletionsArray[j]));
            fubconstraint.setName(fubname.c_str());
         }
      }

      // Objective function
      IloNumExpr objective(_env);
      for (HEdge e : reactionsToProduce)
      {
         objective += fluxArray[e.getId()];
      }
      //hyperstory.add(objective >= 0.01);
      hyperstory.add(IloMaximize(_env, objective));
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   try{
      IloCplex solver(_env);
      solver.setOut(_env.getNullStream());
      solver.setWarning(_env.getNullStream());
      solver.setParam(IloCplex::WorkDir,".");
      solver.setParam(IloCplex::WorkMem,5000);
      solver.setParam(IloCplex::NodeFileInd,3);
      solver.setParam(IloCplex::TiLim, 21600);
      solver.setParam(IloCplex::EpGap, 1e-7);
      //solver.setParam(IloCplex::PreInd, false);

      //cout << "[###] The model has (without presolve):" << endl;
      //cout << "[###] " << numVars << " variables;" << endl;
      //cout << "[###] " << numRestricoes << " constraints;" << endl;

      //      IloExtractable fubconstraint;
      //      for (auto it : this->_graph.getEdges())
      //      {
      //         HEdge e = it.second;
      //         int j = e.getId();
      //         IloNumExpr fluxGeBound(_env);
      //         fluxGeBound += fluxArray[j];
      //         fubconstraint = hyperstory.add(fluxGeBound <= 0.2);

      solver.extract(hyperstory);
      solver.exportModel("/tmp/fbaBiomassEthanolGlycerol.lp");
      solver.solve();

      //cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

      if ( solver.getStatus() == Optimal )
      {
         // Get the solution

         std::vector<double> fluxesSol = std::vector<double>(ncolsS);
         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = fluxesSolTmp[l];
            //cout << this->_graph.getEdge(l).getName() << "\t" << fluxesSol[l] << endl;
         }
         cout << "Biomass: " << fluxesSol[2108] << ", Ethanol: " << fluxesSol[1760] << "Glycerol: " << fluxesSol[1807] <<   endl;
         /*for(unsigned int l=0;l<ncolsS;++l)
            {
               if (fluxesSol[l] != 0 )
               {
                  HEdge edge = this->_graph.getEdge(l);
                  cout << edge.getDescription() << " (" << edge.getName() <<") = " << fluxesSol[l] << endl;
               }
            }*/
      }
      // Enumeration
      /*std::string outFilename = "/tmp/point_" + std::to_string(biomassProduction) + "b_" + std::to_string(ethanolProduction) + "e_" + std::to_string(glycerolProduction) + "g.txt";
      FILE *solutionFile = fopen (outFilename.c_str(), "w");
      while ( solver.getStatus() == Optimal )
      {
         int idToFixToZero = -1;
         // Get the solution
         IloNumArray deletionsSolTmp(_env,ncolsS);
         solver.getValues(deletionsSolTmp,deletionsArray);

         for(unsigned int l=0;l<ncolsS;++l){
            if (deletionsSolTmp[l] + 0.5 > 1){
               HEdge e = this->_graph.getEdge(l);
               std::cout << "(" << e.getName() << ") " << e.getDescription() << std::endl;
               std::string solutionLine = "(" + e.getName() + ") " + e.getDescription() + "\n";
               fprintf (solutionFile,solutionLine.c_str(),"%s");
               idToFixToZero = l;
            }
         }
         // Add the constraint
         assert(idToFixToZero >= 0);
         hyperstory.add(deletionsArray[idToFixToZero] == 0);
         solver.extract(hyperstory);
         solver.solve();
      }
      fclose(solutionFile);*/
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }
}

void CplexFBAModeler::doMOMAFixingBiomassAndEthanolExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, double biomassProduction, double ethanolProduction, vector<double>  scores)
{
   IloNumExpr objective(_env);

   // Number of lines and columns
   const unsigned int ncolsS = _graph.getNbEdges();
   const unsigned int nlinesS = _graph.getNbNodes();
   cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   IloNumArray inputDeltas(_env,nlinesS);
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      inputDeltas[id] = 0.0;
   }
   for (const int id : _graph.getBlackNodes())
   {
      inputDeltas[id] = 0.0;
   }
   IloModel hyperstory(_env);
   IloNumVarArray fluxArray(_env,ncolsS);
   IloNumVarArray momaDiffArray(_env,ncolsS);
   IloNumVarArray deletionsArray(_env,ncolsS);


   try{
      // Model variables
      // For each flux
      for(unsigned int i=0;i<ncolsS;++i)
      {
         std::string vname = "v_"+std::to_string(i);
         fluxArray[i] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());
         std::string yname = "y_"+std::to_string(i);
         deletionsArray[i] = IloNumVar(_env,0,1,ILOBOOL,yname.c_str());
         std::string mname = "momaDiffArray_"+std::to_string(i);
         momaDiffArray[i] = IloNumVar(_env,0.0,IloInfinity,ILOFLOAT,mname.c_str());
      }
      hyperstory.add(fluxArray);
      hyperstory.add(deletionsArray);

      // Constraints
      for(unsigned int i=0;i<nlinesS;++i)
      {
         bool addSSConstraint = false;
         IloNumExpr variationOfCompound(_env);
         for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
         {
            int edge_id = edge_it->first;
            HEdge edge = edge_it->second;
            const list<int> sources = edge.getTailNodes();
            const list<double> sourcesStoich = edge.getTailStoichiometry();
            list<int>::const_iterator it1;
            list<double>::const_iterator it2;
            for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound -= nodeStoich*fluxArray[edge_id];
               }
            }
            list<int> products = edge.getHeadNodes();
            list<double> productsStoich = edge.getHeadStoichiometry();
            for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound += nodeStoich*fluxArray[edge_id];
               }
            }
         }
         // Sv[i] = b[i]
         if (addSSConstraint)
         {
            if (!this->_graph.getNode(i).isBoundary())
            {
               std::string svbname = "(Sv=b)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
               auto svconstraint = hyperstory.add(variationOfCompound == 0);
               svconstraint.setName(svbname.c_str());
            }
         }
      }


      // Fixing the rates of biomass and ethanol production
      HEdge biomass = *this->_graph.getEdge("r_2110");
      auto bioConstraint = hyperstory.add(fluxArray[biomass.getId()] >= biomassProduction);
      bioConstraint.setName("BiomassProd");
      HEdge ethanol = *this->_graph.getEdge("r_1761");
      auto ethanolConstraint = hyperstory.add(fluxArray[ethanol.getId()] == ethanolProduction);
      ethanolConstraint.setName("EthanolProd");
      //HEdge gly = *this->_graph.getEdge("r_1808");
      //auto glyConstraint = hyperstory.add(fluxArray[gly.getId()] == 0);
      //glyConstraint.setName("GlycerolProd");


      // Fixing hte number of fixed deletion to one
      IloNumExpr numberOfDeletions(_env);
      for(unsigned int i=0;i<ncolsS;++i)
      {
         numberOfDeletions += deletionsArray[i];
      }
      std::string svbname = "MaxOfDeletions";
      auto delconstraint = hyperstory.add(numberOfDeletions == 1);
      delconstraint.setName(svbname.c_str());

      // Fixing some Y to zero because we don't want to delete those reactions
      /*
      std::string reactionsToFix[] = {"r_1542","r_1545","r_1546","r_1547","r_1548","r_1549","r_1550","r_1551","r_1552","r_1553","r_1554","r_1563","r_1564","r_1565","r_1566","r_1571","r_1572","r_1577","r_1580","r_1581","r_1586","r_1589","r_1598","r_1604","r_1621","r_1625","r_1627","r_1629","r_1630","r_1631","r_1634","r_1639","r_1641","r_1643","r_1648","r_1649","r_1650","r_1651","r_1654","r_1663","r_1671","r_1672","r_1683","r_1687","r_1702","r_1705","r_1706","r_1709","r_1710","r_1711","r_1712","r_1714","r_1715","r_1716","r_1718","r_1727","r_1730","r_1749","r_1753","r_1757","r_1761","r_1764","r_1765","r_1767","r_1788","r_1791","r_1792","r_1793","r_1798","r_1799","r_1800","r_1806","r_1807","r_1808","r_1810","r_1814","r_1815","r_1818","r_1820","r_1831","r_1833","r_1840","r_1842","r_1846","r_1860","r_1861","r_1864","r_1865","r_1866","r_1869","r_1872","r_1874","r_1877","r_1878","r_1879","r_1880","r_1882","r_1885","r_1888","r_1890","r_1892","r_1895","r_1896","r_1898","r_1899","r_1901","r_1902","r_1903","r_1905","r_1908","r_1910","r_1911","r_1912","r_1913","r_1914","r_1915","r_1916","r_1917","r_1930","r_1932","r_1946","r_1948","r_1950","r_1951","r_1961","r_1966","r_1967","r_1983","r_1984","r_1986","r_1988","r_1991","r_1992","r_1993","r_1998","r_1999","r_2000","r_2004","r_2008","r_2018","r_2019","r_2023","r_2027","r_2032","r_2037","r_2042","r_2043","r_2045","r_2048","r_2050","r_2051","r_2054","r_2055","r_2057","r_2059","r_2060","r_2061","r_2065","r_2066","r_2067","r_2072","r_2075","r_2082","r_2089","r_2090","r_2091","r_2099","r_2101","r_2103","r_2105","r_2109","r_2110"};
            for (std::string reactionName : reactionsToFix)
            {
               HEdge edge = *this->_graph.getEdge(reactionName);
               auto fixconstraint = hyperstory.add(deletionsArray[edge.getId()] == 0);
               std::string cname = "FIX_"+edge.getName();
               fixconstraint.setName(cname.c_str());
      }
      */
      for(unsigned int i=0;i<ncolsS;++i)
      {
         if (referenceFluxVector[i] == 0)
         {
            auto fixconstraint = hyperstory.add(deletionsArray[i] == 0);
            std::string cname = "FIX_at_zero_"+_graph.getEdge(i).getName();
            fixconstraint.setName(cname.c_str());
         }
         /*else
         {
            std::cout << "HERE " << _graph.getEdge(i).getName() << std::endl;
         }*/
      }
      std::string reactionsToFix[] = {"r_1761", "r_2110"};
      for (std::string reactionName : reactionsToFix)
      {
         HEdge edge = *this->_graph.getEdge(reactionName);
         auto fixconstraint = hyperstory.add(deletionsArray[edge.getId()] == 0);
         std::string cname = "FIX_at_zero_"+reactionName;
         fixconstraint.setName(cname.c_str());
      }

      set<int> constrainedReactions;
      // Constraints for reactions deletions
      for (Bounds b : reactionBounds)
      {
         unsigned int j = b.id;
         assert(j<ncolsS);

         IloNumExpr lBound(_env);
         IloNumExpr uBound(_env);

         lBound += b.lb;
         uBound += b.ub;
         std::string blbname = "b_lo_bound_"+std::to_string(j);
         auto lbconstraint = hyperstory.add(lBound*(1-deletionsArray[j]) <= fluxArray[j]);
         lbconstraint.setName(blbname.c_str());
         std::string bubname = "b_up_bound_"+std::to_string(j);
         auto ubconstraint = hyperstory.add(uBound*(1-deletionsArray[j]) >= fluxArray[j]);
         ubconstraint.setName(bubname.c_str());
         constrainedReactions.insert(j);
      }

      // Unconstrained reactions should also be bounded to the Y variables
      for(unsigned int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string vname = "v_"+std::to_string(j);
            fluxArray[j] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());

            IloNumExpr fluxLoBound(_env);
            fluxLoBound += fluxArray[j];
            std::string flbname = "flux_lo_bound_"+std::to_string(j);
            auto flbconstraint = hyperstory.add(fluxLoBound >= -100000*(1-deletionsArray[j]));
            flbconstraint.setName(flbname.c_str());

            IloNumExpr fluxGeBound(_env);
            fluxGeBound += fluxArray[j];
            std::string fubname = "flux_up_bound_"+std::to_string(j);
            auto fubconstraint = hyperstory.add(fluxGeBound <= 100000*(1-deletionsArray[j]));
            fubconstraint.setName(fubname.c_str());
         }
      }

      // Setting momaDiffArray as the module of
      // the difference of the fluxArray and
      // the referenceFluxVector
      assert(referenceFluxVector.size() == ncolsS);
      for(unsigned int i=0;i<ncolsS;++i)
      {
         IloNumExpr momaModule(_env);
         IloNum referenceValue = referenceFluxVector[i];
         momaModule += fluxArray[i] - referenceValue;
         std::string cname = "MomaModuleDiff1_" + std::to_string(i) ;
         auto momaModuleConstraint1 = hyperstory.add(momaDiffArray[i] >= momaModule);
         momaModuleConstraint1.setName(cname.c_str());
         cname = "MomaModuleDiff2_" + std::to_string(i) ;
         auto momaModuleConstraint2 = hyperstory.add(momaDiffArray[i] >= -momaModule);
         momaModuleConstraint2.setName(cname.c_str());
      }

      // Objective function
      for (unsigned int j=0;j<ncolsS;++j)
      {
         objective += momaDiffArray[j];
      }
      hyperstory.add(IloMinimize(_env, objective));
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   try{
      IloCplex solver(_env);
      //solver.setOut(_env.getNullStream());
      //solver.setWarning(_env.getNullStream());
      solver.setParam(IloCplex::WorkDir,".");
      solver.setParam(IloCplex::WorkMem,5000);
      solver.setParam(IloCplex::NodeFileInd,3);
      //solver.setParam(IloCplex::TiLim, 21600);
      solver.setParam(IloCplex::EpGap, 1e-9);
      solver.setParam(IloCplex::PreInd, false);
      solver.setParam(IloCplex::EpAGap, 1e-9);
      solver.setParam(IloCplex::EpOpt, 1e-9);
      solver.setParam(IloCplex::EpLin, 1e-9);
      solver.setParam(IloCplex::EpInt, 1e-9);
      solver.setParam(IloCplex::EpRHS, 1e-9);
      solver.setParam(IloCplex::NumericalEmphasis, true);
      solver.writeParam("/tmp/cplexParams.prm");

      //cout << "[###] The model has (without presolve):" << endl;
      //cout << "[###] " << numVars << " variables;" << endl;
      //cout << "[###] " << numRestricoes << " constraints;" << endl;

      //      IloExtractable fubconstraint;
      //      for (auto it : this->_graph.getEdges())
      //      {
      //         HEdge e = it.second;
      //         int j = e.getId();
      //         IloNumExpr fluxGeBound(_env);
      //         fluxGeBound += fluxArray[j];
      //         fubconstraint = hyperstory.add(fluxGeBound <= 0.2);

      solver.extract(hyperstory);
      solver.exportModel("/tmp/fbaBiomassEthanol.lp");
      solver.solve();

      //cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

      double glycerolProduction = -1;
      double optimalMomaValue = solver.getObjValue();
      bool addedOptimalBound = false;

      if ( solver.getStatus() == Optimal )
      {
         // Get the solution
         std::vector<double> fluxesSol = std::vector<double>(ncolsS);
         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = fluxesSolTmp[l];
            //cout << this->_graph.getEdge(l).getName() << "\t" << fluxesSol[l] << endl;
         }
         cout << "Biomass: " << fluxesSol[2108] << ", Ethanol: " << fluxesSol[1760] << "Glycerol: " << fluxesSol[1807] <<   endl;
         glycerolProduction = fluxesSol[1807];
         /*for(unsigned int l=0;l<ncolsS;++l)
            {
               if (fluxesSol[l] != 0 )
               {
                  HEdge edge = this->_graph.getEdge(l);
                  cout << edge.getDescription() << " (" << edge.getName() <<") = " << fluxesSol[l] << endl;
               }
            }*/

         cout << "[DEBUG] Optimal value: " << optimalMomaValue << endl;

      }
      else
      {
         cout << "No solution found." << endl;
      }

      std::string outFilename = "/tmp/point_MOMA_Bi_" + std::to_string(biomassProduction) + "b_" + std::to_string(ethanolProduction) + "e_" + std::to_string(glycerolProduction) + "g.txt";
      FILE *solutionFile = fopen (outFilename.c_str(), "w");
      while ( solver.getStatus() == Optimal )
      {
         int idToFixToZero = -1;
         int numberOfZeroFluxes = 0;
         std::vector<double> fluxesSol = std::vector<double>(ncolsS);
         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = fluxesSolTmp[l];
            if (fluxesSol[l] == 0.0)
            {
               ++numberOfZeroFluxes;
            }
         }
         //std::cout << "Solution with " << numberOfZeroFluxes << " zero reactions." << std::endl;

         // Get the solution
         IloNumArray deletionsSolTmp(_env,ncolsS);
         solver.getValues(deletionsSolTmp,deletionsArray);
         for(unsigned int l=0;l<ncolsS;++l){
            if (deletionsSolTmp[l] + 0.5 > 1){
               HEdge e = this->_graph.getEdge(l);
               //std::cout << "Delete reaction " << e.getName() << " (MOMA value: " << solver.getObjValue() << ")" << std::endl;
               //std::cout << e.getName() << ";" << scores[e.getId()] << std::endl;
               std::stringstream solutionLine;
               //solutionLine << e.getName() << " ; " << scores[e.getId()] << "\n";
               solutionLine << e.getName() << " ; " << "\n";
               fprintf (solutionFile,solutionLine.str().c_str(),"%s");
               idToFixToZero = l;
               break;
            }
         }
         // Check we are not getting something strange
         //cout << optimalMomaValue - solver.getObjValue() << endl;
         assert(abs(optimalMomaValue - solver.getObjValue()) <= 1e-7);

         // Add the constraint
         assert(idToFixToZero >= 0);
         if (!addedOptimalBound)
         {
            // Fixing the MOMA value to the optimal for enumeration
            hyperstory.add(objective <= optimalMomaValue);
            addedOptimalBound = true;
         }
         hyperstory.add(deletionsArray[idToFixToZero] == 0);
         solver.extract(hyperstory);
         solver.exportModel("/tmp/fbaBiomassEthanol.lp");
         solver.solve();
      }
      fclose(solutionFile);

   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }
}

void CplexFBAModeler::doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, double biomassProduction, double ethanolProduction, double glycerolProduction, vector<double>  scores)
{
   IloNumExpr objective(_env);

   // Number of lines and columns
   const unsigned int ncolsS = _graph.getNbEdges();
   const unsigned int nlinesS = _graph.getNbNodes();
   cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   IloNumArray inputDeltas(_env,nlinesS);
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      inputDeltas[id] = 0.0;
   }
   for (const int id : _graph.getBlackNodes())
   {
      inputDeltas[id] = 0.0;
   }
   IloModel hyperstory(_env);
   IloNumVarArray fluxArray(_env,ncolsS);
   IloNumVarArray momaDiffArray(_env,ncolsS);
   IloNumVarArray deletionsArray(_env,ncolsS);


   try{
      // Model variables
      // For each flux
      for(unsigned int i=0;i<ncolsS;++i)
      {
         std::string vname = "v_"+std::to_string(i);
         fluxArray[i] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());
         std::string yname = "y_"+std::to_string(i);
         deletionsArray[i] = IloNumVar(_env,0,1,ILOBOOL,yname.c_str());
         std::string mname = "momaDiffArray_"+std::to_string(i);
         momaDiffArray[i] = IloNumVar(_env,0.0,IloInfinity,ILOFLOAT,mname.c_str());
      }
      hyperstory.add(fluxArray);
      hyperstory.add(deletionsArray);

      // Constraints
      // Fixing hte number of fixed deletion to one
      IloNumExpr numberOfDeletions(_env);
      for(unsigned int i=0;i<ncolsS;++i)
      {
         numberOfDeletions += deletionsArray[i];
      }
      std::string svbname = "NumberOfSingleDeletions";
      auto delconstraint = hyperstory.add(numberOfDeletions == 1);
      delconstraint.setName(svbname.c_str());

      // Setting momaDiffArray as the module of
      // the difference of the fluxArray and
      // the referenceFluxVector
      assert(referenceFluxVector.size() == ncolsS);
      for(unsigned int i=0;i<ncolsS;++i)
      {
         IloNumExpr momaModule(_env);
         IloNum referenceValue = referenceFluxVector[i];
         momaModule += fluxArray[i] - referenceValue;
         std::string cname = "MomaModuleDiff1_" + std::to_string(i) ;
         auto momaModuleConstraint1 = hyperstory.add(momaDiffArray[i] >= momaModule);
         momaModuleConstraint1.setName(cname.c_str());
         cname = "MomaModuleDiff2_" + std::to_string(i) ;
         auto momaModuleConstraint2 = hyperstory.add(momaDiffArray[i] >= -momaModule);
         momaModuleConstraint2.setName(cname.c_str());
      }

      // Fixing the rates of biomass and ethanol production
      HEdge biomass = *this->_graph.getEdge("r_2110");
      auto bioConstraint = hyperstory.add(fluxArray[biomass.getId()] >= biomassProduction);
      bioConstraint.setName("BiomassProd");
      HEdge ethanol = *this->_graph.getEdge("r_1761");
      auto ethanolConstraint = hyperstory.add(fluxArray[ethanol.getId()] >= ethanolProduction);
      ethanolConstraint.setName("EthanolProd");
      HEdge gly = *this->_graph.getEdge("r_1808");
      auto glyConstraint = hyperstory.add(fluxArray[gly.getId()] <= glycerolProduction);
      glyConstraint.setName("GlycerolProd");

      // Fixing some Y to zero because we don't want to delete those reactions
      for(unsigned int i=0;i<ncolsS;++i)
      {
         if (referenceFluxVector[i] == 0)
         {
            auto fixconstraint = hyperstory.add(deletionsArray[i] == 0);
            std::string cname = "FIX_REF_"+_graph.getEdge(i).getName();
            fixconstraint.setName(cname.c_str());
         }
      }
      std::string reactionsToFix[] = {"r_1761", "r_1808", "r_2110"};
      for (std::string reactionName : reactionsToFix)
      {
         HEdge edge = *this->_graph.getEdge(reactionName);
         auto fixconstraint = hyperstory.add(deletionsArray[edge.getId()] == 0);
         std::string cname = "FIX_"+edge.getName();
         fixconstraint.setName(cname.c_str());
      }

      for(unsigned int i=0;i<nlinesS;++i)
      {
         bool addSSConstraint = false;
         IloNumExpr variationOfCompound(_env);
         for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
         {
            int edge_id = edge_it->first;
            HEdge edge = edge_it->second;
            const list<int> sources = edge.getTailNodes();
            const list<double> sourcesStoich = edge.getTailStoichiometry();
            list<int>::const_iterator it1;
            list<double>::const_iterator it2;
            for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound -= nodeStoich*fluxArray[edge_id];
               }
            }
            list<int> products = edge.getHeadNodes();
            list<double> productsStoich = edge.getHeadStoichiometry();
            for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound += nodeStoich*fluxArray[edge_id];
               }
            }
         }
         // Sv[i] = b[i]
         if (addSSConstraint)
         {
            if (!this->_graph.getNode(i).isBoundary())
            {
               std::string svbname = "(Sv=b)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
               auto svconstraint = hyperstory.add(variationOfCompound == 0);
               svconstraint.setName(svbname.c_str());
            }
         }
      }

      set<int> constrainedReactions;
      // Constraints for reactions
      for (Bounds b : reactionBounds)
      {
         unsigned int j = b.id;
         assert(j<ncolsS);

         IloNumExpr lBound(_env);
         IloNumExpr uBound(_env);

         lBound += b.lb;
         uBound += b.ub;
         std::string blbname = "b_lo_bound_"+std::to_string(j);
         auto lbconstraint = hyperstory.add(lBound*(1-deletionsArray[j]) <= fluxArray[j]);
         lbconstraint.setName(blbname.c_str());
         std::string bubname = "b_up_bound_"+std::to_string(j);
         auto ubconstraint = hyperstory.add(uBound*(1-deletionsArray[j]) >= fluxArray[j]);
         ubconstraint.setName(bubname.c_str());
         constrainedReactions.insert(j);
      }

      for(unsigned int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string vname = "v_"+std::to_string(j);
            fluxArray[j] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());

            IloNumExpr fluxLoBound(_env);
            fluxLoBound += fluxArray[j];
            std::string flbname = "flux_lo_bound_"+std::to_string(j);
            auto flbconstraint = hyperstory.add(fluxLoBound >= -100000*(1-deletionsArray[j]));
            flbconstraint.setName(flbname.c_str());

            IloNumExpr fluxGeBound(_env);
            fluxGeBound += fluxArray[j];
            std::string fubname = "flux_up_bound_"+std::to_string(j);
            auto fubconstraint = hyperstory.add(fluxGeBound <= 100000*(1-deletionsArray[j]));
            fubconstraint.setName(fubname.c_str());
         }
      }

      // Objective function
      for (unsigned int j=0;j<ncolsS;++j)
      {
         objective += momaDiffArray[j];
      }
      hyperstory.add(IloMinimize(_env, objective));
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   try{
      IloCplex solver(_env);
      solver.setOut(_env.getNullStream());
      solver.setWarning(_env.getNullStream());
      solver.setParam(IloCplex::WorkDir,".");
      solver.setParam(IloCplex::WorkMem,5000);
      solver.setParam(IloCplex::NodeFileInd,3);
      //solver.setParam(IloCplex::TiLim, 21600);
      solver.setParam(IloCplex::PreInd, false);
      solver.setParam(IloCplex::EpGap, 1e-9);
      solver.setParam(IloCplex::EpAGap, 1e-9);
      solver.setParam(IloCplex::EpOpt, 1e-9);
      solver.setParam(IloCplex::EpLin, 1e-9);
      solver.setParam(IloCplex::EpInt, 1e-9);
      solver.setParam(IloCplex::EpRHS, 1e-9);
      solver.setParam(IloCplex::NumericalEmphasis, true);

      //solver.setParam(IloCplex::PreInd, false);

      //cout << "[###] The model has (without presolve):" << endl;
      //cout << "[###] " << numVars << " variables;" << endl;
      //cout << "[###] " << numRestricoes << " constraints;" << endl;

      //      IloExtractable fubconstraint;
      //      for (auto it : this->_graph.getEdges())
      //      {
      //         HEdge e = it.second;
      //         int j = e.getId();
      //         IloNumExpr fluxGeBound(_env);
      //         fluxGeBound += fluxArray[j];
      //         fubconstraint = hyperstory.add(fluxGeBound <= 0.2);

      solver.extract(hyperstory);
      solver.exportModel("/tmp/fbaMOMAZeroExcBiomassEthanolGlycerol.lp");
      solver.solve();

      //cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

      double glycerolProduction = -1;
      double optimalMomaValue = solver.getObjValue();
      bool addedOptimalBound = false;

      if ( solver.getStatus() == Optimal )
      {
         // Get the solution
         std::vector<double> fluxesSol = std::vector<double>(ncolsS);
         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = fluxesSolTmp[l];
            //cout << this->_graph.getEdge(l).getName() << "\t" << fluxesSol[l] << endl;
         }
         cout << "Biomass: " << fluxesSol[2108] << ", Ethanol: " << fluxesSol[1760] << "Glycerol: " << fluxesSol[1807] <<   endl;
         glycerolProduction = fluxesSol[1807];
         /*for(unsigned int l=0;l<ncolsS;++l)
            {
               if (fluxesSol[l] != 0 )
               {
                  HEdge edge = this->_graph.getEdge(l);
                  cout << edge.getDescription() << " (" << edge.getName() <<") = " << fluxesSol[l] << endl;
               }
            }*/

         cout << "[DEBUG] Optimal value: " << optimalMomaValue << endl;

      }
      else
      {
         cout << "No solution found." << endl;
      }

      std::string outFilename = "/tmp/point_MOMA_Tri_" + std::to_string(biomassProduction) + "b_" + std::to_string(ethanolProduction) + "e_" + std::to_string(glycerolProduction) + "g.txt";
      FILE *solutionFile = fopen (outFilename.c_str(), "w");
      while ( solver.getStatus() == Optimal )
      {
         int idToFixToZero = -1;
         int numberOfZeroFluxes = 0;
         std::vector<double> fluxesSol = std::vector<double>(ncolsS);
         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = fluxesSolTmp[l];
            if (fluxesSol[l] == 0.0)
            {
               ++numberOfZeroFluxes;
            }
         }
         std::cout << "Solution with " << numberOfZeroFluxes << " zero reactions." << std::endl;

         // Get the solution
         IloNumArray deletionsSolTmp(_env,ncolsS);
         solver.getValues(deletionsSolTmp,deletionsArray);
         for(unsigned int l=0;l<ncolsS;++l){
            if (deletionsSolTmp[l] + 0.5 > 1){
               HEdge e = this->_graph.getEdge(l);
               std::cout << "Delete reaction " << e.getName() << " (MOMA value: " << solver.getObjValue() << ")" << std::endl;
               std::stringstream solutionLine;
               solutionLine << "(" << e.getName() << ") " << e.getDescription() << " ; " << scores[e.getId()] << "\n";
               fprintf (solutionFile,solutionLine.str().c_str(),"%s");
               idToFixToZero = l;
            }
         }
         // Add the constraint
         assert(idToFixToZero >= 0);
         if (!addedOptimalBound)
         {
            // Fixing the MOMA value to the optimal for enumeration
            hyperstory.add(objective <= optimalMomaValue);
            addedOptimalBound = true;
         }
         hyperstory.add(deletionsArray[idToFixToZero] == 0);
         solver.extract(hyperstory);
         solver.exportModel("/tmp/fbaMOMAZeroExcBiomassEthanolGlycerol.lp");
         solver.solve();
      }
      fclose(solutionFile);

   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }
}

/**
 * This is similar to doFBAFixingBiomassAndEthanol, with
 * the difference that we remove the reactios with zero
 * flux in the reference vector from the list of candidate
 * reactions to be deleted.
 */
void CplexFBAModeler::doFBAFixingBiomassAndEthanolExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, double biomassProduction, double ethanolProduction, vector<double>  scores)
{
   // Number of lines and columns
   const unsigned int ncolsS = _graph.getNbEdges();
   const unsigned int nlinesS = _graph.getNbNodes();
   cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   IloNumArray inputDeltas(_env,nlinesS);
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      inputDeltas[id] = 0.0;
   }
   for (const int id : _graph.getBlackNodes())
   {
      inputDeltas[id] = 0.0;
   }
   IloModel hyperstory(_env);
   IloNumVarArray fluxArray(_env,ncolsS);
   IloNumVarArray deletionsArray(_env,ncolsS);


   try{
      // Model variables
      // For each flux
      for(unsigned int i=0;i<ncolsS;++i)
      {
         std::string vname = "v_"+std::to_string(i);
         fluxArray[i] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());
         std::string yname = "y_"+std::to_string(i);
         deletionsArray[i] = IloNumVar(_env,0,1,ILOBOOL,yname.c_str());
      }

      // Constraints
      // Fixing hte number of fixed deletion to one
      IloNumExpr numberOfDeletions(_env);
      for(unsigned int i=0;i<ncolsS;++i)
      {
         numberOfDeletions += deletionsArray[i];
      }
      std::string svbname = "NumberOfSingleDeletions";
      auto delconstraint = hyperstory.add(numberOfDeletions == 1);
      delconstraint.setName(svbname.c_str());

      // Fixing the rates of biomass and ethanol production
      HEdge biomass = *this->_graph.getEdge("r_2110");
      auto bioConstraint = hyperstory.add(fluxArray[biomass.getId()] >= biomassProduction);
      bioConstraint.setName("BiomassProd");
      HEdge ethanol = *this->_graph.getEdge("r_1761");
      auto ethanolConstraint = hyperstory.add(fluxArray[ethanol.getId()] == ethanolProduction);
      ethanolConstraint.setName("EthanolProd");
      //HEdge gly = *this->_graph.getEdge("r_1808");
      //auto glyConstraint = hyperstory.add(fluxArray[gly.getId()] == 0);
      //glyConstraint.setName("GlycerolProd");

      // Fixing some Y to zero because we don't want to delete those reactions
      // Chosing the reactions that have zero in the reference flux vector.
      for(unsigned int i=0;i<ncolsS;++i)
      {
         if (referenceFluxVector[i] == 0)
         {
            auto fixconstraint = hyperstory.add(deletionsArray[i] == 0);
            std::string cname = "FIX_REF_"+_graph.getEdge(i).getName();
            fixconstraint.setName(cname.c_str());
         }
      }
      std::string reactionsToFix[] = {"r_1761", "r_1808", "r_2110"};
      for (std::string reactionName : reactionsToFix)
      {
         HEdge edge = *this->_graph.getEdge(reactionName);
         auto fixconstraint = hyperstory.add(deletionsArray[edge.getId()] == 0);
         std::string cname = "FIX_"+edge.getName();
         fixconstraint.setName(cname.c_str());
      }
      for(unsigned int i=0;i<nlinesS;++i)
      {
         bool addSSConstraint = false;
         IloNumExpr variationOfCompound(_env);
         for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
         {
            int edge_id = edge_it->first;
            HEdge edge = edge_it->second;
            const list<int> sources = edge.getTailNodes();
            const list<double> sourcesStoich = edge.getTailStoichiometry();
            list<int>::const_iterator it1;
            list<double>::const_iterator it2;
            for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound -= nodeStoich*fluxArray[edge_id];
               }
            }
            list<int> products = edge.getHeadNodes();
            list<double> productsStoich = edge.getHeadStoichiometry();
            for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound += nodeStoich*fluxArray[edge_id];
               }
            }
         }
         // Sv[i] = b[i]
         if (addSSConstraint)
         {
            if (!this->_graph.getNode(i).isBoundary())
            {
               std::string svbname = "(Sv=b)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
               auto svconstraint = hyperstory.add(variationOfCompound == 0);
               svconstraint.setName(svbname.c_str());
            }
            //            else
            //            {
            //
            //               std::string svbname = "(Sv=b)_"+std::to_string(i);
            //               auto svconstraint = hyperstory.add(variationOfCompound >= 0);
            //               svconstraint.setName(svbname.c_str());
            //            }
         }

      }

      set<int> constrainedReactions;
      // Constraints for reactions
      for (Bounds b : reactionBounds)
      {
         unsigned int j = b.id;
         assert(j<ncolsS);

         IloNumExpr lBound(_env);
         IloNumExpr uBound(_env);

         lBound += b.lb;
         uBound += b.ub;
         std::string blbname = "b_lo_bound_"+std::to_string(j);
         auto lbconstraint = hyperstory.add(lBound*(1-deletionsArray[j]) <= fluxArray[j]);
         lbconstraint.setName(blbname.c_str());
         std::string bubname = "b_up_bound_"+std::to_string(j);
         auto ubconstraint = hyperstory.add(uBound*(1-deletionsArray[j]) >= fluxArray[j]);
         ubconstraint.setName(bubname.c_str());
         constrainedReactions.insert(j);
      }

      for(unsigned int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string vname = "v_"+std::to_string(j);
            fluxArray[j] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());

            IloNumExpr fluxLoBound(_env);
            fluxLoBound += fluxArray[j];
            std::string flbname = "flux_lo_bound_"+std::to_string(j);
            auto flbconstraint = hyperstory.add(fluxLoBound >= -100000*(1-deletionsArray[j]));
            flbconstraint.setName(flbname.c_str());

            IloNumExpr fluxGeBound(_env);
            fluxGeBound += fluxArray[j];
            std::string fubname = "flux_up_bound_"+std::to_string(j);
            auto fubconstraint = hyperstory.add(fluxGeBound <= 100000*(1-deletionsArray[j]));
            fubconstraint.setName(fubname.c_str());
         }
      }

      // Objective function
      IloNumExpr objective(_env);
      for (HEdge e : reactionsToProduce)
      {
         objective += fluxArray[e.getId()];
      }
      //hyperstory.add(objective >= 0.01);
      hyperstory.add(IloMaximize(_env, objective));
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   try{
      IloCplex solver(_env);
      solver.setOut(_env.getNullStream());
      solver.setWarning(_env.getNullStream());
      solver.setParam(IloCplex::WorkDir,".");
      solver.setParam(IloCplex::WorkMem,5000);
      solver.setParam(IloCplex::NodeFileInd,3);
      solver.setParam(IloCplex::TiLim, 21600);
      solver.setParam(IloCplex::EpGap, 1e-9);
      solver.setParam(IloCplex::PreInd, false);
      solver.setParam(IloCplex::EpAGap, 1e-9);
      solver.setParam(IloCplex::EpOpt, 1e-9);
      solver.setParam(IloCplex::EpLin, 1e-9);
      solver.setParam(IloCplex::EpInt, 1e-9);
      solver.setParam(IloCplex::EpRHS, 1e-9);
      solver.setParam(IloCplex::NumericalEmphasis, true);
      //cout << "[###] The model has (without presolve):" << endl;
      //cout << "[###] " << numVars << " variables;" << endl;
      //cout << "[###] " << numRestricoes << " constraints;" << endl;

      //      IloExtractable fubconstraint;
      //      for (auto it : this->_graph.getEdges())
      //      {
      //         HEdge e = it.second;
      //         int j = e.getId();
      //         IloNumExpr fluxGeBound(_env);
      //         fluxGeBound += fluxArray[j];
      //         fubconstraint = hyperstory.add(fluxGeBound <= 0.2);

      solver.extract(hyperstory);
      solver.exportModel("/tmp/fbaBiomassEthanol.lp");
      solver.solve();

      //cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

      double glycerolProduction = -1;
      if ( solver.getStatus() == Optimal )
      {
         // Get the solution

         std::vector<double> fluxesSol = std::vector<double>(ncolsS);
         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = fluxesSolTmp[l];
            //cout << this->_graph.getEdge(l).getName() << "\t" << fluxesSol[l] << endl;
         }
         cout << "Biomass: " << fluxesSol[2108] << ", Ethanol: " << fluxesSol[1760] << "Glycerol: " << fluxesSol[1807] <<   endl;
         glycerolProduction = fluxesSol[1807];
         /*for(unsigned int l=0;l<ncolsS;++l)
            {
               if (fluxesSol[l] != 0 )
               {
                  HEdge edge = this->_graph.getEdge(l);
                  cout << edge.getDescription() << " (" << edge.getName() <<") = " << fluxesSol[l] << endl;
               }
            }*/
      }

      std::string outFilename = "/tmp/point_BiObj_RemZeros" + std::to_string(biomassProduction) + "b_" + std::to_string(ethanolProduction) + "e_" + std::to_string(glycerolProduction) + "g.txt";
      FILE *solutionFile = fopen (outFilename.c_str(), "w");
      while ( solver.getStatus() == Optimal )
      {
         int idToFixToZero = -1;
         // Get the solution
         IloNumArray deletionsSolTmp(_env,ncolsS);
         solver.getValues(deletionsSolTmp,deletionsArray);
         for(unsigned int l=0;l<ncolsS;++l){
            if (deletionsSolTmp[l] + 0.5 > 1){
               HEdge e = this->_graph.getEdge(l);
               std::cout << "Delete reaction " << e.getName() << std::endl;
               std::stringstream solutionLine;
               solutionLine << "(" << e.getName() << ") " << e.getDescription() << " ; " << scores[e.getId()] << "\n";
               fprintf (solutionFile,solutionLine.str().c_str(),"%s");
               idToFixToZero = l;
            }
         }
         // Add the constraint
         assert(idToFixToZero >= 0);
         hyperstory.add(deletionsArray[idToFixToZero] == 0);
         solver.extract(hyperstory);
         solver.solve();
      }
      fclose(solutionFile);

   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }
}

void CplexFBAModeler::doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, double biomassProduction, double ethanolProduction, double glycerolProduction, vector<double>  scores)
{
   // Number of lines and columns
   const unsigned int ncolsS = _graph.getNbEdges();
   const unsigned int nlinesS = _graph.getNbNodes();
   cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   IloNumArray inputDeltas(_env,nlinesS);
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      inputDeltas[id] = 0.0;
   }
   for (const int id : _graph.getBlackNodes())
   {
      inputDeltas[id] = 0.0;
   }
   IloModel hyperstory(_env);
   IloNumVarArray fluxArray(_env,ncolsS);
   IloNumVarArray deletionsArray(_env,ncolsS);


   try{
      // Model variables
      // For each flux
      for(unsigned int i=0;i<ncolsS;++i)
      {
         std::string vname = "v_"+std::to_string(i);
         fluxArray[i] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());
         std::string yname = "y_"+std::to_string(i);
         deletionsArray[i] = IloNumVar(_env,0,1,ILOBOOL,yname.c_str());
      }

      // Constraints
      // Fixing hte number of fixed deletion to one
      IloNumExpr numberOfDeletions(_env);
      for(unsigned int i=0;i<ncolsS;++i)
      {
         numberOfDeletions += deletionsArray[i];
      }
      std::string svbname = "NumberOfSingleDeletions";
      auto delconstraint = hyperstory.add(numberOfDeletions == 1);
      delconstraint.setName(svbname.c_str());

      // Fixing the rates of biomass and ethanol production
      HEdge biomass = *this->_graph.getEdge("r_2110");
      auto bioConstraint = hyperstory.add(fluxArray[biomass.getId()] >= biomassProduction);
      bioConstraint.setName("BiomassProd");
      HEdge ethanol = *this->_graph.getEdge("r_1761");
      auto ethanolConstraint = hyperstory.add(fluxArray[ethanol.getId()] >= ethanolProduction);
      ethanolConstraint.setName("EthanolProd");
      HEdge gly = *this->_graph.getEdge("r_1808");
      auto glyConstraint = hyperstory.add(fluxArray[gly.getId()] == glycerolProduction);
      glyConstraint.setName("GlycerolProd");

      // Fixing some Y to zero because we don't want to delete those reactions
      // Chosing the reactions that have zero in the reference flux vector.
      for(unsigned int i=0;i<ncolsS;++i)
      {
         if (referenceFluxVector[i] == 0)
         {
            auto fixconstraint = hyperstory.add(deletionsArray[i] == 0);
            std::string cname = "FIX_REF_"+_graph.getEdge(i).getName();
            fixconstraint.setName(cname.c_str());
         }
      }

      // Forbiding to remove essential reactions
      std::string reactionsToFix[] = {"r_1761", "r_1808", "r_2110"};
      for (std::string reactionName : reactionsToFix)
      {
         HEdge edge = *this->_graph.getEdge(reactionName);
         auto fixconstraint = hyperstory.add(deletionsArray[edge.getId()] == 0);
         std::string cname = "FIX_"+edge.getName();
         fixconstraint.setName(cname.c_str());
      }

      for(unsigned int i=0;i<nlinesS;++i)
      {
         bool addSSConstraint = false;
         IloNumExpr variationOfCompound(_env);
         for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
         {
            int edge_id = edge_it->first;
            HEdge edge = edge_it->second;
            const list<int> sources = edge.getTailNodes();
            const list<double> sourcesStoich = edge.getTailStoichiometry();
            list<int>::const_iterator it1;
            list<double>::const_iterator it2;
            for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound -= nodeStoich*fluxArray[edge_id];
               }
            }
            list<int> products = edge.getHeadNodes();
            list<double> productsStoich = edge.getHeadStoichiometry();
            for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound += nodeStoich*fluxArray[edge_id];
               }
            }
         }
         // Sv[i] = b[i]
         if (addSSConstraint)
         {
            if (!this->_graph.getNode(i).isBoundary())
            {
               std::string svbname = "(Sv=b)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
               auto svconstraint = hyperstory.add(variationOfCompound == 0);
               svconstraint.setName(svbname.c_str());
            }
            //            else
            //            {
            //
            //               std::string svbname = "(Sv=b)_"+std::to_string(i);
            //               auto svconstraint = hyperstory.add(variationOfCompound >= 0);
            //               svconstraint.setName(svbname.c_str());
            //            }
         }

      }

      set<int> constrainedReactions;
      // Constraints for reactions
      for (Bounds b : reactionBounds)
      {
         unsigned int j = b.id;
         assert(j<ncolsS);

         IloNumExpr lBound(_env);
         IloNumExpr uBound(_env);

         lBound += b.lb;
         uBound += b.ub;
         std::string blbname = "b_lo_bound_"+std::to_string(j);
         auto lbconstraint = hyperstory.add(lBound*(1-deletionsArray[j]) <= fluxArray[j]);
         lbconstraint.setName(blbname.c_str());
         std::string bubname = "b_up_bound_"+std::to_string(j);
         auto ubconstraint = hyperstory.add(uBound*(1-deletionsArray[j]) >= fluxArray[j]);
         ubconstraint.setName(bubname.c_str());
         constrainedReactions.insert(j);
      }

      for(unsigned int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string vname = "v_"+std::to_string(j);
            fluxArray[j] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());

            IloNumExpr fluxLoBound(_env);
            fluxLoBound += fluxArray[j];
            std::string flbname = "flux_lo_bound_"+std::to_string(j);
            auto flbconstraint = hyperstory.add(fluxLoBound >= -10000*(1-deletionsArray[j]));
            flbconstraint.setName(flbname.c_str());

            IloNumExpr fluxGeBound(_env);
            fluxGeBound += fluxArray[j];
            std::string fubname = "flux_up_bound_"+std::to_string(j);
            auto fubconstraint = hyperstory.add(fluxGeBound <= 10000*(1-deletionsArray[j]));
            fubconstraint.setName(fubname.c_str());
         }
      }

      // Objective function
      IloNumExpr objective(_env);
      for (HEdge e : reactionsToProduce)
      {
         objective += fluxArray[e.getId()];
      }
      //hyperstory.add(objective >= 0.01);
      hyperstory.add(IloMaximize(_env, objective));
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   try{
      IloCplex solver(_env);
      solver.setOut(_env.getNullStream());
      solver.setWarning(_env.getNullStream());
      solver.setParam(IloCplex::WorkDir,".");
      solver.setParam(IloCplex::WorkMem,5000);
      solver.setParam(IloCplex::NodeFileInd,3);
      solver.setParam(IloCplex::TiLim, 21600);
      solver.setParam(IloCplex::EpGap, 1e-9);
      solver.setParam(IloCplex::EpAGap, 1e-9);
      solver.setParam(IloCplex::EpOpt, 1e-9);
      solver.setParam(IloCplex::EpLin, 1e-9);
      solver.setParam(IloCplex::EpInt, 1e-9);
      solver.setParam(IloCplex::EpRHS, 1e-9);
      solver.setParam(IloCplex::NumericalEmphasis, true);
      solver.setParam(IloCplex::PreInd, false);



      //solver.setParam(IloCplex::PreInd, false);

      //cout << "[###] The model has (without presolve):" << endl;
      //cout << "[###] " << numVars << " variables;" << endl;
      //cout << "[###] " << numRestricoes << " constraints;" << endl;

      //      IloExtractable fubconstraint;
      //      for (auto it : this->_graph.getEdges())
      //      {
      //         HEdge e = it.second;
      //         int j = e.getId();
      //         IloNumExpr fluxGeBound(_env);
      //         fluxGeBound += fluxArray[j];
      //         fubconstraint = hyperstory.add(fluxGeBound <= 0.2);

      solver.extract(hyperstory);
      solver.exportModel("/tmp/fbaBiomassEthanolGlycerol.lp");
      solver.solve();

      cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

      double glycerolProduction = -1;
      if ( solver.getStatus() == Optimal )
      {
         // Get the solution

         std::vector<double> fluxesSol = std::vector<double>(ncolsS);
         IloNumArray fluxesSolTmp(_env,ncolsS);
         solver.getValues(fluxesSolTmp,fluxArray);
         for(unsigned int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = fluxesSolTmp[l];
            //cout << this->_graph.getEdge(l).getName() << "\t" << fluxesSol[l] << endl;
         }
         cout << "Biomass: " << fluxesSol[2108] << ", Ethanol: " << fluxesSol[1760] << "Glycerol: " << fluxesSol[1807] <<   endl;
         glycerolProduction = fluxesSol[1807];
         /*for(unsigned int l=0;l<ncolsS;++l)
            {
               if (fluxesSol[l] != 0 )
               {
                  HEdge edge = this->_graph.getEdge(l);
                  cout << edge.getDescription() << " (" << edge.getName() <<") = " << fluxesSol[l] << endl;
               }
            }*/
      }

      std::string outFilename = "/tmp/point_TriObj_RemZeros" + std::to_string(biomassProduction) + "b_" + std::to_string(ethanolProduction) + "e_" + std::to_string(glycerolProduction) + "g.txt";
      FILE *solutionFile = fopen (outFilename.c_str(), "w");
      while ( solver.getStatus() == Optimal )
      {
         int idToFixToZero = -1;
         // Get the solution
         IloNumArray deletionsSolTmp(_env,ncolsS);
         solver.getValues(deletionsSolTmp,deletionsArray);
         for(unsigned int l=0;l<ncolsS;++l){
            if (deletionsSolTmp[l] + 0.5 > 1){
               HEdge e = this->_graph.getEdge(l);
               std::cout << "Delete reaction " << e.getName() << std::endl;
               std::stringstream solutionLine;
               solutionLine << "(" << e.getName() << ") " << e.getDescription() << " ; " << scores[e.getId()] << "\n";
               fprintf (solutionFile,solutionLine.str().c_str(),"%s");
               idToFixToZero = l;
            }
         }
         // Add the constraint
         assert(idToFixToZero >= 0);
         hyperstory.add(deletionsArray[idToFixToZero] == 0);
         solver.extract(hyperstory);
         solver.solve();
      }
      fclose(solutionFile);

   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }
}

vector<double> CplexFBAModeler::doMultiFBADeletingReactions(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<HEdge> reactionsToDelete)
{
   // Number of lines and columns
   const unsigned int ncolsS = _graph.getNbEdges();
   const unsigned int nlinesS = _graph.getNbNodes();
   cout << "Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   // Return vector
   std::vector<double> fluxesSol = std::vector<double>(ncolsS);

   IloNumArray inputDeltas(_env,nlinesS);
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      inputDeltas[id] = 0.0;
   }
   for (const int id : _graph.getBlackNodes())
   {
      inputDeltas[id] = 0.0;
   }
   IloModel hyperstory(_env);
   IloNumVarArray fluxArray(_env,ncolsS);

   try{
      // Model variables
      // For each flux
      for(unsigned int i=0;i<ncolsS;++i)
      {
         std::string vname = "v_"+std::to_string(i);
         fluxArray[i] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());
      }

      // Constraints
      for(unsigned int i=0;i<nlinesS;++i)
      {
         bool addSSConstraint = false;
         IloNumExpr variationOfCompound(_env);
         for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
         {
            int edge_id = edge_it->first;
            HEdge edge = edge_it->second;
            const list<int> sources = edge.getTailNodes();
            const list<double> sourcesStoich = edge.getTailStoichiometry();
            list<int>::const_iterator it1;
            list<double>::const_iterator it2;
            for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound -= nodeStoich*fluxArray[edge_id];
               }
            }
            list<int> products = edge.getHeadNodes();
            list<double> productsStoich = edge.getHeadStoichiometry();
            for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
            {
               int nodeId = *it1;
               double nodeStoich = *it2;
               if(nodeId == (int)i)
               {
                  addSSConstraint = true;
                  variationOfCompound += nodeStoich*fluxArray[edge_id];
               }
            }
         }
         // Sv[i] = b[i]
         if (addSSConstraint)
         {
            if (!this->_graph.getNode(i).isBoundary())
            {
               std::string svbname = "(Sv=b)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
               auto svconstraint = hyperstory.add(variationOfCompound == 0);
               svconstraint.setName(svbname.c_str());
            }
            //            else
            //            {
            //
            //               std::string svbname = "(Sv=b)_"+std::to_string(i);
            //               auto svconstraint = hyperstory.add(variationOfCompound >= 0);
            //               svconstraint.setName(svbname.c_str());
            //            }
         }

      }

      set<int> constrainedReactions;
      // Constraints for reactions
      for (Bounds b : reactionBounds)
      {
         unsigned int j = b.id;
         assert(j<ncolsS);

         IloNumExpr lBound(_env);
         IloNumExpr uBound(_env);

         lBound += b.lb;
         uBound += b.ub;
         std::string blbname = "b_lo_bound_"+std::to_string(j);
         auto lbconstraint = hyperstory.add(lBound <= fluxArray[j]);
         lbconstraint.setName(blbname.c_str());
         std::string bubname = "b_up_bound_"+std::to_string(j);
         auto ubconstraint = hyperstory.add(uBound >= fluxArray[j]);
         ubconstraint.setName(bubname.c_str());
         constrainedReactions.insert(j);
      }

      for(unsigned int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string vname = "v_"+std::to_string(j);
            fluxArray[j] = IloNumVar(_env,-IloInfinity,IloInfinity,ILOFLOAT,vname.c_str());

            IloNumExpr fluxLoBound(_env);
            fluxLoBound += fluxArray[j];
            std::string flbname = "flux_lo_bound_"+std::to_string(j);
            auto flbconstraint = hyperstory.add(fluxLoBound >= -100000);
            flbconstraint.setName(flbname.c_str());

            IloNumExpr fluxGeBound(_env);
            fluxGeBound += fluxArray[j];
            std::string fubname = "flux_up_bound_"+std::to_string(j);
            auto fubconstraint = hyperstory.add(fluxGeBound <= 100000);
            fubconstraint.setName(fubname.c_str());
         }
      }

      // Objective function
      IloNumExpr objective(_env);
      for (HEdge e : reactionsToProduce)
      {
         objective += fluxArray[e.getId()];
      }
      //hyperstory.add(objective >= 0.01);
      hyperstory.add(IloMaximize(_env, objective));
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   vector<double> returnScores;

   try{
      IloCplex solver(_env);
      solver.setOut(_env.getNullStream());
      solver.setWarning(_env.getNullStream());
      solver.setParam(IloCplex::WorkDir,".");
      solver.setParam(IloCplex::WorkMem,5000);
      solver.setParam(IloCplex::NodeFileInd,3);
      solver.setParam(IloCplex::TiLim, 21600);
      solver.setParam(IloCplex::EpGap, 1e-7);
      //solver.setParam(IloCplex::PreInd, false);
      solver.exportModel("/tmp/fba.lp");

      //cout << "[###] The model has (without presolve):" << endl;
      //cout << "[###] " << numVars << " variables;" << endl;
      //cout << "[###] " << numRestricoes << " constraints;" << endl;

      IloExtractable fubconstraint;
      for (HEdge e : reactionsToDelete)
      {
         int j = e.getId();
         IloNumExpr fluxGeBound(_env);
         fluxGeBound += fluxArray[j];
         fubconstraint = hyperstory.add(fluxGeBound <= 0.2);

         solver.extract(hyperstory);
         solver.exportModel("/tmp/score.lp");
         solver.solve();

         cout << "[###] Solver status: " << solver.getStatus() << " - Objective = " << solver.getObjValue() <<endl;

         if ( solver.getStatus() == Optimal )
         {
            // Get the solution
            string reactionsToFixAtZero = "";

            IloNumArray fluxesSolTmp(_env,ncolsS);
            solver.getValues(fluxesSolTmp,fluxArray);
            for(unsigned int l=0;l<ncolsS;++l){
               fluxesSol[l] = fluxesSolTmp[l];
            }

            //cout << e.getName() << " " << e.getDescription() << " (Score: " << fluxesSol[1760] << ")" <<  endl;

            // Adding as score the amount of ethanol produced
            returnScores.push_back(fluxesSol[1760]);

            hyperstory.remove(fubconstraint);
         }
      }
      solver.exportModel("/tmp/fba.mps");
   } catch (IloException & exception) {
      const char* msg = exception.getMessage();
      cout << msg << endl;
   }

   return returnScores;
}

