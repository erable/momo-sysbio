/*
 * utilsForSCIP.cpp
 *
 *  Created on: 17 janv. 2017
 *      Author: randrade
 */

#include "../../hypergraph/HGraph.h"
#include <iostream>
#include <fstream>

/**
 * A temporary function for integration with PolySCIP.
 * It's main objective is to generate a MPS input file for the PolySCIP solver.
 * The problem we want to solve is to optimise two fluxes in a FBA context,
 * subject to standard steady state conditions with bound on fluxes.
 */
void generateMPSFormatFileInput(std::string filename, HGraph _graph, int numberOfDeletions, vector<HEdge> reactionsToMaximize, vector<HEdge> reactionsToMinimize, std::vector<Bounds> bounds, std::vector<std::string> reactionsToFix)
{
   /*map <std::string,HEdge> edgeNameToEdge;
   for (auto e = _graph.getEdges().begin(); e != _graph.getEdges().end(); ++e )
   {
      HEdge edge = e->second;
      edgeNameToEdge.insert(pair<std::string, HEdge> (edge.getName(), edge));
   }*/
   std::ofstream output;
   output.open (filename);
   std::string name = "TEMP";
   output << "NAME          " << name << std::endl;
   output << "OBJSENSE" << std::endl;
   output << " MIN" << std::endl;
   output << "ROWS" << std::endl;
   for (unsigned int i = 1; i<=reactionsToMaximize.size()+reactionsToMinimize.size();++i)
   {
      output << " N OBJ" << i << std::endl;
   }
   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      if(!_graph.getNode(id).isBoundary())
      {
         std::string compoundName = "C_" + std::to_string(id);
         output << " E " << compoundName  << endl;
      }
   }
   for (Bounds b : bounds)
   {
      std::string reactionName = std::to_string(_graph.getEdge(b.id).getId());
      HEdge reaction = _graph.getEdge(b.id);
      std::string reactionId = std::to_string(reaction.getId());
      std::string constraintName = "UB_" + reactionId;
      output << " L " << constraintName  << endl;
      constraintName = "LB_" + reactionId;
      output << " G " << constraintName  << endl;
   }
   output << " E KNOCK1" << std::endl;
   // Fixing those deletions to zero so they are never a candidate.
   //1542,1545,1546,1547,1548,1549,1550,1551,1552,1553,1554,1563,1564,1565,1566,1571,1572,1577,1580,1581,1586,1589,1598,1604,1621,1625,1627,1629,1630,1631,1634,1639,1641,1643,1648,1649,1650,1651,1654,1663,1671,1672,1683,1687,1702,1705,1706,1709,1710,1711,1712,1714,1715,1716,1718,1727,1730,1749,1753,1757,1761,1764,1765,1767,1788,1791,1792,1793,1798,1799,1800,1806,1807,1808,1810,1814,1815,1818,1820,1831,1833,1840,1842,1846,1860,1861,1864,1865,1866,1869,1872,1874,1877,1878,1879,1880,1882,1885,1888,1890,1892,1895,1896,1898,1899,1901,1902,1903,1905,1908,1910,1911,1912,1913,1914,1915,1916,1917,1930,1932,1946,1948,1950,1951,1961,1966,1967,1983,1984,1986,1988,1991,1992,1993,1998,1999,2000,2004,2008,2018,2019,2023,2027,2032,2037,2042,2043,2045,2048,2050,2051,2054,2055,2057,2059,2060,2061,2065,2066,2067,2072,2075,2082,2089,2090,2091,2099,2101,2103,2105,2109,2110
   //vector<std::string> reactionsToFix = {"r_1542","r_1545","r_1546","r_1547","r_1548","r_1549","r_1550","r_1551","r_1552","r_1553","r_1554","r_1563","r_1564","r_1565","r_1566","r_1571","r_1572","r_1577","r_1580","r_1581","r_1586","r_1589","r_1598","r_1604","r_1621","r_1625","r_1627","r_1629","r_1630","r_1631","r_1634","r_1639","r_1641","r_1643","r_1648","r_1649","r_1650","r_1651","r_1654","r_1663","r_1671","r_1672","r_1683","r_1687","r_1702","r_1705","r_1706","r_1709","r_1710","r_1711","r_1712","r_1714","r_1715","r_1716","r_1718","r_1727","r_1730","r_1749","r_1753","r_1757","r_1761","r_1764","r_1765","r_1767","r_1788","r_1791","r_1792","r_1793","r_1798","r_1799","r_1800","r_1806","r_1807","r_1808","r_1810","r_1814","r_1815","r_1818","r_1820","r_1831","r_1833","r_1840","r_1842","r_1846","r_1860","r_1861","r_1864","r_1865","r_1866","r_1869","r_1872","r_1874","r_1877","r_1878","r_1879","r_1880","r_1882","r_1885","r_1888","r_1890","r_1892","r_1895","r_1896","r_1898","r_1899","r_1901","r_1902","r_1903","r_1905","r_1908","r_1910","r_1911","r_1912","r_1913","r_1914","r_1915","r_1916","r_1917","r_1930","r_1932","r_1946","r_1948","r_1950","r_1951","r_1961","r_1966","r_1967","r_1983","r_1984","r_1986","r_1988","r_1991","r_1992","r_1993","r_1998","r_1999","r_2000","r_2004","r_2008","r_2018","r_2019","r_2023","r_2027","r_2032","r_2037","r_2042","r_2043","r_2045","r_2048","r_2050","r_2051","r_2054","r_2055","r_2057","r_2059","r_2060","r_2061","r_2065","r_2066","r_2067","r_2072","r_2075","r_2082","r_2089","r_2090","r_2091","r_2099","r_2101","r_2103","r_2105","r_2109","r_2110"};
   for (string reactionName : reactionsToFix)
   {
      boost::optional<HEdge> reaction = _graph.getEdge(reactionName);
      assert(reaction.is_initialized());
      std::string reactionId = std::to_string(reaction->getId());
      string cName = "FIX_" + reactionId;
      output << " E ";
      output.width(10);
      output << std::left << cName << endl;
   }

   output << "COLUMNS" << endl;
   output << "    MARK0000  \'MARKER\'                 \'INTORG\'" <<std::endl;
   for (Bounds b : bounds)
   {
      HEdge edge = _graph.getEdge(b.id);
      std::string reactionId = std::to_string(edge.getId());
      std::string reactionName = edge.getName();
      std::string variableName = "Y_" + reactionId;
      string bname;
      if (std::find(reactionsToFix.begin(), reactionsToFix.end(),reactionName) != reactionsToFix.end())
      {
         bname = "FIX_" + reactionId;
         output << "    ";
         output.width(10);
         output <<  std::left << variableName;
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << "1" << endl;
      }
      bname = "KNOCK1";
      output << "    ";
      output.width(10);
      output <<  std::left << variableName;
      output.width(10);
      output << std::left << bname;
      output.width(12);
      output << std::right << "1" << endl;
      if (b.ub != 0)
      {
         bname = "UB_" + reactionId;
         output << "    ";
         output.width(10);
         output <<  std::left << variableName;
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << b.ub << endl;
      }
      if (b.lb != 0)
      {
         bname = "LB_" + reactionId;
         output << "    ";
         output.width(10);
         output <<  std::left << variableName;
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << b.lb << endl;
      }
   }
   output << "    MARK0000  \'MARKER\'                 \'INTEND\'" <<std::endl;
   int objIndex = 1;
   for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
   {
      int edge_id = edge_it->first;
      HEdge edge = edge_it->second;
      std:: string rid = "R_" + std::to_string(edge_id);

      const list<int> sources = edge.getTailNodes();
      const list<double> sourcesStoich = edge.getTailStoichiometry();
      list<int>::const_iterator it1;
      list<double>::const_iterator it2;
      for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
      {
         int nodeId = *it1;
         double nodeStoich = -(*it2);
         if(!_graph.getNode(nodeId).isBoundary())
         {
            string cname = "C_" + std::to_string(nodeId);
            output << "    ";
            output.width(10);
            output <<  std::left << rid;
            output.width(10);
            output << std::left << cname;
            output.width(12);
            output << std::right << nodeStoich << endl;
         }
      }
      list<int> products = edge.getHeadNodes();
      list<double> productsStoich = edge.getHeadStoichiometry();
      for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
      {
         int nodeId = *it1;
         double nodeStoich = *it2;
         if(!_graph.getNode(nodeId).isBoundary())
         {
            string cname = "C_" + std::to_string(nodeId);
            output << "    ";
            output.width(10);
            output <<  std::left << rid;
            output.width(10);
            output << std::left << cname;
            output.width(12);
            output << std::right << nodeStoich << endl;
         }
      }
      if (std::find(reactionsToMinimize.begin(), reactionsToMinimize.end(), edge) != reactionsToMinimize.end() )
      {
         output << "    ";
         output.width(10);
         output <<  std::left << rid;
         output << "OBJ" << objIndex << "                1" << std::endl;
         ++objIndex;
      }
      else if (std::find(reactionsToMaximize.begin(), reactionsToMaximize.end(), edge) != reactionsToMaximize.end())
      {
         output << "    ";
         output.width(10);
         output <<  std::left << rid;
         output << "OBJ" << objIndex << "                -1" << std::endl;
         ++objIndex;
      }
      for (Bounds b : bounds)
      {
         if (b.id == edge.getId())
         {
            std::string rid = std::to_string(edge.getId());
            std:: string vrid = "R_" + std::to_string(edge_id);
            string bname = "UB_" + rid;
            output << "    ";
            output.width(10);
            output <<  std::left << vrid;
            output.width(10);
            output << std::left << bname;
            output.width(12);
            output << std::right << "1" << endl;
            bname = "LB_" + rid;
            output << "    ";
            output.width(10);
            output <<  std::left << vrid;
            output.width(10);
            output << std::left << bname;
            output.width(12);
            output << std::right << "1" << endl;
         }
      }
   }
   output << "RHS" << endl;
   for (Bounds b : bounds)
   {
      HEdge edge = _graph.getEdge(b.id);
      std::string rid = std::to_string(edge.getId());
      string bname;
      if (b.ub != 0)
      {
         bname = "UB_" + rid;
         output << "    ";
         output.width(10);
         output <<  std::left << "RHS";
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << b.ub << endl;
      }
      if (b.lb != 0)
      {
         bname = "LB_" + rid;
         output << "    ";
         output.width(10);
         output <<  std::left << "RHS";
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << b.lb << endl;
      }
   }
   output << "    RHS       KNOCK1               " << numberOfDeletions << std::endl;
   output << "BOUNDS" << endl;
   for (Bounds b : bounds)
   {
      HEdge reaction = _graph.getEdge(b.id);
      std::string rid = "R_" + std::to_string(reaction.getId());
      string bname = "BND";
      output << " MI ";
      output.width(10);
      output << std::left << bname;
      output.width(10);
      output << std::left << rid << endl;
      output << " PL ";
      output.width(10);
      output << std::left << bname;
      output.width(10);
      output << std::left << rid << endl;
   }
   for (Bounds b : bounds)
   {
      HEdge reaction = _graph.getEdge(b.id);
      std::string rid = std::to_string(reaction.getId());
      std::string variableName = "Y_" + rid;
      string bname = "BND";
      output << " LO ";
      output.width(10);
      output << std::left << bname;
      output.width(10);
      output << std::left << variableName;
      output.width(12);
      output << std::right << "0" << endl;
      output << " UP ";
      output.width(10);
      output << std::left << bname;
      output.width(10);
      output << std::left << variableName;
      output.width(12);
      output << std::right << "1" << endl;
   }
   output   << "ENDATA" << endl;
   output.close();
}


void generateMPSFormatFileInputWithRobustnessFunction(std::string filename, HGraph _graph, int numberOfDeletions, vector<HEdge> reactionsToMaximize, vector<HEdge> reactionsToMinimize, vector<HEdge> robustnessReactions, std::vector<Bounds> bounds, std::vector<std::string> reactionsToFix)
{
   // TODO Replace all reactionName (the ID of the metabolic network) for the reactionId (the ID of the object HEdge from the the graph.
   //      Reason : If the SBML reaction ID is too long it will cause problems in the MPS file
   std::ofstream output;
   output.open (filename);
   std::string name = "TEMP";
   output << "NAME          " << name << std::endl;
   output << "OBJSENSE" << std::endl;
   output << " MIN" << std::endl;
   output << "ROWS" << std::endl;
   for (unsigned int i = 1; i<=reactionsToMaximize.size()+reactionsToMinimize.size();++i)
   {
      output << " N OBJ" << i << std::endl;
   }
   // Robustness objective function label
   output << " N OBJ" << reactionsToMaximize.size()+reactionsToMinimize.size()+1 << std::endl;
   double robustness_coefficient = 1./robustnessReactions.size();

   for (auto nodeit = _graph.getNodes().begin(); nodeit != _graph.getNodes().end(); ++nodeit)
   {
      int id = nodeit->first;
      if(!_graph.getNode(id).isBoundary())
      {
         std::string compoundName = "C_" + std::to_string(id);
         output << " E " << compoundName  << endl;
      }
   }
   for (Bounds b : bounds)
   {
      std::string reactionName = _graph.getEdge(b.id).getName();
      std::string constraintName = "UB_" + reactionName;
      output << " L " << constraintName  << endl;
      constraintName = "LB_" + reactionName;
      output << " G " << constraintName  << endl;
   }
   output << " E KNOCK1" << std::endl;

   for (string reactionName : reactionsToFix)
   {
      string cName = "FIX_" + reactionName;
      output << " E ";
      output.width(10);
      output << std::left << cName << endl;
   }

   output << "COLUMNS" << endl;
   output << "    MARK0000  \'MARKER\'                 \'INTORG\'" <<std::endl;
   for (Bounds b : bounds)
   {
      HEdge edge = _graph.getEdge(b.id);
      std::string reactionName = edge.getName();
      std::string variableName = "Y_" + reactionName;
      string bname;
      if (std::find(reactionsToFix.begin(), reactionsToFix.end(),reactionName) != reactionsToFix.end())
      {
         bname = "FIX_" + reactionName;
         output << "    ";
         output.width(10);
         output <<  std::left << variableName;
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << "1" << endl;
      }
      bname = "KNOCK1";
      output << "    ";
      output.width(10);
      output <<  std::left << variableName;
      output.width(10);
      output << std::left << bname;
      output.width(12);
      output << std::right << "1" << endl;
      if (b.ub != 0)
      {
         bname = "UB_" + reactionName;
         output << "    ";
         output.width(10);
         output <<  std::left << variableName;
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << b.ub << endl;
      }
      if (b.lb != 0)
      {
         bname = "LB_" + reactionName;
         output << "    ";
         output.width(10);
         output <<  std::left << variableName;
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << b.lb << endl;
      }
   }
   output << "    MARK0000  \'MARKER\'                 \'INTEND\'" <<std::endl;
   int objIndex = 1;
   for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
   {
      int edge_id = edge_it->first;
      HEdge edge = edge_it->second;
      std:: string rname = edge.getName();
      if (rname.length() > 8)
      {
         rname = "R_" + std::to_string(edge_id);
      }
      const list<int> sources = edge.getTailNodes();
      const list<double> sourcesStoich = edge.getTailStoichiometry();
      list<int>::const_iterator it1;
      list<double>::const_iterator it2;
      for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
      {
         int nodeId = *it1;
         double nodeStoich = -(*it2);
         if(!_graph.getNode(nodeId).isBoundary())
         {
            string cname = "C_" + std::to_string(nodeId);
            output << "    ";
            output.width(10);
            output <<  std::left << rname;
            output.width(10);
            output << std::left << cname;
            output.width(12);
            output << std::right << nodeStoich << endl;
         }
      }
      list<int> products = edge.getHeadNodes();
      list<double> productsStoich = edge.getHeadStoichiometry();
      for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
      {
         int nodeId = *it1;
         double nodeStoich = *it2;
         if(!_graph.getNode(nodeId).isBoundary())
         {
            string cname = "C_" + std::to_string(nodeId);
            output << "    ";
            output.width(10);
            output <<  std::left << rname;
            output.width(10);
            output << std::left << cname;
            output.width(12);
            output << std::right << nodeStoich << endl;
         }
      }
      if (std::find(reactionsToMinimize.begin(), reactionsToMinimize.end(), edge) != reactionsToMinimize.end() )
      {
         output << "    ";
         output.width(10);
         output <<  std::left << rname;
         output << "OBJ" << objIndex << "                1" << std::endl;
         ++objIndex;
      }
      else if (std::find(reactionsToMaximize.begin(), reactionsToMaximize.end(), edge) != reactionsToMaximize.end())
      {
         output << "    ";
         output.width(10);
         output <<  std::left << rname;
         output << "OBJ" << objIndex << "                -1" << std::endl;
         ++objIndex;
      }
      else if (std::find(robustnessReactions.begin(), robustnessReactions.end(), edge) != robustnessReactions.end())
      {
         output << "    ";
         output.width(10);
         output <<  std::left << rname;
         output << "OBJ" << reactionsToMaximize.size()+reactionsToMinimize.size()+1 << "                " << -robustness_coefficient << std::endl;
      }
      for (Bounds b : bounds)
      {
         if (b.id == edge.getId())
         {
            std::string reactionName = edge.getName();
            string bname = "UB_" + reactionName;
            output << "    ";
            output.width(10);
            output <<  std::left << rname;
            output.width(10);
            output << std::left << bname;
            output.width(12);
            output << std::right << "1" << endl;
            bname = "LB_" + reactionName;
            output << "    ";
            output.width(10);
            output <<  std::left << rname;
            output.width(10);
            output << std::left << bname;
            output.width(12);
            output << std::right << "1" << endl;
         }
      }
   }
   output << "RHS" << endl;
   for (Bounds b : bounds)
   {
      HEdge edge = _graph.getEdge(b.id);
      std::string reactionName = edge.getName();
      string bname;
      if (b.ub != 0)
      {
         bname = "UB_" + reactionName;
         output << "    ";
         output.width(10);
         output <<  std::left << "RHS";
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << b.ub << endl;
      }
      if (b.lb != 0)
      {
         bname = "LB_" + reactionName;
         output << "    ";
         output.width(10);
         output <<  std::left << "RHS";
         output.width(10);
         output << std::left << bname;
         output.width(12);
         output << std::right << b.lb << endl;
      }
   }
   output << "    RHS       KNOCK1               " << numberOfDeletions << std::endl;
   output << "BOUNDS" << endl;
   for (Bounds b : bounds)
   {
      HEdge reaction = _graph.getEdge(b.id);
      std::string reactionName = reaction.getName();
      string bname = "BND";
      output << " MI ";
      output.width(10);
      output << std::left << bname;
      output.width(10);
      output << std::left << reactionName << endl;
      output << " PL ";
      output.width(10);
      output << std::left << bname;
      output.width(10);
      output << std::left << reactionName << endl;
   }
   for (Bounds b : bounds)
   {
      HEdge reaction = _graph.getEdge(b.id);
      std::string variableName = "Y_" + reaction.getName();
      string bname = "BND";
      output << " LO ";
      output.width(10);
      output << std::left << bname;
      output.width(10);
      output << std::left << variableName;
      output.width(12);
      output << std::right << "0" << endl;
      output << " UP ";
      output.width(10);
      output << std::left << bname;
      output.width(10);
      output << std::left << variableName;
      output.width(12);
      output << std::right << "1" << endl;
   }
   output   << "ENDATA" << endl;
   output.close();
}


