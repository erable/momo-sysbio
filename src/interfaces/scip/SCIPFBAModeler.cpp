/*
 * SCIPFBAModeler.cpp
 *
 * Interface with the SCIP Optimizer
 * This file provides functions to create and solve all
 * FBA models with SCIP.
 *
 *  Created on: 11 janv. 2017
 *      Author: Ricardo Andrade <ricardoluizandrade@gmail.com>
 */

#include "SCIPFBAModeler.h"
#include "utilsForSCIP.h"

SCIPFBAModeler::SCIPFBAModeler(const HGraph&  graph) : _graph(graph), _vars(), _const(), _flux_max(100000)
{
}

SCIPFBAModeler::~SCIPFBAModeler()
{
}

void SCIPFBAModeler::doPolyRobustnessObjectiveDeleting(const int deletionSetSize, const vector<HEdge>& reactionsToMaximize, const vector<HEdge>& reactionsToMinimize,  const vector<HEdge>& robustnessReactions, const vector<Bounds>& reactionBounds, vector<pair<HEdge,double>> reactionsToFix, const vector<std::string>& reactionsToBeIgnoredInTheDeletions, const std::string& output_filename)
{
   vector<Bounds> localBounds;
   // If there is no reaction to fix use the input bound without modifying it.
   if (reactionsToFix.size() == 0)
   {
      localBounds = reactionBounds;
   }
   else // If not, copy and modify it
   {
      for(Bounds b : reactionBounds)
      {
         Bounds newb;
         newb.id = b.id;
         newb.lb = b.lb;
         newb.ub = b.ub;

         for (std::pair<HEdge,double> pr : reactionsToFix)
         {
            HEdge reaction = pr.first;
            double flux = pr.second;

            if (reaction.getId() == b.id)
            {
               b.lb = flux;
               b.ub = flux;
               break;
            }
         }
         localBounds.push_back(newb);
      }
   }
   generateMPSFormatFileInputWithRobustnessFunction("input",this->_graph, deletionSetSize, reactionsToMaximize, reactionsToMinimize, robustnessReactions, localBounds,reactionsToBeIgnoredInTheDeletions);
   try {
      LiftedWeightSpaceSolver solver;
      solver.readProblem("input", false);
      solver.initialise(false);
      while (solver.hasUncheckedWeight()) {
         solver.solveUncheckedWeight(false);
         /*if (solver.foundNewUnbounded())
            solver.printRayInfo(cout, true, true);
         else if (solver.foundNewOptimum())
            solver.printSolInfo(cout, true, true);*/
      }
      solver.deleteWeaklyNondomPointsAndCorrespSols();
      std::ofstream solfs(output_filename);
      if (solfs.is_open())
      {
         solfs << "# considered objectives = [";
         for (unsigned i=0; i<solver.getNObjs(); ++i) {
            solver.printObjName(solfs,i);
            solfs << ((i+1 < solver.getNObjs()) ? ", " : "]\n");
         }
         solver.printSolInfo(solfs, false, false, "");
         solfs.close();
      }
   }catch (std::exception &e) {
      cerr << "ERROR: " << e.what() << endl;
   }
}

void SCIPFBAModeler::doPolyObjectiveDeleting(const int deletionSetSize, const vector<HEdge>& reactionsToMaximize, const vector<HEdge>& reactionsToMinimize, const vector<Bounds>& reactionBounds, vector<pair<HEdge,double>> reactionsToFix, const vector<std::string>& reactionsToBeIgnoredInTheDeletions, const std::string& output_filename)
{
   vector<Bounds> localBounds;
   // If there is no reaction to fix use the input bound without modifying it.
   if (reactionsToFix.size() == 0)
   {
      localBounds = reactionBounds;
   }
   else // If not, copy and modify it
   {
      for(Bounds b : reactionBounds)
      {
         Bounds newb;
         newb.id = b.id;
         newb.lb = b.lb;
         newb.ub = b.ub;

         for (std::pair<HEdge,double> pr : reactionsToFix)
         {
            HEdge reaction = pr.first;
            double flux = pr.second;

            if (reaction.getId() == b.id)
            {
               b.lb = flux;
               b.ub = flux;
               break;
            }
         }
         localBounds.push_back(newb);
      }
   }
   // First, lets generate MPS file input for PolySCIP
   //vector<std::string> reactionsToFixAtZero = {"r_1542","r_1545","r_1546","r_1547","r_1548","r_1549","r_1550","r_1551","r_1552","r_1553","r_1554","r_1563","r_1564","r_1565","r_1566","r_1571","r_1572","r_1577","r_1580","r_1581","r_1586","r_1589","r_1598","r_1604","r_1621","r_1625","r_1627","r_1629","r_1630","r_1631","r_1634","r_1639","r_1641","r_1643","r_1648","r_1649","r_1650","r_1651","r_1654","r_1663","r_1671","r_1672","r_1683","r_1687","r_1702","r_1705","r_1706","r_1709","r_1710","r_1711","r_1712","r_1714","r_1715","r_1716","r_1718","r_1727","r_1730","r_1749","r_1753","r_1757","r_1761","r_1764","r_1765","r_1767","r_1788","r_1791","r_1792","r_1793","r_1798","r_1799","r_1800","r_1806","r_1807","r_1808","r_1810","r_1814","r_1815","r_1818","r_1820","r_1831","r_1833","r_1840","r_1842","r_1846","r_1860","r_1861","r_1864","r_1865","r_1866","r_1869","r_1872","r_1874","r_1877","r_1878","r_1879","r_1880","r_1882","r_1885","r_1888","r_1890","r_1892","r_1895","r_1896","r_1898","r_1899","r_1901","r_1902","r_1903","r_1905","r_1908","r_1910","r_1911","r_1912","r_1913","r_1914","r_1915","r_1916","r_1917","r_1930","r_1932","r_1946","r_1948","r_1950","r_1951","r_1961","r_1966","r_1967","r_1983","r_1984","r_1986","r_1988","r_1991","r_1992","r_1993","r_1998","r_1999","r_2000","r_2004","r_2008","r_2018","r_2019","r_2023","r_2027","r_2032","r_2037","r_2042","r_2043","r_2045","r_2048","r_2050","r_2051","r_2054","r_2055","r_2057","r_2059","r_2060","r_2061","r_2065","r_2066","r_2067","r_2072","r_2075","r_2082","r_2089","r_2090","r_2091","r_2099","r_2101","r_2103","r_2105","r_2109","r_2110"};
   generateMPSFormatFileInput("input",this->_graph, deletionSetSize, reactionsToMaximize, reactionsToMinimize, localBounds,reactionsToBeIgnoredInTheDeletions);
   try {
      LiftedWeightSpaceSolver solver;
      solver.readProblem("input", false);
      solver.initialise(false);
      while (solver.hasUncheckedWeight()) {
         solver.solveUncheckedWeight(false);
         /*if (solver.foundNewUnbounded())
            solver.printRayInfo(cout, true, true);
         else if (solver.foundNewOptimum())
            solver.printSolInfo(cout, true, true);*/
      }
      solver.deleteWeaklyNondomPointsAndCorrespSols();
      std::ofstream solfs(output_filename);
      if (solfs.is_open())
      {
         solfs << "# considered objectives = [";
         for (unsigned i=0; i<solver.getNObjs(); ++i) {
            solver.printObjName(solfs,i);
            solfs << ((i+1 < solver.getNObjs()) ? ", " : "]\n");
         }
         solver.printSolInfo(solfs, false, false, "");
         solfs.close();
      }
   }catch (std::exception &e) {
      cerr << "ERROR: " << e.what() << endl;
   }
}

vector<double> SCIPFBAModeler::doFBA(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds)
{
   SCIP *scip;
   this->initializeSCIP(&scip);

   // Number of lines and columns
   const int ncolsS = _graph.getNbEdges();
   const int nlinesS = _graph.getNbNodes();
   cout << "[DEBUG] Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   // Return vector
   std::vector<double> fluxesSol = std::vector<double>(ncolsS);

   // Create basic model
   this->createSteadyStateBasicVariablesAndConstraints(scip,nlinesS,ncolsS,reactionsToProduce,true,reactionBounds);

   // Set precision and other numerical paramters
   setNumericalParameters(scip);

   // Solve
   SCIP_CALL_EXC( SCIPsolve(scip) );

   // Get solutions
   SCIP_SOL * sol = SCIPgetBestSol(scip);
   if (sol != NULL)
   {
      for(int l=0;l<ncolsS;++l)
      {
         fluxesSol[l] = SCIPgetSolVal(scip, sol, _vars[l]);
      }
   }

   // Printing the problem to a file
   SCIP_Var** vars = &_vars[0];
   SCIP_Cons** cons = &_const[0];
   SCIP_RESULT r;
   FILE *problemFile;

   problemFile = fopen("/tmp/FBA.lp","w");
   try{
      SCIP_CALL_EXC(SCIPwriteLp(scip,problemFile,"FBA",0,SCIP_OBJSENSE_MAXIMIZE, 1.0,0.0,vars,_vars.size(),ncolsS,0,0,ncolsS,cons,_const.size(),&r));
   }catch(SCIPException & exec)
   {
      cerr<<exec.what()<<endl;
   }
   fclose(problemFile);


   // Release all variables
   for (SCIP_VAR * v : _vars)
   {
      SCIP_CALL_EXC( SCIPreleaseVar(scip,&v) );

   }
   _vars.clear();

   // Release all constraints
   for (SCIP_CONS * c : _const)
   {
      SCIP_CALL_EXC( SCIPreleaseCons(scip,&c) );

   }
   _const.clear();

   this->freeSCIP(&scip);

   return fluxesSol;
}

vector<double> SCIPFBAModeler::doFBAFixingProductions(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<pair<HEdge,double>> reactionsToFix)
{
   SCIP *scip;
   this->initializeSCIP(&scip);

   // Number of lines and columns
   const int ncolsS = _graph.getNbEdges();
   const int nlinesS = _graph.getNbNodes();
   cout << "[DEBUG] Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   // Return vector
   std::vector<double> fluxesSol = std::vector<double>(ncolsS);

   // Create basic model
   this->createSteadyStateBasicVariablesAndConstraints(scip,nlinesS,ncolsS,reactionsToProduce,true,reactionBounds);

   // Fix the flux of some reactions
   this->createConstraintsToFixReactions(scip,reactionsToFix);

   // Printing the problem to a file
   FILE *problemFile;
   problemFile = fopen("/tmp/FBA_fixing.lp","w");
   SCIPprintOrigProblem(scip, problemFile, "lp", FALSE);
   fclose(problemFile);

   // Set precision and other numerical paramters
   setNumericalParameters(scip);

   // Solve
   SCIP_CALL_EXC( SCIPsolve(scip) );

   // Get solutions
   SCIP_SOL * sol = SCIPgetBestSol(scip);
   if (sol != NULL)
   {
      for(int l=0;l<ncolsS;++l)
      {
         fluxesSol[l] = SCIPgetSolVal(scip, sol, _vars[l]);
      }
   }

   // Release all variables
   for (SCIP_VAR * v : _vars)
   {
      SCIP_CALL_EXC( SCIPreleaseVar(scip,&v) );

   }
   _vars.clear();

   // Release all constraints
   for (SCIP_CONS * c : _const)
   {
      SCIP_CALL_EXC( SCIPreleaseCons(scip,&c) );

   }
   _const.clear();

   this->freeSCIP(&scip);

   return fluxesSol;
}

vector<pair<double,double>> SCIPFBAModeler::doOptimalFVA(double optimalSolutionValue, vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds,vector<pair<HEdge,double>> reactionsToFix)
{
   SCIP *scip;
   this->initializeSCIP(&scip);

   // Number of lines and columns
   const int ncolsS = _graph.getNbEdges();
   const int nlinesS = _graph.getNbNodes();
   cout << "[DEBUG] Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   // Return vector
   vector<pair<double, double>> solution (ncolsS, std::make_pair(0,0));

   // Create basic model
   this->createSteadyStateBasicVariablesAndConstraints(scip,nlinesS,ncolsS,reactionsToProduce,true,reactionBounds);

   // Fix the original objective function value
   this->createConstraintToFixFBAObjFunction(scip, nlinesS, ncolsS, optimalSolutionValue, reactionsToProduce);

   // Fix the flux of some reactions
   this->createConstraintsToFixReactions(scip,reactionsToFix);

   // Erase the original objective function
   for(int i=0;i<ncolsS;++i)
   {
      SCIP_CALL_EXC ( SCIPchgVarObj(scip, _vars[i], 0.0));
   }

   // For each reaction solve the problem of maximising and minimising it

   // Set precision and other numerical paramters
   setNumericalParameters(scip);

   for(int i=0;i<ncolsS;++i)
   {
      cout << "[DEBUG] Solving for reaction: " << i << endl;

      // Maximise reaction i
      SCIP_CALL_EXC(SCIPfreeTransform(scip));
      SCIP_CALL_EXC ( SCIPchgVarObj(scip, _vars[i], 1.0));
      SCIP_CALL_EXC( SCIPsolve(scip) );
      // Get solution
      SCIP_Real solMax = SCIPgetPrimalbound(scip);
      HEdge ee = _graph.getEdge(i);
      if (randomlySaveFluxVector(ee.getName(),ncolsS, scip))
      {
         cout << "[DEBUG] Printed " << ee.getName() << endl;
      }

      // Minimise reaction i
      SCIP_CALL_EXC(SCIPfreeTransform(scip));
      SCIP_CALL_EXC ( SCIPchgVarObj(scip, _vars[i], -1.0));
      SCIP_CALL_EXC( SCIPsolve(scip) );
      // Get solution
      SCIP_Real solMin = -SCIPgetPrimalbound(scip);
      HEdge e = _graph.getEdge(i);
      if (randomlySaveFluxVector(ee.getName(),ncolsS, scip))
      {
         cout << "[DEBUG] Printed " << ee.getName() << endl;
      }

      solution[i].first = solMin;
      solution[i].second = solMax;

      // Remove variable from objective function
      SCIP_CALL_EXC(SCIPfreeTransform(scip));
      SCIP_CALL_EXC ( SCIPchgVarObj(scip, _vars[i], 0.0));
   }

   // Release all variables
   for (SCIP_VAR * v : _vars)
   {
      SCIP_CALL_EXC( SCIPreleaseVar(scip,&v) );

   }
   _vars.clear();

   // Release all constraints
   for (SCIP_CONS * c : _const)
   {
      SCIP_CALL_EXC( SCIPreleaseCons(scip,&c) );

   }
   _const.clear();

   this->freeSCIP(&scip);

   return solution;
}

vector<double> SCIPFBAModeler::doSingleDeletionFixingProductionExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, vector<pair<HEdge,double>> reactionsToFix)
{
   SCIP *scip;
   this->initializeSCIP(&scip);

   // Number of lines and columns
   const int ncolsS = _graph.getNbEdges();
   const int nlinesS = _graph.getNbNodes();
   cout << "[DEBUG] Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   // Return vector
   std::vector<double> fluxesSol = std::vector<double>(ncolsS);
   std::vector<int> reactionsToDelete = std::vector<int>();

   // Create basic model
   this->createSteadyStateBasicVariablesAndConstraints(scip,nlinesS,ncolsS,reactionsToProduce,false,reactionBounds);

   // Fix the flux of some reactions
   this->createConstraintsToFixReactions(scip,reactionsToFix);


   vector<HEdge> reactionsToBeIgnored;
   for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
   {
      int edge_id = edge_it->first;
      HEdge r = edge_it->second;
      if (referenceFluxVector[edge_id] == 0)
      {
         reactionsToBeIgnored.push_back(r);
      }
   }
   // Add variables and constraints for deletions
   this->createDeletionsVariablesAndConstraints(scip,1,nlinesS,ncolsS,reactionsToBeIgnored,reactionBounds);

   FILE *problemFile;
   problemFile = fopen("/tmp/FBA.lp","w");

   SCIP_Var** vars = &_vars[0];
   SCIP_Cons** cons = &_const[0];

   SCIP_RESULT r;
   try{
      SCIP_CALL_EXC(SCIPwriteLp(scip,problemFile,"SingleDeletionYeast",0,SCIP_OBJSENSE_MAXIMIZE, 1.0,0.0,vars,_vars.size(),ncolsS,0,0,ncolsS,cons,_const.size(),&r));
   }catch(SCIPException & exec)
   {
      cerr<<exec.what()<<endl;
   }
   fclose(problemFile);

   // Set precision and other numerical paramters
   setNumericalParameters(scip);

   // Solve
   try
   {
      SCIP_CALL_EXC(SCIPsolve(scip) );
   }catch(SCIPException & exec)
   {
      cerr<<exec.what()<<endl;
   }


   // Get solutions

   SCIP_SOL * sol = SCIPgetBestSol(scip);
   if (sol != NULL)
   {
      for(int l=0;l<ncolsS;++l)
      {
         fluxesSol[l] = SCIPgetSolVal(scip, sol, _vars[l]);
         int y =  SCIPgetSolVal(scip, sol, _vars[l+ncolsS]) + 0.5;
         if (y == 1)
         {
            reactionsToDelete.push_back(l);
            cout << "[DEBUG] Delete reaction " << _graph.getEdge(l).getName() << " (flux: " << referenceFluxVector[l] << " in the reference vector)" << endl;
         }
      }
   }
   else
   {
      cout << "[DEBUG] Found no solution. " << endl;
   }

   // Release all variables
   for (SCIP_VAR * v : _vars)
   {
      SCIP_CALL_EXC( SCIPreleaseVar(scip,&v) );

   }
   _vars.clear();

   // Release all constraints
   for (SCIP_CONS * c : _const)
   {
      SCIP_CALL_EXC( SCIPreleaseCons(scip,&c) );

   }
   _const.clear();

   this->freeSCIP(&scip);

   return fluxesSol;
}

vector<tuple<vector<HEdge>,vector<double>>> SCIPFBAModeler::doDeletionEnumerationWithMOMA(int deletionSetSize, vector<Bounds> reactionBounds, vector<pair<HEdge,double>> reactionsToFix, vector<std::string> reactionsToBeIgnoredInTheDeletions, vector<double> referenceVector)
{
   SCIP *scip;
   this->initializeSCIP(&scip);

   // Number of lines and columns
   const int ncolsS = _graph.getNbEdges();
   const int nlinesS = _graph.getNbNodes();
   cout << "[DEBUG] Model has: " << nlinesS << " lines and " << ncolsS << " columns" << endl;

   // Empty vectors
   vector<HEdge> reactionsToProduce;

   // Return vector
   std::vector<tuple<vector<HEdge>,vector<double>>> solutions;

   std::vector<double> fluxesSol = std::vector<double>(ncolsS);
   std::vector<HEdge> reactionsToDelete = std::vector<HEdge>();

   // Create basic model
   this->createSteadyStateBasicVariablesAndConstraints(scip,nlinesS,ncolsS,reactionsToProduce,true,reactionBounds);

   // Fix the flux of some reactions
   this->createConstraintsToFixReactions(scip,reactionsToFix);

   // Identify the graph edges that cannot be deleted
   vector<HEdge> reactionsToBeIgnored;
   for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
   {
      HEdge r = edge_it->second;
      if (std::find(reactionsToBeIgnoredInTheDeletions.begin(), reactionsToBeIgnoredInTheDeletions.end(),r.getName())!=reactionsToBeIgnoredInTheDeletions.end())
      {
         reactionsToBeIgnored.push_back(r);
      }
   }
   for(pair<HEdge,double> p : reactionsToFix)
   {
      HEdge e = p.first;
      double v = p.second;
      if (v != 0)
      {
         reactionsToBeIgnored.push_back(e);
      }

   }
   // Add variables and constraints for deletions
   this->createDeletionsVariablesAndConstraints(scip,deletionSetSize,nlinesS,ncolsS,reactionsToBeIgnored,reactionBounds);

   // Add MOMA constraint and objective function
   this->createMOMAObjFunctionAndConstraints(scip,nlinesS,ncolsS,referenceVector);

   SCIPinfoMessage(scip, NULL, "*****************************************\n");
   SCIPinfoMessage(scip, NULL, "Solving the problem\n");
   SCIPinfoMessage(scip, NULL, "*****************************************\n");

   // Printing the problem to a file
   FILE *problemFile;
   problemFile = fopen("/tmp/enumeration.lp","w");
   SCIPprintOrigProblem(scip, problemFile, "lp", FALSE);
   fclose(problemFile);
   problemFile = fopen("/tmp/enumeration_cplex.lp","w");
   SCIPprintOrigProblem(scip, problemFile, "lp", TRUE);
   fclose(problemFile);

   // Set precision and other numerical paramters
   setNumericalParameters(scip);

   bool continueEnumeration = true;
   bool fixedObjectiveFuncion = false;
   do
   {
      // Solve
      try
      {
         SCIP_CALL_EXC(SCIPsolve(scip) );
      }catch(SCIPException & exec)
      {
         cerr<<exec.what()<<endl;
      }

      // Get solutions
      SCIP_SOL * sol = SCIPgetBestSol(scip);
      if (sol != NULL)
      {
         for(int l=0;l<ncolsS;++l)
         {
            fluxesSol[l] = SCIPgetSolVal(scip, sol, _vars[l]);
            int y =  SCIPgetSolVal(scip, sol, _vars[l+ncolsS]) + 0.5;
            if (y == 1)
            {
               HEdge e = this->_graph.getEdge(l);
               reactionsToDelete.push_back(e);
               cout << "[DEBUG] Deletion: " << l << endl;
            }
            for(int l=0;l<ncolsS;++l)
            {
               fluxesSol[l] = SCIPgetSolVal(scip, sol, _vars[l]);
            }
         }

         solutions.push_back(tuple<vector<HEdge>,vector<double>>(reactionsToDelete,fluxesSol));

         // Add MOMA objective function if not done yet
         if (fixedObjectiveFuncion == false)
         {
            double momaValue = SCIPgetPrimalbound(scip);
            cout << "[DEBUG] Obj: " << momaValue << endl;
            this->createConstraintToFixMOMAValue(scip, nlinesS, ncolsS, momaValue);
            fixedObjectiveFuncion = true;
         }
         // Add constraint to forbid current solution
         this->createDeletionsExclusionConstraints(scip,deletionSetSize,nlinesS,ncolsS,reactionsToDelete);

         problemFile = fopen("/tmp/enumeration.lp","w");
         SCIPprintOrigProblem(scip, problemFile, "lp", FALSE);
         fclose(problemFile);

         cout << "[DEBUG] Looking for more solutions..." << endl;
      }
      else
      {
         cout << endl << "[DEBUG]  No more solutions" << endl << "Finishing enumeration. " << endl;
         continueEnumeration = false;
      }

      // Cleanning up the solution vector to the next iteration
      reactionsToDelete.clear();

   }while (continueEnumeration);

   // Release all variables
   for (SCIP_VAR * v : _vars)
   {
      SCIP_CALL_EXC( SCIPreleaseVar(scip,&v) );

   }
   _vars.clear();

   // Release all constraints
   for (SCIP_CONS * c : _const)
   {
      SCIP_CALL_EXC( SCIPreleaseCons(scip,&c) );
   }
   _const.clear();

   this->freeSCIP(&scip);

   return solutions;
}

void SCIPFBAModeler::setNumericalParameters(SCIP* scip) {
   SCIPchgRealParam(scip, SCIPgetParam(scip, "numerics/epsilon"), 1e-7);
   SCIPchgRealParam(scip, SCIPgetParam(scip, "numerics/sumepsilon"), 1e-7);
   SCIPchgRealParam(scip, SCIPgetParam(scip, "numerics/feastol"), 1e-7);
   SCIPchgRealParam(scip, SCIPgetParam(scip, "numerics/lpfeastol"), 1e-7);
   SCIPchgRealParam(scip, SCIPgetParam(scip, "numerics/dualfeastol"), 1e-7);
   //SCIPchgRealParam(scip, SCIPgetParam(scip, "benders/solutiontol"), 1e-9);

}

bool SCIPFBAModeler::randomlySaveFluxVector(const string filename, const int ncolsS,
      SCIP* scip) {
   std::vector<double> fluxesSol = std::vector<double>(ncolsS);
   SCIP_SOL* sol;
   if (getRandomBool()) {
      sol = SCIPgetBestSol(scip);
      if (sol != NULL) {
         for (unsigned int l = 0; l < fluxesSol.size(); ++l) {
            fluxesSol[l] = SCIPgetSolVal(scip, sol, _vars[l]);
         }
      }
      writeOutput(filename.c_str(),_graph,fluxesSol);
      return true;
   }
   return false;
}

void SCIPFBAModeler::initializeSCIP(SCIP** scip)
{
   // initialize scip
   SCIP_CALL_EXC( SCIPcreate(scip) );
   // load default plugins linke separators, heuristics, etc.
   SCIP_CALL_EXC( SCIPincludeDefaultPlugins(*scip) );

   // disable scip output to stdout
   //SCIP_CALL_EXC( SCIPsetMessagehdlr(*scip, NULL) );
   // If I set the Message Handler to NULL the functions to output
   // the problem model in different formats don't work.
   // So, using the "quiet" function instead.
   SCIPsetMessagehdlrQuiet(*scip, 1);
   // create an empty problem
   SCIP_CALL_EXC( SCIPcreateProb(*scip, "FBA", NULL, NULL, NULL, NULL, NULL, NULL, NULL) );
   // set the objective sense to maximize, default is minimize
   SCIP_CALL_EXC( SCIPsetObjsense(*scip, SCIP_OBJSENSE_MAXIMIZE) );

}

void SCIPFBAModeler::freeSCIP(SCIP** scip)
{
   SCIP_CALL_EXC( SCIPfree(scip) );
}

void SCIPFBAModeler::createSteadyStateBasicVariablesAndConstraints(SCIP* scip, int nlinesS, int ncolsS, vector<HEdge> reactionsToProduce, bool addFluxBounds, vector<Bounds> reactionBounds)
{
   // Adding variables for fluxes
   vector<float> obj_coef(ncolsS);
   for(int i=0;i<ncolsS;++i)
   {
      obj_coef[i] = 0.0;
   }
   for (HEdge e : reactionsToProduce)
   {
      int i = e.getId();
      obj_coef[i] = 1.0;
   }

   for(int i=0;i<ncolsS;++i)
   {
      std::string vname = "v_"+std::to_string(i);
      SCIP_VAR* var;

      // create the SCIP_VAR object
      SCIP_CALL_EXC( SCIPcreateVar(scip, & var, vname.c_str(), -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), obj_coef[i], SCIP_VARTYPE_CONTINUOUS, TRUE, FALSE, NULL, NULL, NULL, NULL, NULL) );

      // add the SCIP_VAR object to the scip problem
      SCIP_CALL_EXC( SCIPaddVar(scip, var) );

      // storing the SCIP_VAR pointer for later access
      _vars.push_back(var);
   }

   // Adding constraints
   // Constraints
   for(int i=0;i<nlinesS;++i)
   {
      bool addSSConstraint = false;
      SCIP_CONS * cons;
      std::string svbname = "(Sveb)_"+this->_graph.getNode(i).getName()+"_"+std::to_string(i);
      SCIP_CALL_EXC(SCIPcreateConsLinear(scip, & cons, svbname.c_str(), 0, NULL, NULL, 0.0, 0.0,
            TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );

      for (auto edge_it = _graph.getEdges().begin();edge_it != _graph.getEdges().end(); edge_it++)
      {
         int edge_id = edge_it->first;
         HEdge edge = edge_it->second;
         const list<int> sources = edge.getTailNodes();
         const list<double> sourcesStoich = edge.getTailStoichiometry();
         list<int>::const_iterator it1;
         list<double>::const_iterator it2;
         for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
         {
            int nodeId = *it1;
            double nodeStoich = *it2;
            if(nodeId == i)
            {
               addSSConstraint = true;
               //variationOfCompound -= nodeStoich*fluxArray[edge_id];
               SCIP_CALL_EXC( SCIPaddCoefLinear(scip, cons, _vars[edge_id], -nodeStoich) );
            }
         }
         list<int> products = edge.getHeadNodes();
         list<double> productsStoich = edge.getHeadStoichiometry();
         for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
         {
            int nodeId = *it1;
            double nodeStoich = *it2;
            if(nodeId == i)
            {
               addSSConstraint = true;
               //variationOfCompound += nodeStoich*fluxArray[edge_id];
               SCIP_CALL_EXC( SCIPaddCoefLinear(scip, cons, _vars[edge_id], nodeStoich) );
            }
         }
      }
      // Sv[i] = b[i]
      if (addSSConstraint)
      {
         if (!this->_graph.getNode(i).isBoundary())
         {
            SCIP_CALL_EXC( SCIPaddCons(scip, cons) );
            _const.push_back(cons);
         }
      }
   }

   // Constraints for reactions fluxes
   set<int> constrainedReactions;
   if (addFluxBounds)
   {
      for (Bounds b : reactionBounds)
      {
         int j = b.id;
         assert(j<ncolsS);

         std::string bname = "b_bounds_"+std::to_string(j);
         SCIP_CONS * cons;
         SCIP_CALL_EXC(SCIPcreateConsLinear(scip, & cons, bname.c_str(), 0, NULL, NULL, b.lb, b.ub,
               TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
         SCIP_CALL_EXC( SCIPaddCoefLinear(scip, cons, _vars[j], 1.0) );
         SCIP_CALL_EXC( SCIPaddCons(scip, cons) );
         _const.push_back(cons);

         constrainedReactions.insert(j);
      }

      for(int j=0;j<ncolsS;++j)
      {
         if (constrainedReactions.find(j) == constrainedReactions.end())
         {
            std::string cname = "v_bounds_"+std::to_string(j);
            SCIP_CONS * cons;
            SCIP_CALL_EXC(SCIPcreateConsLinear(scip, & cons, cname.c_str(), 0, NULL, NULL, -_flux_max, _flux_max,
                  TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
            SCIP_CALL_EXC( SCIPaddCoefLinear(scip, cons, _vars[j], 1.0) );
            SCIP_CALL_EXC( SCIPaddCons(scip, cons) );
            _const.push_back(cons);
         }
      }
   }
}

void SCIPFBAModeler::createConstraintsToFixReactions(SCIP* scip, vector<pair<HEdge,double>> reactionsToFix)
{
   for(pair<HEdge,double> p : reactionsToFix)
   {
      HEdge e = p.first;
      double v = p.second;
      std::string cname = "v_fix_"+std::to_string(e.getId());
      SCIP_CONS * cons;
      SCIP_CALL_EXC(SCIPcreateConsLinear(scip, & cons, cname.c_str(), 0, NULL, NULL, v, v,
            TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, cons, _vars[e.getId()], 1.0) );
      SCIP_CALL_EXC( SCIPaddCons(scip, cons) );
      _const.push_back(cons);
   }
}

void SCIPFBAModeler::createDeletionsVariablesAndConstraints(SCIP* scip, int numberOfDeletions, int nlinesS, int ncolsS,  vector<HEdge> reactionsToBeIgnoredAsDeletions,  vector<Bounds> reactionBounds)
{
   // Creating constraint to count deletions
   SCIP_CONS * delcons;
   std::string dcname = "max_of_deletions";
   SCIP_CALL_EXC(SCIPcreateConsLinear(scip, & delcons, dcname.c_str(), 0, NULL, NULL, numberOfDeletions, numberOfDeletions,
         TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );

   // Adding variables for deletions and summing them in the constraint
   for(int i=0;i<ncolsS;++i)
   {
      std::string vname = "y_"+std::to_string(i);
      SCIP_VAR* y;

      // create the SCIP_VAR object
      SCIP_CALL_EXC( SCIPcreateVar(scip, & y, vname.c_str(), 0, 1, 0, SCIP_VARTYPE_BINARY, TRUE, FALSE, NULL, NULL, NULL, NULL, NULL) );

      // add the SCIP_VAR object to the scip problem
      SCIP_CALL_EXC( SCIPaddVar(scip, y) );

      // storing the SCIP_VAR pointer for later access
      _vars.push_back(y);

      // Adding y to the counting constraint
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, delcons, y, 1.0) );
   }

   // Adding constraint for fixing the amount of deletions
   SCIP_CALL_EXC( SCIPaddCons(scip, delcons) );
   _const.push_back(delcons);

   for(HEdge r :  reactionsToBeIgnoredAsDeletions)
   {
      int i = r.getId();
      // Setting y to zero if the reaction is in the list of reactions
      // to be ignored.
      SCIP_CONS * fixatzero;
      std::string cname = "FIX_at_zero_"+std::to_string(i);
      SCIP_CALL_EXC(SCIPcreateConsLinear(scip, &fixatzero, cname.c_str(), 0, NULL, NULL, 0.0, 0.0,
            TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, fixatzero, _vars[ncolsS + i], 1.0) );
      SCIP_CALL_EXC( SCIPaddCons(scip, fixatzero) );
      _const.push_back(fixatzero);
   }
   for(int i=0;i<ncolsS;++i)
   {
      assert (i<reactionBounds.size());
      Bounds b = reactionBounds[i];
      // For each y relate them with the corresponding flux
      // in order to make it zero if y = 1.
      // First: LB <= v + yLB
      SCIP_CONS * fluxlowerbound;
      std::string dclowername = "deletion_constraint_lower_"+std::to_string(i);
      SCIP_CALL_EXC(SCIPcreateConsLinear(scip, &fluxlowerbound, dclowername.c_str(), 0, NULL, NULL, b.lb, std::numeric_limits<double>::max(),
            TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, fluxlowerbound, _vars[b.id], 1.0) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, fluxlowerbound, _vars[ncolsS + b.id], b.lb) );
      SCIP_CALL_EXC( SCIPaddCons(scip, fluxlowerbound) );
      _const.push_back(fluxlowerbound);
      // Then: v +yUB <= UB
      SCIP_CONS * fluxuperbound;
      std::string dcuppername = "deletion_constraint_upper_"+std::to_string(i);
      SCIP_CALL_EXC(SCIPcreateConsLinear(scip, &fluxuperbound, dcuppername.c_str(), 0, NULL, NULL, -std::numeric_limits<double>::max() , b.ub,
            TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, fluxuperbound, _vars[b.id], 1.0) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, fluxuperbound, _vars[ncolsS + b.id], b.ub) );
      SCIP_CALL_EXC( SCIPaddCons(scip, fluxuperbound) );
      _const.push_back(fluxuperbound);
   }

}

void SCIPFBAModeler::createMOMAObjFunctionAndConstraints(SCIP* scip, int nlinesS, int ncolsS, vector<double> referenceFlux)
{
   // Adding variables for deletions and summing them in the constraint
   for(int i=0;i<ncolsS;++i)
   {
      std::string vname = "MOMA_"+std::to_string(i);
      SCIP_VAR* moma;

      // create the SCIP_VAR object
      SCIP_CALL_EXC( SCIPcreateVar(scip, &moma, vname.c_str(), 0.0, std::numeric_limits<float>::max(), -1.0, SCIP_VARTYPE_CONTINUOUS, TRUE, FALSE, NULL, NULL, NULL, NULL, NULL) );

      // add the SCIP_VAR object to the scip problem
      SCIP_CALL_EXC( SCIPaddVar(scip,moma) );

      // storing the SCIP_VAR pointer for later access
      _vars.push_back(moma);

      SCIP_CONS * momaConstraint1;
      std::string cname = "MomaModuleDiff1_"+std::to_string(i);
      SCIP_CALL_EXC(SCIPcreateConsLinear(scip, &momaConstraint1, cname.c_str(), 0, NULL, NULL,-referenceFlux[i], std::numeric_limits<double>::max(),
            TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, momaConstraint1, moma, 1.0) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, momaConstraint1, _vars[i], -1.0) );
      SCIP_CALL_EXC( SCIPaddCons(scip, momaConstraint1) );
      _const.push_back(momaConstraint1);

      SCIP_CONS * momaConstraint2;
      std::string cname2 = "MomaModuleDiff2_"+std::to_string(i);
      SCIP_CALL_EXC(SCIPcreateConsLinear(scip, &momaConstraint2, cname2.c_str(), 0, NULL, NULL, referenceFlux[i], std::numeric_limits<double>::max(),
            TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, momaConstraint2, moma, 1.0) );
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, momaConstraint2, _vars[i], 1.0) );
      SCIP_CALL_EXC( SCIPaddCons(scip, momaConstraint2) );
      _const.push_back(momaConstraint2);
   }

}
void SCIPFBAModeler::createDeletionsExclusionConstraints(SCIP* scip, unsigned int numberOfDeletions, int nlinesS, int ncolsS, vector<HEdge> solutionToBeExcluded)
{
   SCIP_CALL_EXC(SCIPfreeTransform(scip));
   if (solutionToBeExcluded.size() > 0)
   {
      assert(solutionToBeExcluded.size() == numberOfDeletions);
      SCIP_CONS * excludeSolution;
      std::string cname = "exclude_y_";
      SCIP_CALL_EXC(SCIPcreateConsLinear(scip, &excludeSolution, cname.c_str(), 0, NULL, NULL, 0.0, numberOfDeletions - 1,
            TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
      for(HEdge e : solutionToBeExcluded)
      {
         int l = e.getId();
         SCIP_CALL_EXC( SCIPaddCoefLinear(scip, excludeSolution, _vars[ncolsS + l], 1.0) );
      }
      SCIP_CALL_EXC( SCIPaddCons(scip, excludeSolution) );
      _const.push_back(excludeSolution);
   }
}

void SCIPFBAModeler::createConstraintToFixMOMAValue(SCIP* scip, int nlinesS, int ncolsS, double momaValue)
{
   SCIP_CALL_EXC(SCIPfreeTransform(scip));
   SCIP_CONS * fixMOMAObjFunction;
   std::string cname = "fixMOMA_";
   SCIP_CALL_EXC(SCIPcreateConsLinear(scip, &fixMOMAObjFunction, cname.c_str(), 0, NULL, NULL, momaValue, momaValue,
         TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
   for(int l = 0; l < ncolsS; ++l)
   {
      SCIP_CALL_EXC( SCIPaddCoefLinear(scip, fixMOMAObjFunction, _vars[2*ncolsS + l], -1.0) );
   }
   SCIP_CALL_EXC( SCIPaddCons(scip, fixMOMAObjFunction) );
   _const.push_back(fixMOMAObjFunction);
}


void SCIPFBAModeler::createConstraintToFixFBAObjFunction(SCIP* scip, int nlinesS, int ncolsS, double optimalValue,vector<HEdge> reactionsToProduce)
{
   // Adding variables for fluxes
   vector<float> obj_coef(ncolsS);
   for(int i=0;i<ncolsS;++i)
   {
      obj_coef[i] = 0.0;
   }
   // Setting the correct original coefficients
   for (HEdge e : reactionsToProduce)
   {
      int i = e.getId();
      obj_coef[i] = 1.0;
   }

   SCIP_CALL_EXC(SCIPfreeTransform(scip));
   SCIP_CONS * fixObjFunction;
   std::string cname = "fixObjFunc_";
   SCIP_CALL_EXC(SCIPcreateConsLinear(scip, &fixObjFunction, cname.c_str(), 0, NULL, NULL, optimalValue, optimalValue,
         TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
   for(int l = 0; l < ncolsS; ++l)
   {
      if (obj_coef[l] == 1.0)
      {
         cout << "[DEBUG] Fixing reaction " << l << " at " << optimalValue << endl;
         SCIP_CALL_EXC( SCIPaddCoefLinear(scip, fixObjFunction, _vars[l], obj_coef[l]) );
      }
   }
   SCIP_CALL_EXC( SCIPaddCons(scip, fixObjFunction) );
   _const.push_back(fixObjFunction);
}

bool SCIPFBAModeler::getRandomBool()
{
   if (!this->_randGeneratorInitialized)
   {
      srand(time(NULL));
      this->_randGeneratorInitialized = true;
   }

   int randomNumber = rand()%1000;
   if (randomNumber < 500)
   {
      return false;
   }
   return true;
}
