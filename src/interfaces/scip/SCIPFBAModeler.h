/*
 * SCIPFBAModeler.hpp
 *
 *  Created on: 11 janv. 2017
 *      Author: randrade
 */

#ifndef SRC_INTERFACES_SCIP_SCIPFBAMODELER_H_
#define SRC_INTERFACES_SCIP_SCIPFBAMODELER_H_

using namespace std;

#include <cmath>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <limits>
#include <set>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include <scip/scip.h>
#include <scip/scipdefplugins.h>

#include <LiftedWeightSpaceSolver.h>

#include "../../hypergraph/HGraph.h"
#include "../../io/Bounds.h"
#include "../../io/read_bounds.h"
#include "../../io/read_writeFile.h"
#include "scip_exception.h"
#include "LiftedWeightSpaceSolver.h"
#include "PolySCIPConfig.h"



// ===========================================================================
//                             Definitions
// ===========================================================================
#define Optimal 2

class SCIPFBAModeler {
public:
   SCIPFBAModeler(const HGraph&  graph);

   void doPolyObjectiveDeleting(const int deletionSetSize, const vector<HEdge>& reactionsToMaximize, const vector<HEdge>& reactionsToMinimize, const vector<Bounds>& reactionBounds, vector<pair<HEdge,double>> reactionsToFix, const vector<std::string>& reactionsToBeIgnoredInTheDeletions, const std::string& output_filename);
   void doPolyRobustnessObjectiveDeleting(const int deletionSetSize, const vector<HEdge>& reactionsToMaximize, const vector<HEdge>& reactionsToMinimize,  const vector<HEdge>& robustnessReactions, const vector<Bounds>& reactionBounds, vector<pair<HEdge,double>> reactionsToFix, const vector<std::string>& reactionsToBeIgnoredInTheDeletions, const std::string& output_filename);
   vector<double> doFBA(vector<HEdge> reactionsToProduce,vector<Bounds> reactionBounds);
   vector<double> doFBAFixingProductions(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<pair<HEdge,double>> reactionsToFix);
   vector<pair<double,double>> doOptimalFVA(double optimalSolutionValue, vector<HEdge> reactionsToProduce,vector<Bounds> reactionBounds, vector<pair<HEdge,double>> reactionsToFix);
   vector<tuple<vector<HEdge>,vector<double>>> doDeletionEnumerationWithMOMA(int deletionSetSize, vector<Bounds> reactionBounds, vector<pair<HEdge,double>> reactionsToFix, vector<std::string> reactionsToBeIgnoredInTheDeletions, vector<double> referenceFluxVector);
   vector<double> doSingleDeletionFixingProductionExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, vector<pair<HEdge,double>> reactionsToFix);
   vector<double> doSingleDeletionMOMAFixingProductionExcludingZerosFromReference(vector<HEdge> reactionsToProduce, vector<Bounds> reactionBounds, vector<double> referenceFluxVector, vector<pair<HEdge,double>> reactionsToFix);

   virtual ~SCIPFBAModeler();

private:
   const HGraph _graph;

   /** @brief Vector with all model variables
    * To access variable information (objective value, bounds,
    * etc.) use the SCIP_VAR * pointer. Since we want to know the
    * value of each variable in the solution, we have to store
    * these pointers.
    */
   std::vector<SCIP_VAR *> _vars;

   /** @brief Vector with all model constraints
    * To access constraint information (right hand side, left hand
    * side, dual values, etc.) use the SCIP_CONS * pointer. For the
    * n-queens problem we do not really need to store them but we
    * do for illustration.
    */
   std::vector<SCIP_CONS *> _const;

   /**
    * The maximum amount a flux variable can attain.
    */
   const double _flux_max;

   bool _randGeneratorInitialized = false;

   void initializeSCIP(SCIP** scip);
   void freeSCIP(SCIP** scip);
   void createSteadyStateBasicVariablesAndConstraints(SCIP* scip, int nlinesS, int ncolsS, vector<HEdge> reactionsToProduce, bool addFluxBounds, vector<Bounds> reactionBounds);
   void createConstraintsToFixReactions(SCIP* scip, vector<pair<HEdge,double>> reactionsToFix);
   void createDeletionsVariablesAndConstraints(SCIP* scip, int numberOfDeletions, int nlinesS, int ncolsS, vector<HEdge> reactionsToBeIgnoredAsDeletions,vector<Bounds> reactionBounds);
   void createDeletionsExclusionConstraints(SCIP* scip, unsigned int numberOfDeletions, int nlinesS, int ncolsS, vector<HEdge> solutionToBeExcluded);
   void createMOMAObjFunctionAndConstraints(SCIP* scip, int nlinesS, int ncolsS, vector<double> referenceFlux);
   void createConstraintToFixMOMAValue(SCIP* scip, int nlinesS, int ncolsS, double momaValue);
   void createConstraintToFixFBAObjFunction(SCIP* scip, int nlinesS, int ncolsS, double optimalValue,vector<HEdge> reactionsToProduce);
   bool getRandomBool();
   bool randomlySaveFluxVector(const string filename, const int ncolsS, SCIP* scip);
   void setNumericalParameters(SCIP* scip);
};



#endif /* SRC_INTERFACES_SCIP_SCIPFBAMODELER_H_ */
