#ifndef readXML_H_
#define readXML_H_

#include <assert.h>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <set>
#include<vector>

#include <libxml++/libxml++.h>
#include <libxml++/parsers/textreader.h>

#include "Bounds.h"
#include "../hypergraph/HGraph.h"

using namespace std;

void readXML (const char *fileName, HGraph & graph);
void readBNfile (char *fileName, HGraph & graph);
void readListOfReactions (const char *fileName, HGraph & graph, vector<HEdge> & reactionList);
void readListOfFluxes (const char *fileName, HGraph & graph, vector<double> & fluxes);
std::vector<Bounds> readReactionBoundsFile (const char *fileName, HGraph & graph);
void writeXML (const char *fileName, HGraph & graph);
void writeOutput (const char *fileName, const HGraph & graph, vector<double> & solutionFluxVector);
void writeOutput (const char *fileName, const HGraph & graph, vector<tuple<vector<HEdge>,vector<double>>>  & solutionFluxVector);
void printOutput (HGraph & graph, vector<double> & solutionFluxVector);
void printOutput (HGraph & graph, vector<tuple<vector<HEdge>,vector<double>>>  & solutionFluxVector);
class reader_exception : public std::exception {
   const char* _msg;
    public:
    reader_exception(const char* msg) : _msg(msg)
    {
    }
    virtual const char* what() const throw()
     {
       return _msg;
     }
};

#endif // ___CLASS_H___
