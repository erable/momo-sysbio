/*
 * Bounds.h
 *
 *  Created on: 14 juil. 2016
 *      Author: randrade
 */

#ifndef SRC_BOUNDS_H_
#define SRC_BOUNDS_H_

struct Bounds
{
    int id;
    double lb;
    double ub;
};


#endif /* SRC_BOUNDS_H_ */
