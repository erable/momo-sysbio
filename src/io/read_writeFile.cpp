#include "read_writeFile.h"

//! Reading xml and filling an HGraph Object
/*!
 * \brief read xml file and construct a  graph
 * \param fileName  name of the xml file
 * \param graph: HGraph object,
 * HGraph will be updates, a node will be created for every metabolites insides
 * the xml file.
 * Each reaction will be one edge
 */
void readXML (const char *fileName, HGraph & graph)
{
   string reactionName = "";
   string reactionRealName = "";
   list<string> products;
   list<double> productsStoich;
   list<string> reactants;
   list<double> reactantsStoich;
   string rev = "false";
   try
   {
      std::ifstream infile(fileName);
      if (infile.good() == false) throw reader_exception("Network file not found.");
      xmlpp::TextReader reader(fileName);
      while(reader.read())
      {
         string field = string(reader.get_name().c_str());
         //=========================================================
         // FILLING METABOLITES (SPECIES)
         //=========================================================
         if( field.compare("species") == 0)
         {
            if (reader.has_attributes())
            {
               reader.move_to_first_attribute();
               string species = "";
               bool boundary = false;
               do
               {
                  field = string(reader.get_name().c_str());
                  if(field.compare("id") == 0)
                  {
                     //HERE GET SPECIES!
                     species = string(reader.get_value().c_str());
                  }
                  else if (field.compare("boundaryCondition") == 0)
                  {
                     string sboundary = string(reader.get_value().c_str());
                     if (sboundary.compare("true") == 0)
                     {
                        boundary = true;
                     }
                  }
               } while (reader.move_to_next_attribute());
               if(species.length() > 0)
               {
                  graph.addNode(species,boundary);
               }
               reader.move_to_element();
            }
            else
            { // here put error
               cout << "Nodes no ID!!" << endl;
            }

         }
         //=========================================================
         // Getting Reactions
         //=========================================================
         if (field.compare("reaction") == 0)
         {
            if (reactionName == "")
            {
               // GET REACTION ID! & name
               if (reader.has_attributes())
               {
                  reader.move_to_first_attribute();
                  do
                  {
                     field = string(reader.get_name().c_str());
                     if (field.compare("id") == 0)
                     {
                        reactionName =  string(reader.get_value().c_str());
                     }
                     if (field.compare("name") == 0)
                     {
                        reactionRealName = string(reader.get_value().c_str());
                     }
                     if (field.compare("reversible") == 0)
                     {
                        rev =  string(reader.get_value().c_str());
                     }
                  } while (reader.move_to_next_attribute());
                  reader.move_to_element();
               }
            }
            else
               //=========================================================
               // Writing Reactions to the graph
               //=========================================================
            {// Adding the reaction to graph
               field = string(reader.get_name().c_str());
               if (field.compare("true") == 0)
               {
                  graph.addEdge("+" + reactionName, reactionRealName, reactants, reactantsStoich, products, productsStoich);
                  graph.addEdge("-" +  reactionName, reactionRealName, reactants, reactantsStoich, products, productsStoich);
               }
               else
               {
                  //graph.addEdge("*" + reactionName, reactants, reactantsStoich, products, productsStoich);
                  graph.addEdge(reactionName, reactionRealName, reactants, reactantsStoich, products, productsStoich);
                  HEdge e = graph.getEdge(graph.getNbEdges());
                  //cout << e.getName() << " - " << e.getDescription() << " <-> " <<reactionRealName << endl;
               }

               products.clear();
               productsStoich.clear();
               reactants.clear();
               reactantsStoich.clear();
               reactionName= "";
               reactionRealName = "";
            }
         }
         // Finding reactants and products:
         bool producB;
         if (field.compare("listOfProducts") == 0)
         {
            producB = true;
         }
         if (field.compare("listOfReactants") == 0)
         {
            producB = false;
         }
         if (field.compare("speciesReference") == 0)
         {
            bool hasStoichiometry = false;
            if (reader.has_attributes())
            {
               reader.move_to_first_attribute();
               do
               {
                  field = string(reader.get_name().c_str());
                  if (field.compare("species") == 0)
                  {
                     if (producB)//products
                     {
                        products.push_front(string(reader.get_value().c_str()) );
                     }
                     else // reactant
                     {
                        reactants.push_front(string(reader.get_value().c_str()));
                     }
                  }
                  reader.move_to_next_attribute();
                  field = string(reader.get_name().c_str());
                  if (field.compare("stoichiometry") == 0)
                  {
                     hasStoichiometry = true;
                     if (producB)//products
                     {
                        string st = string(reader.get_value().c_str());
                        double st_v = stod(st);
                        productsStoich.push_front(st_v);
                     }
                     else // reactant
                     {
                        string st = string(reader.get_value().c_str());
                        double st_v = stod(st);
                        reactantsStoich.push_front(st_v);
                     }
                  }
               } while (reader.move_to_next_attribute());
               reader.move_to_element();
               //field = string(reader.get_name().c_str());
            }
            if (!hasStoichiometry)
            {
               if (producB)//products
               {
                  productsStoich.push_front(1.0);
               }
               else // reactant
               {
                  reactantsStoich.push_front(1.0);
               }
            }
         }
      }
   }
   catch (const exception& e)
   {
      cout << "Exception caught: " << e.what() << endl;
      exit (EXIT_FAILURE);
   }
}

/**
 * Gets the filename if the file containing a list of
 * reaction names, one by line and also the network (graph).
 *
 * Returns via reactionList a list of reactions read from
 * the input file.
 */
void readListOfReactions (const char *fileName, HGraph & graph, vector<HEdge> & reactionList)
{
   std::ifstream readList(fileName);
   if (! readList.good())
   {
      throw std::invalid_argument("File containing the list of reactions does not exist." );
   }
   std::string line;
   char *reactionName;// length: random choose here, can be increased
   while (std::getline(readList, line))
   {
      char* linechar = (char*) line.c_str();
      reactionName = strtok( linechar, " " );
      assert(reactionName != NULL);
      boost::optional<HEdge> e = graph.getEdge(reactionName);
      if (e)
      {
         reactionList.push_back(e.get());
      }
      else
      {
         stringstream ss;
         ss << "ERROR: Reaction " << reactionName << " does not exist.";
         throw std::invalid_argument(ss.str());
      }
   }
}

/**
 * Gets the filename if the file containing a list of
 * reaction names and their respective fluxes, in the
 * format reaction=flux, one by line and also the
 * network (graph).
 *
 * Returns via fluxes a vector of the fluxes read from
 * the input file, respecting the order of the
 * reactions given by their ID in the network.
 */
void readListOfFluxes (const char *fileName, HGraph & graph, vector<double> & fluxes)
{
   std::ifstream readList(fileName);
   if (! readList.good())
   {
      throw std::invalid_argument("File containing the list of fluxes does not exist." );
   }
   std::string line;
   char *reactionName;
   char *fluxString;
   int fluxVectorCorrectSize = graph.getNbEdges();
   // Be sure that the fluxes vector has the right size
   fluxes.resize(fluxVectorCorrectSize, 0.0);
   while (std::getline(readList, line))
   {
      char* linechar = (char*) line.c_str();
      reactionName = strtok( linechar, "=" );
      assert(reactionName != NULL);
      fluxString = strtok( NULL, "=" );
      HEdge e = *graph.getEdge(reactionName);
      fluxes[e.getId()] = std::stod(fluxString);
   }
}

//We should be using special xml library but doc not coherent
//! Writting a pseudo- xml to visualize an and HGraph Object
/*!
 * \brief From a HGraph, wrtie an xml for visualisation
 * \param fileName  name of the xml file
 * \param graph: HGraph object,
 */
void writeXML (const char *fileName, HGraph & graph)
{
   FILE *writingXML = fopen (fileName, "w");
   // Header for conversion
   fprintf (writingXML, "<?xml version=\"1.0\"  encoding=\"UTF-8\"?>\n");
   fprintf (writingXML, "<sbml xmlns=\"http://www.sbml.org/sbml/level2\" version=\"1\" level=\"2\" xmlns:html=\"http://www.w3.org/1999/xhtml\">\n");
   fprintf (writingXML, "<model id=\"toyexample\" name=\"NA\">\n" );
   fprintf (writingXML," <listOfCompartments>\n");
   fprintf (writingXML, "<compartment id=\"NA\" name =\"NA\" /> \n ");
   fprintf (writingXML, "</listOfCompartments>\n");
   //=========== Species =================
   fprintf (writingXML, "<listOfSpecies>\n");
   map<int, HNode> compounds = graph.getNodes();

   for (auto it = compounds.begin(); it != compounds.end(); ++it)
   {
      int initialAmount = (it->second).isBlack()? 1 : 0;
      stringstream stmp;
      stmp << it->first;
      string idMet = string("__")+ stmp.str() + string("_met");
      fprintf (writingXML, "  <species id=\"%s\" name=\"%s\" compartment=\"NA\" boundaryCondition=\"false\"  initialAmount=\"%i\" />\n", idMet.c_str(), (it->second).getName().c_str(), initialAmount);
   }
   map<int,HEdge> allEdges = graph.getEdges();
   fprintf (writingXML, "</listOfSpecies>\n");
   //=========== Reactions =================
   fprintf (writingXML, "<listOfReactions>\n" );

   for (auto itHE = allEdges.begin(); itHE != allEdges.end(); ++itHE)
   {
      auto myEdge = (itHE->second);
      stringstream stmp;
      stmp << itHE->first;
      string idReac= string("__")+ stmp.str() + string("_reac");
      fprintf (writingXML,"  <reaction id=\"%s\" name=\"%s\" reversible=\"false\">\n",idReac.c_str(), myEdge.getName().c_str());
      //  the reactants

      fprintf (writingXML,"  <listOfReactants>\n");
      list <int> nodes = myEdge.getHeadNodes();
      for (auto itN=nodes.begin(); itN!=nodes.end(); ++itN)
      {
         stringstream stmp;
         stmp << *itN;
         string idMet = string("__") + stmp.str() + string("_met");
         fprintf (writingXML,"    <speciesReference species=\"%s\" stoichiometry=\"NaN\"/>\n", idMet.c_str());  // we dont have the stochiometry for now
      }
      fprintf (writingXML,"  </listOfReactants> \n" );
      //  the products
      fprintf (writingXML,"  <listOfProducts>\n");
      nodes = myEdge.getTailNodes();
      for (auto itN=nodes.begin(); itN!=nodes.end(); ++itN)
      {
         stringstream stmp;
         stmp << *itN;
         string toto =  string("__") + stmp.str() + string("_met");
         fprintf (writingXML,"    <speciesReference species=\"%s\" stoichiometry=\"NaN\"/>\n",  toto.c_str() );
      }
      fprintf (writingXML,"  </listOfProducts>\n");
      fprintf (writingXML, "  </reaction>\n");
   }
   fprintf (writingXML, "</listOfReactions>\n" );
   fprintf (writingXML, "</model>\n");

   fprintf (writingXML,"</sbml>");

   fclose (writingXML);

}

/**
 * Writes a vector of fluxes to filename in the format
 * reactionID=flux
 *
 */
void writeOutput (const char *fileName, const HGraph & graph, vector<double> & solutionFluxVector)
{
   fstream output;
   output.open(fileName, fstream::out);
   for(unsigned int i=0; i<solutionFluxVector.size();++i)
   {
      output << graph.getEdge(i).getName();
      output << "=";
      output << solutionFluxVector[i] << endl;
   }
   output.close();
}

/**
 * Writes a vector of deletions ans fluxes to filename in the format
 * deletions
 * reactionID=flux
 * -
 * deletions
 * reactionID=flux
 * -
 * etc.
 *
 */
void writeOutput (const char *fileName, const HGraph & graph, vector<tuple<vector<HEdge>,vector<double>>> & solutionFluxVector)
{
   fstream output;
   output.open (fileName, fstream::out);
   for (vector<tuple<vector<HEdge>,vector<double>>>::const_iterator i = solutionFluxVector.begin(); i != solutionFluxVector.end(); ++i)
   {
      vector<HEdge> deletions =  get<0>(*i);
      output << "Knock-out(s): ";
      for(vector<HEdge>::const_iterator j = deletions.begin(); j != deletions.end(); ++j)
      {
         HEdge r = (*j);
         output << r.getName() << " ";
      }
      output << endl;
      vector<double> fluxes = get<1>(*i);
      output << "Fluxes:" << endl;
      for(unsigned int j=0; j<fluxes.size();++j)
      {
         output << graph.getEdge(j).getName();
         output << "=";
         output << fluxes[j] << endl;
      }
      output << "-" << endl;
   }
   output.close();
}

/**
 * Writes a vector of fluxes to stdout in the format
 * reactionID=flux
 *
 */
void printOutput (HGraph & graph, vector<double> & solutionFluxVector)
{
   for(unsigned int i=0; i<solutionFluxVector.size();++i)
   {
      cout << graph.getEdge(i).getName() << "=" << solutionFluxVector[i] << endl;
   }
}

/**
 * Writes a vector of deletions ans fluxes to stdout in the format
 * deletions
 * reactionID=flux
 * -
 * deletions
 * reactionID=flux
 * -
 * etc.
 **/
void printOutput (HGraph & graph, vector<tuple<vector<HEdge>,vector<double>>> & solutionFluxVector)
{
   for (vector<tuple<vector<HEdge>,vector<double>>>::const_iterator i = solutionFluxVector.begin(); i != solutionFluxVector.end(); ++i) {
      vector<HEdge> deletions =  get<0>(*i);
      for(vector<HEdge>::const_iterator j = deletions.begin(); j != deletions.end(); ++j)
      {
         HEdge r = (*j);
         cout << r.getName() << " ";
      }
      cout << endl;
      vector<double> fluxes = get<1>(*i);
      for(unsigned int j=0; j<fluxes.size();++j)
      {
         cout << graph.getEdge(j).getName();
         cout << "=";
         cout << fluxes[j] << endl;
      }
   }
}

