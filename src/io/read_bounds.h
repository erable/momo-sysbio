/*
 * read_bounds.h
 *
 *  Created on: 14 juil. 2016
 *      Author: randrade
 */

#ifndef SRC_HYPERGRAPH_READ_BOUNDS_H_
#define SRC_HYPERGRAPH_READ_BOUNDS_H_

#include<vector>

#include "Bounds.h"
#include "../hypergraph/HGraph.h"


std::vector<Bounds> readReactionBoundsFile (const char *fileName, HGraph & graph);

#endif /* SRC_HYPERGRAPH_READ_BOUNDS_H_ */
