/*
 * read_bounds.cpp
 *
 *  Created on: 14 juil. 2016
 *      Author: randrade
 */

#include "read_bounds.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>

#include "../hypergraph/HGraph.h"


std::vector<Bounds> readReactionBoundsFile (const char *fileName, HGraph & graph)
{
   std::vector<Bounds> boundsForFBA;

   FILE *readBn = fopen (fileName, "r");
   if (readBn == NULL)
   {
      std::cerr << "Error opening the bounds file" << std::endl;
      exit(EXIT_FAILURE);
   }
   char *line= new char[1000];
   char *name;// length: random choose here, can be increased
   char *bmin = NULL;
   char *bmax = NULL;

   while (fgets(line, 1000, readBn) != NULL)
   {
      name = strtok( line, " \t" );
      bmin = strtok( NULL, " \t" );
      bmax = strtok( NULL, "\n" );
      assert(bmin != NULL);
      assert(bmax != NULL);
      double bdoublevaluemin = atof(bmin);
      double bdoublevaluemax = atof(bmax);
      if (bdoublevaluemin > bdoublevaluemax){
         double t;
         t = bdoublevaluemax;
         bdoublevaluemax = bdoublevaluemin;
         bdoublevaluemin = t;
      }

      boost::optional<HEdge> e = graph.getEdge(name);
      if (e != boost::none)
      {
         int edgeId =e->getId();
         Bounds b;
         b.id = edgeId;
         b.lb = bdoublevaluemin;
         b.ub = bdoublevaluemax;
         boundsForFBA.push_back(b);
      }
      else
      {
         std::cerr << "Error opening the bounds file, reaction " << name << " not found in the network." << std::endl;
         exit(EXIT_FAILURE);
      }
   }
   fclose(readBn);

   return boundsForFBA;

}





