/*
 * InputParser.h
 *
 *  Created on: 13 juin 2017
 *      Author: randrade
 */

#ifndef SRC_IO_INPUTPARSER_H_
#define SRC_IO_INPUTPARSER_H_

#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>
#include <iostream>
#include <string>
#include <vector>

class InputParser {
public:
   InputParser();
   void parseInput(int argc, char** argv);
   const std::string& getBoundsFileName() const;
   const std::string& getNetworkFileName() const;
   const std::string& getOutputFileName() const;
   const std::string& getDeletionsCandidateFileName() const;
   const std::string& getReferenceFluxFileName() const;
   const std::string& getRobustnessReactionsFileName() const;
   const int& getOptimizerTimeout() const;
   const int& getOptimizerInternalMemory() const;
   const bool didAskedForHelp() const;
   const bool shouldEnumerateSolutions() const;
   const bool doFVA() const;
   void printHelpDescription() const;
   const std::vector<std::string>& getReactionIdsToMaximize() const;
   const std::vector<std::string>& getReactionIdsToMinimize() const;
   const std::vector<std::pair<std::string,double>>& getReactionToFix() const;
   const int& getDeletionsSetSize() const;



   virtual ~InputParser();

private:
   boost::program_options::options_description _desc;
   bool _askedForHelp = false;
   bool _doFVA = false;
   std::string _bounds_file;
   std::string _network_file;
   std::string _output_file = "output.txt";
   std::string _deletions_candidates_file;
   std::string _reference_flux_file;
   std::string _robustness_reactions_file;
   std::vector<std::string> _reactions_to_maximize;
   std::vector<std::string> _reactions_to_minimize;
   std::vector<std::pair<std::string,double>> _reactions_to_fix;
   int _optTimeout;
   int _optMemory;
   int _deletionSetSize = 0;
   int _enumerateSolutions = false;
};
inline const bool InputParser::didAskedForHelp() const
{
   return _askedForHelp;
}

inline const bool InputParser::shouldEnumerateSolutions() const
{
   return _enumerateSolutions;
}

inline const bool InputParser::doFVA() const
{
   return _doFVA;
}

inline const std::string& InputParser::getBoundsFileName() const
{
   return _bounds_file;
}

inline const std::string& InputParser::getNetworkFileName() const
{
   return _network_file;
}

inline const std::string& InputParser::getDeletionsCandidateFileName() const
{
   return _deletions_candidates_file;
}

inline const std::string& InputParser::getReferenceFluxFileName() const
{
   return _reference_flux_file;
}

inline const std::string& InputParser::getRobustnessReactionsFileName() const
{
   return _robustness_reactions_file;
}

inline const std::string& InputParser::getOutputFileName() const
{
   return _output_file;
}

inline const std::vector<std::string>& InputParser::getReactionIdsToMaximize() const
{
   return _reactions_to_maximize;
}

inline const std::vector<std::string>& InputParser::getReactionIdsToMinimize() const
{
   return _reactions_to_minimize;
}

inline const std::vector<std::pair<std::string,double>>& InputParser::getReactionToFix() const
{
   return _reactions_to_fix;
}

inline const int& InputParser::getOptimizerTimeout() const
{
   return _optTimeout;
}

inline const int& InputParser::getOptimizerInternalMemory() const
{
   return _optMemory;
}

inline const int& InputParser::getDeletionsSetSize() const
{
   return _deletionSetSize;
}

inline void InputParser::printHelpDescription() const
{
      //_desc.print(std::cout);
      std::cout << _desc << std::endl;
}

#endif /* SRC_IO_INPUTPARSER_H_ */
