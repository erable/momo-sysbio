
#include "InputParser.h"

InputParser::InputParser() : _desc("Allowed options")
{
   //boost::program_options::options_description &dsc("Allowed options");
   //_desc = dsc;
   _desc.add_options()
           ("help,h", "produce help message")
           ("network-file,N", boost::program_options::value<std::string>(&_network_file), "input network file (MANDATORY).")
           ("bounds-file,B", boost::program_options::value<std::string>(&_bounds_file), "input file with the bounds for the input file (MANDATORY).")
           ("output-file,o", boost::program_options::value<std::string>(&_output_file)->default_value("solutions.txt"), "output file.")
           ("optimizer-timeout,t", boost::program_options::value<int>(&_optTimeout)->default_value(3600), "Timeout for the optimization procedure (in seconds).")
           ("maximize", boost::program_options::value<std::string>(), "Reactions to maximize")
           ("minimize", boost::program_options::value<std::string>(), "Reactions to minimize")
           ("reactions-to-fix", boost::program_options::value<std::string>(), "Reactions to fix. Format reactionID=flux[,reactionID=flux] (useful for the enumeration)")
           ("number-deletions",boost::program_options::value<int>(&_deletionSetSize)->default_value(0), "Consider deletions sets of informed size.")
           ("enumerate", "Enumerates deletions. In this case the objective functions are ignored.")
           ("deletions-candidates-file,D", boost::program_options::value<std::string>(&_deletions_candidates_file), "input file with the reactions that should be considered for deletion.")
           ("reference-flux-file,R", boost::program_options::value<std::string>(&_reference_flux_file), "input file with the fluxes that will be used as reference for the MOMA approach during enumeration.")
           ("optimizer-internal-memory,m", boost::program_options ::value<int>(&_optMemory)->default_value(2048), "Internal memory used by the optimizer before using disk space (in Mb).")
           ("robustness-reactions-file,T", boost::program_options::value<std::string>(&_robustness_reactions_file), "input file with the reactions that will be used to compose the robustness function to be optimized.")
           ("FVA-at-optimum,V", "Perform a fluv variance analisys at the optimal point. Works in the FBA mode only.");

}

InputParser::~InputParser()
{
   // TODO Auto-generated destructor stub
}

void InputParser::parseInput(int argc, char** argv )
{
   try
   {
      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, _desc), vm);
      boost::program_options::notify(vm);
      if (vm.count("help")) {
         this->_askedForHelp = true;
      }
      if (vm.count("enumerate")) {
         this->_enumerateSolutions = true;
      }
      if (vm.count("output-file"))
      {
         this->_output_file = vm["output-file"].as<std::string>();
      }
      if (vm.count("deletions-candidates-file"))
      {
         this->_deletions_candidates_file = vm["deletions-candidates-file"].as<std::string>();
      }
      if (vm.count("reference-flux-file"))
      {
         this->_reference_flux_file = vm["reference-flux-file"].as<std::string>();
      }
      if (vm.count("robustness-reactions-file"))
      {
         this->_robustness_reactions_file = vm["robustness-reactions-file"].as<std::string>();
      }
      if (vm.count("network-file"))
      {
         this->_network_file = vm["network-file"].as<std::string>();
      }
      if (vm.count("optimizer-timeout"))
      {
         this->_optTimeout = vm["optimizer-timeout"].as<int>();
      }
      if (vm.count("optimizer-internal-memory"))
      {
         this->_optMemory = vm["optimizer-internal-memory"].as<int>();
      }
      if (vm.count("maximize"))
      {
         std::string  reactions_ids = vm["maximize"].as<std::string>();
         boost::char_separator<char> sep(", ");
         boost::tokenizer<boost::char_separator<char>> tokens(reactions_ids, sep);
         for (const auto& t : tokens) {
            this->_reactions_to_maximize.push_back(t);
         }
      }
      if (vm.count("minimize"))
      {
         std::string  reactions_ids = vm["minimize"].as<std::string>();
         boost::char_separator<char> sep(", ");
         boost::tokenizer<boost::char_separator<char>> tokens(reactions_ids, sep);
         for (const auto& t : tokens) {
            this->_reactions_to_minimize.push_back(t);
         }
      }
      if (vm.count("reactions-to-fix"))
      {
         std::string  reactions_ids = vm["reactions-to-fix"].as<std::string>();
         boost::char_separator<char> sep1(", ");
         boost::char_separator<char> sep2("=");
         boost::tokenizer<boost::char_separator<char>> tokens(reactions_ids, sep1);
         for (const auto& t : tokens) {
            boost::tokenizer<boost::char_separator<char>> reaction_flux_tokents(t, sep2);
            boost::tokenizer<boost::char_separator<char>>::iterator rf_iterator = reaction_flux_tokents.begin();
            std::pair<std::string,double> reaction_flux;
            reaction_flux.first = *rf_iterator;
            ++rf_iterator;
            assert(rf_iterator!=reaction_flux_tokents.end());
            double f = atof((*rf_iterator).c_str());
            reaction_flux.second = f;
            this->_reactions_to_fix.push_back(reaction_flux);
         }
      }
      if (vm.count("enumerate-deletions"))
      {
         this->_deletionSetSize = vm["enumerate-deletions"].as<int>();
      }
      if (vm.count("FVA-at-optimum"))
      {
         this->_doFVA = true;
      }

   } catch ( const std::exception& e ) {
      std::cerr << e.what() << std::endl;
   }
}
