/*

 * main.cpp
 *
 *  Created on: 14 juil. 2016
 *      Author: randrade
 */

#include <iostream>
#include <list>
#include <cstring>
#include <string>
#include <unistd.h>
#include <set>

#include <boost/optional.hpp>

#include "hypergraph/Decomposition.h"
#include "interfaces/cplex/CplexFBAModeler.h"
#include "interfaces/scip/SCIPFBAModeler.h"
#include "io/read_bounds.h"
#include "io/read_writeFile.h"
#include "io/InputParser.h"
#include "basicTests.h"

// arg 1 : xml file
// arg2  : bn file
int main(int argc, char** argv )
{
   //Run tests
   //mainTests(argc,argv);
   //return 0;

   char directory[1024];
   if (getcwd(directory, sizeof(directory)) == NULL)
   {
      perror("getcwd() error");
      return -9;
   }
   InputParser parser;
   parser.parseInput(argc,argv);
   if(parser.didAskedForHelp())
   {
      parser.printHelpDescription();
      return 0;
   }
   HGraph  graph;
   if(parser.getNetworkFileName().size() > 0)
   {
      // Read the metabolic network
      cout << "Reading network " << parser.getNetworkFileName() << endl;
      readXML(parser.getNetworkFileName().c_str(), graph);
   }
   else
   {
      cerr << "No input SBML file."<< endl;
      parser.printHelpDescription();
      return -1;
   }

   // Re-index the graph so all ids will begin in 0 and be sequential.
   graph.reIndex();

   vector<Bounds> bounds;
   if(parser.getBoundsFileName().size() > 0)
   {
      bounds = readReactionBoundsFile(parser.getBoundsFileName().c_str(), graph);
   }
   else
   {
      cout << "No bounds file" << endl;
      return -2;
   }

   // Get the reference flux vector for the enumeration with MOMA.
   vector<double> referenceFlux;
   if(parser.getReferenceFluxFileName().size() > 0)
   {
      try{
         cout << "Reading reference flux file " << parser.getReferenceFluxFileName()  << "... ";
         readListOfFluxes(parser.getReferenceFluxFileName().c_str(),graph, referenceFlux);
         cout << "total of " << referenceFlux.size() << " fluxes read." << endl;
      }
      catch ( const std::invalid_argument& e)
      {
         cout << e.what() << endl;
         return -3;
      }
   }
   // Get deletions candidates to create the vector of reactions that should not be considered for deletion.
   vector<HEdge> deletion_candidates;
   if(parser.getDeletionsCandidateFileName().size() > 0)
   {
      try{
         cout << "Reading deletion candidates file " << parser.getDeletionsCandidateFileName() << "... ";
         readListOfReactions(parser.getDeletionsCandidateFileName().c_str(),graph, deletion_candidates);
         cout << "total of " << deletion_candidates.size() << " reactions read." << endl;
      }
      catch ( const std::invalid_argument& e)
      {
         cout << e.what() << endl;
         return -3;
      }
   }
   // Get the list of the reactions that will be part of the robustness function.
   vector<HEdge> robustness_reactions;
   if(parser.getRobustnessReactionsFileName().size() > 0)
   {
      try{
         cout << "Reading the file containing the reactions for the robustness function " << parser.getRobustnessReactionsFileName() << "... ";
         readListOfReactions(parser.getRobustnessReactionsFileName().c_str(),graph, robustness_reactions);
         cout << "total of " << robustness_reactions.size() << " reactions read." << endl;
      }
      catch ( const std::invalid_argument& e)
      {
         cout << e.what() << endl;
         return -3;
      }
   }
   vector<std::string> reactionsToBeIgnoredInTheDeletions;
   for (auto edge_it = graph.getEdges().begin();edge_it != graph.getEdges().end(); edge_it++)
   {
      HEdge edge = edge_it->second;
      if (std::find(deletion_candidates.begin(), deletion_candidates.end(), edge) == deletion_candidates.end())
      {
         reactionsToBeIgnoredInTheDeletions.push_back(edge.getName());
      }
   }
   cout << "A total of " << reactionsToBeIgnoredInTheDeletions.size() << " reactions from the network will not be considered as deletion candidates." << endl;


   // Get reactions to be maximized
   vector<HEdge> reactions_maximize;
   if(parser.getReactionIdsToMaximize().size() > 0)
   {
      for (const string& it : parser.getReactionIdsToMaximize())
      {
         boost::optional<HEdge> oe = graph.getEdge(it.c_str());
         if (oe.is_initialized())
         {
            reactions_maximize.push_back(oe.get());
         }
         else
         {
            cerr << "Could not find reaction " << it << " to maximize." << endl;
            return -3;
         }
      }
   }

   // Get reactions to be minimized
   vector<HEdge> reactions_minimize;
   if(parser.getReactionIdsToMinimize().size() > 0)
   {
      for (const string& it : parser.getReactionIdsToMinimize())
      {
         boost::optional<HEdge> oe = graph.getEdge(it.c_str());
         if (oe.is_initialized())
         {
            reactions_minimize.push_back(oe.get());
         }
         else
         {
            cerr << "Could not find reaction " << it << " to minimize." << endl;
            return -3;
         }
      }
   }

   std::vector<std::pair<std::string,double>> reactionIDsToFix= parser.getReactionToFix();
   std::vector<std::pair<HEdge,double>> reactionsToFix;
   for (std::pair<std::string,double> p : reactionIDsToFix)
   {
      string reactionID = p.first;
      boost::optional<HEdge> oe = graph.getEdge(reactionID);
      if (oe.is_initialized())
      {
         HEdge e = oe.get();
         std::pair<HEdge,double> np(e,p.second);
         reactionsToFix.push_back(np);
         cout << "Fixing reaction " << reactionID << " to " << p.second << endl;
      }
      else
      {
         cerr << "Could not find reaction " << p.first << " to fix." << endl;
         return -3;
      }
   }

   std::string outputFilename;
   outputFilename = parser.getOutputFileName();

   SCIPFBAModeler solverSCIP(graph);

   // We have to launch the solver appropriately so let's
   // identify the mode of operation according to the input parameters

   // If we are not enumerating and we have more that one objective, it is poly-objective case
   if (!parser.shouldEnumerateSolutions() && reactions_maximize.size() + reactions_minimize.size() > 1)
   {
      // We are in the multi-objective case
      cout << "Starting the multiobjective case." << endl;
      cout << "Reactions to maximise: ";
      for (std::vector<HEdge>::const_iterator i = reactions_maximize.begin(); i != reactions_maximize.end(); ++i)
          cout << i->getName() << ", ";
      cout << endl;
      cout << "Reactions to minimise: ";
      for (std::vector<HEdge>::const_iterator i = reactions_minimize.begin(); i != reactions_minimize.end(); ++i)
          cout << i->getName() << ", ";
      cout << endl;

      if (robustness_reactions.size() > 0)
      {
         //Here we are at the case where we want to maximise also the robustness function (EXPERIMENTAL)
         //Solve the problem
         solverSCIP.doPolyRobustnessObjectiveDeleting(parser.getDeletionsSetSize(),reactions_maximize, reactions_minimize, robustness_reactions, bounds, reactionsToFix, reactionsToBeIgnoredInTheDeletions, outputFilename);
      }
      else // Here we are at the regular multi-objective case
      {
         //Solve the problem
         solverSCIP.doPolyObjectiveDeleting(parser.getDeletionsSetSize(),reactions_maximize, reactions_minimize,bounds,reactionsToFix,reactionsToBeIgnoredInTheDeletions, outputFilename);
      }
   }
   // If we are not enumerating and we have one objective, and we have the no-deletions flag, we have our FBA case or the FVA at optimum case
   else if (!parser.shouldEnumerateSolutions() && reactions_maximize.size() + reactions_minimize.size() <= 1 && parser.getDeletionsSetSize() == 0)
   {
      if (reactions_maximize.size() < 1)
      {
         cerr << "[ERROR] The reaction should be maximized, not minimized." << endl;
      }
      if (parser.doFVA())
      {
         HEdge targetReaction = reactions_maximize[0];
         cout << "Starting the FVA case." << endl;
         cout << "Reaction to maximise: " << targetReaction.getName() << endl;
         vector<double> solutionFluxVector;
         solutionFluxVector = solverSCIP.doFBAFixingProductions(reactions_maximize,bounds,reactionsToFix);
         int targetReactionID = targetReaction.getId();
         double optimalSolution = solutionFluxVector[targetReactionID];
         cout << "Optimal production of the target: " << optimalSolution << endl;
         vector<pair<double,double>> fvaSolution;
         //Solve the problem
         fvaSolution = solverSCIP.doOptimalFVA(optimalSolution, reactions_maximize, bounds,reactionsToFix);
         if (fvaSolution.size() > 0)
         {
            for(unsigned int i=0; i<fvaSolution.size();++i)
            {
               cout << graph.getEdge(i).getName() << "=" << fvaSolution[i].first << "\t" << fvaSolution[i].second << "\t" << fvaSolution[i].second - fvaSolution[i].first << endl;
            }
         }
      }
      else
      {
         cout << "Starting the single objective case." << endl;
         cout << "Reaction to maximise: " << reactions_maximize[0].getName() << endl;
         vector<double> solutionFluxVector;
         //Solve the problem
         solutionFluxVector = solverSCIP.doFBAFixingProductions(reactions_maximize,bounds,reactionsToFix);
         if (outputFilename.length() > 0)
         {
            cout << "Output solution to " << outputFilename << endl;
            writeOutput(outputFilename.c_str(),graph,solutionFluxVector);
         }
         else
         {
            printOutput(graph,solutionFluxVector);
         }
      }
   }

   // If we are enumerating deletions and we have no objective, it is our enumeration case
   else if (parser.shouldEnumerateSolutions() && parser.getDeletionsSetSize() > 0 && reactions_maximize.size() + reactions_minimize.size() == 0)
   {
      cout << "Enumeration mode" << endl;
      vector<tuple<vector<HEdge>,vector<double>>>  solutions;
      //Solve the problem
      solutions = solverSCIP.doDeletionEnumerationWithMOMA(parser.getDeletionsSetSize(), bounds, reactionsToFix, reactionsToBeIgnoredInTheDeletions,referenceFlux);
      if (outputFilename.length() > 0)
      {
         cout << "Output solution to " << outputFilename << endl;
         writeOutput(outputFilename.c_str(),graph,solutions);
      }
      else
      {
         printOutput(graph,solutions);
      }
   }
   else {
      cout << "Could not identify the execution mode." << endl;
      parser.printHelpDescription();
   }

   return 0;
}


