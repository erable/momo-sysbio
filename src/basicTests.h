/*
 * basicTests.c
 *
 *  Created on: 22 juin 2017
 *      Author: randrade
 */

#include <iostream>
#include <list>
#include <cstring>
#include <string>
#include <unistd.h>
#include <set>

#include <boost/optional.hpp>

#include "hypergraph/Decomposition.h"
#include "interfaces/cplex/CplexFBAModeler.h"
#include "interfaces/scip/SCIPFBAModeler.h"
#include "io/read_bounds.h"
#include "io/read_writeFile.h"
#include "io/InputParser.h"

void printFluxes(std::string filename, HGraph  graph, vector<double> fluxes);

// arg 1 : xml file
// arg2  : bn file
int mainTests(int argc, char** argv )
{
   char directory[1024];
   if (getcwd(directory, sizeof(directory)) == NULL)
   {
      perror("getcwd() error");
      return -9;
   }
   InputParser parser;
   parser.parseInput(argc,argv);
   if(parser.didAskedForHelp())
   {
      parser.printHelpDescription();
      return 0;
   }
   HGraph  graph;
   if(parser.getNetworkFileName().size() > 0)
   {
      cout << "Reading " << parser.getNetworkFileName() << endl;
      readXML(parser.getNetworkFileName().c_str(), graph);
   }
   else
   {
      cerr << "No input SBML file."<< endl;
      parser.printHelpDescription();
      return -1;
   }

   // Re-index the graph so all ids will begin in 0 and be sequential.
   graph.reIndex();
   //graph.display();

   vector<Bounds> bounds;
   if(parser.getBoundsFileName().size() > 0)
   {
      bounds = readReactionBoundsFile(parser.getBoundsFileName().c_str(), graph);
   }
   else
   {
      cout << "No bounds file" << endl;
      return -2;
   }

   vector<HEdge> deletion_candidates;
   if(parser.getDeletionsCandidateFileName().size() > 0)
   {
      readListOfReactions(parser.getDeletionsCandidateFileName().c_str(),graph, deletion_candidates);
   }

   vector<HEdge> reactions_maximize;
   if(parser.getReactionIdsToMaximize().size() > 0)
   {
      for (const string& it : parser.getReactionIdsToMaximize())
      {
         boost::optional<HEdge> oe = graph.getEdge(it.c_str());
         if (oe.is_initialized())
         {
            reactions_maximize.push_back(oe.get());
         }
         else
         {
            cerr << "Could not find reaction " << it << " to maximize." << endl;
            return -3;
         }
      }
   }

   vector<HEdge> reactions_minimize;
   if(parser.getReactionIdsToMinimize().size() > 0)
   {
      for (const string& it : parser.getReactionIdsToMinimize())
      {
         boost::optional<HEdge> oe = graph.getEdge(it.c_str());
         if (oe.is_initialized())
         {
            reactions_minimize.push_back(oe.get());
         }
         else
         {
            cerr << "Could not find reaction " << it << " to minimize." << endl;
            return -3;
         }
      }
   }

   //SCIPFBAModeler solverSCIP(graph);

   //DEBUG
   /*
   for (auto edge_it = graph.getEdges().begin();edge_it != graph.getEdges().end(); ++edge_it)
   {
      int edge_id = edge_it->first;
      HEdge edge = edge_it->second;
      bool hasNADP = false;
      for (auto node_it = edge.getHeadNodes().begin(); node_it != edge.getHeadNodes().end(); ++node_it)
      {
         HNode n = graph.getNode(*node_it);
         // s_1401 = Pyruvate
         // s_1205 = NADP+
         if (n.getName().compare("s_1205") == 0)
         {
            hasNADP = true;
            cout << "DEBUG: " << edge.getId() << "\t" << edge.getName() << endl;
         }

      }
      for (auto node_it = edge.getTailNodes().begin(); node_it != edge.getTailNodes().end(); ++node_it)
      {
         HNode n = graph.getNode(*node_it);
         if (n.getName().compare("s_1401") == 0 && hasNADP)
         {
            cout << "DEBUG:\t Has Pyruvate" << endl;
         }
      }
   }
    */
   //DEBUG

   // OBS:
   // r_1992 - Oxigen exchange
   // r_1761 - Ethanol exchange
   // r_1808 - Glycerol exchange
   // r_2110 - Biomass
   // --------------------------
   // r_0961 - Pyruvate dehydrogenase
   // r_0226 - ATP synthase
   // r_0164 - Alcohol dehydrogenase (glycerol, NADP)
   // r_0489 - Glycerol-3-phosphatase
   // s_0765 - Glycerol (cytoplasm)

   // Yeast
   HEdge biomass = *graph.getEdge("r_2110");
   HEdge ethanol = *graph.getEdge("r_1761");
   //HEdge glycerol = *graph.getEdge("r_1808");



   // E. Coli
   //HEdge biomass = *graph.getEdge("R_Biomass_Ecoli_core_w_GAM");
   //HEdge ethanol = *graph.getEdge("R_EX_etoh_e");

   // Print the stoichiommetry matrix
   /*
   for (int i = 0; i< graph.getNbNodes();++i)
   {
      HNode n = graph.getNode(i);
      vector<double> lineToPrint(graph.getNbEdges());
      for (auto edge_it = n.getOutEdges().begin();edge_it != n.getOutEdges().end(); ++edge_it)
      {
         HEdge e = graph.getEdge(*edge_it);
         const list<int> sources = e.getTailNodes();
         const list<double> sourcesStoich = e.getTailStoichiometry();
         list<int>::const_iterator it1;
         list<double>::const_iterator it2;
         for(it1=sources.begin(),it2=sourcesStoich.begin(); (it1 != sources.end()) && (it2 != sourcesStoich.end()); ++it1,++it2 )
         {
            int nodeId = *it1;
            double nodeStoich = *it2;
            if (n.getId() == nodeId)
            {
               lineToPrint[e.getId()] = -nodeStoich;
            }
         }
      }
      for (auto edge_it = n.getInEdges().begin();edge_it != n.getInEdges().end(); ++edge_it)
      {
         HEdge e = graph.getEdge(*edge_it);
         list<int> products = e.getHeadNodes();
         list<double> productsStoich = e.getHeadStoichiometry();
         list<int>::const_iterator it1;
         list<double>::const_iterator it2;
         for(it1=products.begin(),it2=productsStoich.begin(); (it1 != products.end()) && (it2 != productsStoich.end()); ++it1,++it2 )
         {
            int nodeId = *it1;
            double nodeStoich = *it2;
            if (n.getId() == nodeId)
            {
               lineToPrint[e.getId()] = nodeStoich;
            }
         }
      }
      for (int i = 0; i < graph.getNbEdges(); ++i)
      {
         cout << lineToPrint[i] << " ";
      }
      cout << endl;
   }
    */
   CplexFBAModeler solver(graph);
   /*HEdge r0006 = *graph.getEdge("r_0006");
   for (Bounds b : bounds)
   {
      if (b.id == r0006.getId())
      {
         cout << "Bounds for r0006: " << b.lb << " " << b.ub << endl;
      }

   }*/

   vector<double> referenceFluxVector;
   //referenceFluxVector = solver.doFBA({biomass},bounds);
   //writeOutput("/tmp/FBA_cplex.out",graph,referenceFluxVector);
   //readListOfFluxes("FBA_cplex.out",graph,referenceFluxVector);
   readListOfFluxes("fba_reference.txt",graph,referenceFluxVector);

   //vector<double> referenceFluxVector;
   //referenceFluxVector = solverSCIP.doFBA({biomass},bounds);
   //cout << " Solution found for optimising biomass (Alcohol: " << referenceFluxVector[1760] << ", Glycerol: " << referenceFluxVector[1807] << ", Biomass: " << referenceFluxVector[2108] << ")" <<   endl;
  /*
   for(int i=0; i<graph.getNbEdges(); ++i)
   {
      //cout << graph.getEdge(i).getName() << " : " << referenceFluxVector[i] << endl;
   }
   */
   //vector<std::string> reactionsToBeIgnoredInTheDeletions = {"r_1542","r_1545","r_1546","r_1547","r_1548","r_1549","r_1550","r_1551","r_1552","r_1553","r_1554","r_1563","r_1564","r_1565","r_1566","r_1571","r_1572","r_1577","r_1580","r_1581","r_1586","r_1589","r_1598","r_1604","r_1621","r_1625","r_1627","r_1629","r_1630","r_1631","r_1634","r_1639","r_1641","r_1643","r_1648","r_1649","r_1650","r_1651","r_1654","r_1663","r_1671","r_1672","r_1683","r_1687","r_1702","r_1705","r_1706","r_1709","r_1710","r_1711","r_1712","r_1714","r_1715","r_1716","r_1718","r_1727","r_1730","r_1749","r_1753","r_1757","r_1761","r_1764","r_1765","r_1767","r_1788","r_1791","r_1792","r_1793","r_1798","r_1799","r_1800","r_1806","r_1807","r_1808","r_1810","r_1814","r_1815","r_1818","r_1820","r_1831","r_1833","r_1840","r_1842","r_1846","r_1860","r_1861","r_1864","r_1865","r_1866","r_1869","r_1872","r_1874","r_1877","r_1878","r_1879","r_1880","r_1882","r_1885","r_1888","r_1890","r_1892","r_1895","r_1896","r_1898","r_1899","r_1901","r_1902","r_1903","r_1905","r_1908","r_1910","r_1911","r_1912","r_1913","r_1914","r_1915","r_1916","r_1917","r_1930","r_1932","r_1946","r_1948","r_1950","r_1951","r_1961","r_1966","r_1967","r_1983","r_1984","r_1986","r_1988","r_1991","r_1992","r_1993","r_1998","r_1999","r_2000","r_2004","r_2008","r_2018","r_2019","r_2023","r_2027","r_2032","r_2037","r_2042","r_2043","r_2045","r_2048","r_2050","r_2051","r_2054","r_2055","r_2057","r_2059","r_2060","r_2061","r_2065","r_2066","r_2067","r_2072","r_2075","r_2082","r_2089","r_2090","r_2091","r_2099","r_2101","r_2103","r_2105","r_2109","r_2110"};
   /*vector<std::string> reactionsToBeIgnoredInTheDeletions;
   for(int i=0; i<graph.getNbEdges(); ++i)
   {
      if (referenceFluxVector[i] == 0)
      {
         reactionsToBeIgnoredInTheDeletions.push_back(graph.getEdge(i).getName());
      }
   }*/
   //vector<HEdge> reactionsToMaximize = {biomass,ethanol};
   //vector<HEdge> reactionsToMinimize;
   //std::vector<std::pair<HEdge,double>> reactionsToFix;

   //solverSCIP.doPolyObjectiveDeleting(1,reactionsToMaximize,reactionsToMinimize, bounds, reactionsToFix, reactionsToBeIgnoredInTheDeletions, "output2.txt");

   /*vector<HEdge> reactionsToMaximize = {biomass, ethanol};
   vector<HEdge> reactionsToMinimize;
   solverSCIP.doPolyObjectiveDeleting(reactionsToMaximize,reactionsToMinimize, bounds, reactionsToBeIgnoredInTheDeletions, "output.txt");
   return 0;*/
   //referenceFluxVector = solverSCIP.doFBA({ethanol},bounds);
   //cout << " Solution found for optimising ethanol (Alcohol: " << referenceFluxVector[1760] << ", Glycerol: " << referenceFluxVector[1807] << ", Biomass: " << referenceFluxVector[2108] << ")" <<   endl;
   //cout << " Solution found (Alcohol: " << referenceFluxVector[23] << ", Biomass: " << referenceFluxVector[12] << ")" <<   endl;
   //for(int i=0; i<graph.getNbEdges(); ++i)
   //{
   //cout << graph.getEdge(i).getName() << " : " << referenceFluxVector[i] << endl;
   //}

   //vector<pair<HEdge,double>> reactionsToFix;
   //reactionsToFix.push_back(pair<HEdge,double>(ethanol,17.4));
   //reactionsToFix.push_back(pair<HEdge,double>(biomass,0.186));
   //solverSCIP.doSingleDeletionFixingProductionExcludingZerosFromReference({biomass},bounds,referenceFluxVector,reactionsToFix);
   //vector<double> sol;
   //sol = solverSCIP.doFBAFixingProductions({biomass},bounds,reactionsToFix);
   //cout << " Solution found (Alcohol: " << sol[1760] << ", Glycerol: " << sol[1807] << ", Biomass: " << sol[2108] << ")" <<   endl;

   /*
   std::string filename;
   vector<double> sol;
   vector<pair<HEdge,double>> reactionsToFix;
   reactionsToFix.push_back(pair<HEdge,double>(ethanol,19.8));
   sol = solverSCIP.doSingleDeletionFixingProductionExcludingZerosFromReference({biomass},bounds,referenceFluxVector,reactionsToFix);
   cout << " Solution found (Alcohol: " << sol[1760] << ", Glycerol: " << sol[1807] << ", Biomass: " << sol[2108] << ")" <<   endl;
   filename = "point_" + std::to_string(sol[2108]) +"b_" + std::to_string(sol[1760]) + "e_" + std::to_string(sol[1807] ) + "g";
   printFluxes(filename,graph,sol);
   reactionsToFix.clear();
   reactionsToFix.push_back(pair<HEdge,double>(ethanol,17.5));
   sol = solverSCIP.doSingleDeletionFixingProductionExcludingZerosFromReference({biomass},bounds,referenceFluxVector,reactionsToFix);
   cout << " Solution found (Alcohol: " << sol[1760] << ", Glycerol: " << sol[1807] << ", Biomass: " << sol[2108] << ")" <<   endl;
   filename = "point_" + std::to_string(sol[2108]) +"b_" + std::to_string(sol[1760]) + "e_" + std::to_string(sol[1807] ) + "g";
   printFluxes(filename,graph,sol);
   reactionsToFix.clear();
   reactionsToFix.push_back(pair<HEdge,double>(ethanol,17.4));
   sol = solverSCIP.doSingleDeletionFixingProductionExcludingZerosFromReference({biomass},bounds,referenceFluxVector,reactionsToFix);
   cout << " Solution found (Alcohol: " << sol[1760] << ", Glycerol: " << sol[1807] << ", Biomass: " << sol[2108] << ")" <<   endl;
   filename = "point_" + std::to_string(sol[2108]) +"b_" + std::to_string(sol[1760]) + "e_" + std::to_string(sol[1807] ) + "g";
   printFluxes(filename,graph,sol);
   reactionsToFix.clear();
   reactionsToFix.push_back(pair<HEdge,double>(ethanol,17.2));
   sol = solverSCIP.doSingleDeletionFixingProductionExcludingZerosFromReference({biomass},bounds,referenceFluxVector,reactionsToFix);
   cout << " Solution found (Alcohol: " << sol[1760] << ", Glycerol: " << sol[1807] << ", Biomass: " << sol[2108] << ")" <<   endl;
   filename = "point_" + std::to_string(sol[2108]) +"b_" + std::to_string(sol[1760]) + "e_" + std::to_string(sol[1807] ) + "g";
   printFluxes(filename,graph,sol);
   reactionsToFix.clear();
   reactionsToFix.push_back(pair<HEdge,double>(ethanol,15.3));
   sol = solverSCIP.doSingleDeletionFixingProductionExcludingZerosFromReference({biomass},bounds,referenceFluxVector,reactionsToFix);
   cout << " Solution found (Alcohol: " << sol[1760] << ", Glycerol: " << sol[1807] << ", Biomass: " << sol[2108] << ")" <<   endl;
   filename = "point_" + std::to_string(sol[2108]) +"b_" + std::to_string(sol[1760]) + "e_" + std::to_string(sol[1807] ) + "g";
   printFluxes(filename,graph,sol);
   reactionsToFix.clear();
   reactionsToFix.push_back(pair<HEdge,double>(ethanol,11.8));
   sol = solverSCIP.doSingleDeletionFixingProductionExcludingZerosFromReference({biomass},bounds,referenceFluxVector,reactionsToFix);
   cout << " Solution found (Alcohol: " << sol[1760] << ", Glycerol: " << sol[1807] << ", Biomass: " << sol[2108] << ")" <<   endl;
   filename = "point_" + std::to_string(sol[2108]) +"b_" + std::to_string(sol[1760]) + "e_" + std::to_string(sol[1807] ) + "g";
   printFluxes(filename,graph,sol);
   reactionsToFix.clear();
   reactionsToFix.push_back(pair<HEdge,double>(ethanol,8.83));
   sol = solverSCIP.doSingleDeletionFixingProductionExcludingZerosFromReference({biomass},bounds,referenceFluxVector,reactionsToFix);
   cout << " Solution found (Alcohol: " << sol[1760] << ", Glycerol: " << sol[1807] << ", Biomass: " << sol[2108] << ")" <<   endl;
   filename = "point_" + std::to_string(sol[2108]) +"b_" + std::to_string(sol[1760]) + "e_" + std::to_string(sol[1807] ) + "g";
   printFluxes(filename,graph,sol);
   reactionsToFix.clear();
   cout << "Done!" << endl;
   return 0;
   */
   // Score all reactions
   //vector<HEdge> allEdges;
   //for (auto it: graph.getEdges()){
   //   HEdge e = it.second;
   //   allEdges.push_back(e);
   //}

   //HEdge ethanol = *graph.getEdge("R_ALCD2ir");
   /*
   vector <double> fbaSolution = solver.doFBA({ethanol},bounds);
   for(unsigned int i=0; i<fbaSolution.size();++i)
   {
      double f = fbaSolution[i];
      if (f <= -1e-6 || f >= 1e-6)
      {
         cout << graph.getEdge(i).getDescription() << "(" << graph.getEdge(i).getName() <<")" << "\t" << f << endl;
      }
   }
    */
   //vector<double>  scores = solver.doMultiFBADeletingReactions({biomass},bounds,allEdges);
   vector<double> scores;

   //--- My solutions
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.3,3.29);
   //-- No solution: solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0,18.7);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.21,14.2);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.0457,18.1);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.0495,18);

   //--- Mahdi solutions
   //--No solution:
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0,18.7);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.0541,18); // 0.0541,18
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.210999,14.1); // 0.217,14.1
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.298826,3.45); // 0.305,3.45

   //-- My new solutions Biobjective
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0,19.8); // 0, 19.8
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.169634,17.5); //0.172,17.5);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.176796,17.4); //0.179,17.4);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.186689,17.2); //0.186,17.2);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.267023,15.3); //0.266,15.3);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.411879,11.8); //0.412,11.8);
   //solver.doFBAFixingBiomassAndEthanol({biomass},bounds,0.435534,8.83); //0.436,8.83);

   //-- My new solutions Triobjective
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0.169634,17.5,0); //0.172,17.5
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0.176796,17.4,0); //0.179,17.4
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0.186689,17.2,0); //0.186,17.2
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0.267023,15.3,0); //0.266,15.3)
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0.411879,11.8,0); //0.412,11.8
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0.431831,9.3,1.92453);  //0.424,9.3
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0.431368,7.28,2.30896); //0.429,7.28
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0.435508,8.83,2.29001); //0.436,8.83,2.29251
   //solver.doFBAFixingBiomassEthanolAndGlycerol({biomass},bounds,0,19.8,0); //0,19.8

   // MOMA for the bi-objective
   solver.doMOMAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0,19.8,scores); //0,19.8 -- not runned since produces 0 biomass);
   solver.doMOMAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.169633,17.5,scores); //0.172,17.5);
   solver.doMOMAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.176796,17.4,scores); //0.179,17.4);
   solver.doMOMAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.186689,17.2,scores); //0.186,17.2);
   solver.doMOMAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.267022,15.3,scores); //0.266,15.3);
   solver.doMOMAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.411879,11.8,scores); //0.412,11.8);
   solver.doMOMAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.435533,8.83,scores); //0.436,8.83);

   // MOMA for tri-objective
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.169633,17.5,0,scores); //0.172,17.5
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.176796,17.4,0,scores); //0.179,17.4
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.186689,17.2,0,scores); //0.186,17.2
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.26702,15.3,0,scores); //0.266,15.3)
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.411879,11.8,0,scores); //0.412,11.8
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.42416,9.3,0,scores);  //0.424,9.3
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.42855,7.28,0,scores); //0.429,7.28
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.43550,8.83,2.29251,scores); //0.436,8.83,2.29251
   //solver.doMOMAFixingBiomassAndEthanolGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0,19.8,0,scores); //0,19.8

   //-- Only remove zeros Biobjective
   //solver.doFBAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.169633,17.5,scores); //0.172,17.5);
   //solver.doFBAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.176796,17.4,scores); //0.179,17.4);
   //solver.doFBAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.186689,17.2,scores); //0.186,17.2);
   //solver.doFBAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.267023,15.3,scores); //0.266,15.3);
   //solver.doFBAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.411879,11.8,scores); //0.412,11.8);
   //solver.doFBAFixingBiomassAndEthanolExcludingZerosFromReference({biomass},bounds,referenceFluxVector,0.435534,8.83,scores); //0.436,8.83);

   //-- Only remove zeros Triobjective
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0.169633,17.5,0,scores); //0.172,17.5
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0.176796,17.4,0,scores); //0.179,17.4
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0.186689,17.2,0,scores); //0.186,17.2
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0.26702,15.3,0,scores); //0.266,15.3)
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0.411879,11.8,0,scores); //0.412,11.8
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0.42416,9.3,0,scores);  //0.424,9.3
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0.42855,7.28,0,scores); //0.429,7.28
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0.43550,8.83,2.29251,scores); //0.436,8.83,2.29251
   //solver.doFBAFixingBiomassEthanolAndGlycerolExcludingZerosFromReference({biomass},bounds,referenceFluxVector, 0,19.8,0,scores); //0,19.8

   //cout << "End of enumerations." << endl;


   // Score some reactions
   /*vector <HEdge> edges;
   readListOfReactions("/tmp/reactions.txt",graph,edges);
   vector<double>  scores = solver.doMultiFBADeletingReactions({biomass},bounds,edges);
   for (int i=0;i<edges.size();++i)
   {
      cout  << edges[i].getName() << " ; " << edges[i].getDescription() << " ; " << scores[i] << endl;
   }*/
   /*if (solutions.size() > 0)
   {
      cout << "Solutions found:" << endl;
      cout << "----------------" << endl;
      for (Solution s : solutions)
      {
         s.display();
      }
   }
   else
   {
      cout << "Found no solutions." << endl;

   }*/

   /*vector<std::string> reactionsToFix;
   for(int i=0; i < referenceFluxVector.size();++i)
   {
      if (referenceFluxVector[i]==0)
      {
         reactionsToFix.push_back(graph.getEdge(i).getName());
      }
   }
   solver.generateMPSFormat("MULTIOBJ",bounds,reactionsToFix);*/


   return 0;
}


void printFluxes(std::string filename, HGraph  graph, vector<double> fluxes)
{
   ofstream output;
   output.open (filename);
   for(int i=0; i<graph.getNbEdges(); ++i)
   {
     output << graph.getEdge(i).getDescription() << " (" << graph.getEdge(i).getName() << ") "<< " : " << fluxes[i] << endl;
   }
   output.close();

}


