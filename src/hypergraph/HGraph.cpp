/*
 * HGraph.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: ajulien
 */


// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include "HGraph.h"

#include <algorithm>
#include <assert.h>
#include <map>
#include <cassert>
#include <stdexcept>
#include <set>


// ===========================================================================
//                             Include Project Files
// ===========================================================================


// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================


using namespace std;




//############################################################################
//                                                                           #
//                               Class HGraph                                #
//                                                                           #
//############################################################################

// ===========================================================================
//                               Static attributes
// ===========================================================================

// ===========================================================================
//                                  Constructors
// ===========================================================================
// ===========================================================================
HGraph::HGraph (void) {
   this->_nbNodes = 0;
   this->_nbEdges = 0;
}

HGraph::HGraph( const HGraph &model )
{
   _nodes = model._nodes;
   _edges = model._edges;
   _blackNodes = model._blackNodes;
   _nbEdges = model._nbEdges;
   _nbNodes = model._nbNodes;
   namesNodes = model.namesNodes;
}
// ===========================================================================
//                                  Destructors
// ===========================================================================
HGraph::~HGraph( void )
{
}
// ===========================================================================
//                                   Operators
// ===========================================================================

// ===========================================================================
//                                 Public Methods
// ===========================================================================
void HGraph::addNode(  HNode  & node )
{
   _nbNodes ++;

   _nodes.insert(pair<int,HNode>(_nbNodes, node));

   namesNodes.insert( pair<string,int> (node.getName(), _nbNodes));
   if (node.isBlack())
   {
      _blackNodes.push_front(_nbNodes);
   }
}

void HGraph::addNode( string name )
{
   HNode myNode ( name, _nbNodes + 1 );
   addNode( myNode);
}

void HGraph::addNode( string name, bool isBoundary )
{
   HNode myNode ( name, _nbNodes + 1 );
   myNode.setBoundary(isBoundary);
   addNode( myNode);
}


void HGraph::display (void) const
{
   cout << "====================================="<< endl;
   cout << "HyperGraph with  " << _nbNodes << " nodes and nb edges: " << _nbEdges << endl;
   cout << "====================================="<< endl;
   cout << "black node\t";
   for(const int & blackid: _blackNodes)
   {
      cout << blackid << " (";
      cout << getNode(blackid).getLBConcentrationValue() << ", ";
      cout << getNode(blackid).getUBConcentrationValue() << ")\t";
   }
   cout << "\n";
   for (auto it = _nodes.begin(); it != _nodes.end(); it++)
   {
      cout << "node " << it->first << " " << (it->second).getName() << "\t " << (it->second).getOutDegree() << " " << (it->second).getInDegree() << " " << (it->second).isBlack() << endl;
   }
   for (auto it = _edges.begin(); it != _edges.end(); it++)
   {
      cout << "edges " << it->first <<  " ";;
      (it->second).display() ;
      cout <<  endl;
   }
   cout << "====================================="<< endl;
}



//! Adding an edge
/*!
 * \brief Add an edge to the graph
 * \param nameEdge: label of the edges
 * \param substrates: name of the substrate
 * \param products: name of the products
 * WIll modify the object to add the edge.
 * Update also the nodes out-list
 */
void HGraph::addEdge( string nameEdge, string description, list<string> substrates, list<double> substratesStoich, list<string> products,  list<double> productsStoich)
{
   assert(substrates.size() == substratesStoich.size());
   assert(products.size() == productsStoich.size());
   this->_nbEdges++;  // we are creating a new edge
   HEdge myEdge ( nameEdge, description, _nbEdges );
   // First finding InNodes
   list<int> substratesN;
   list<int> productsN;
   // ========== filling tail and head of the edges
   // tail
   list<string>::iterator itCompound;
   list<double>::iterator itStoich;
   for (itCompound = substrates.begin(), itStoich = substratesStoich.begin(); itCompound != substrates.end() && itStoich != substratesStoich.end(); ++itCompound, ++itStoich )
   {
      auto itNode = namesNodes.find (*itCompound) ;
      double stoich = *itStoich;
      if ( itNode != namesNodes.end() )
      {
         myEdge.addTailNode (itNode->second,stoich);
         _nodes.at(itNode->second).addOutEdge( _nbEdges );
      }
      else
      {
         cerr << "compound:" << (*itCompound) << " unknown: WAS IGNORED";
      }
   }// end Substrates
   for (itCompound = products.begin(),itStoich = productsStoich.begin(); itCompound != products.end() && itStoich != productsStoich.end(); ++itCompound, ++itStoich )
   {
      auto itNode = namesNodes.find( *itCompound ) ;
      double stoich = *itStoich;
      if ( itNode != namesNodes.end() )
      {
         myEdge.addHeadNode (itNode->second,stoich)   ;
         _nodes.at(itNode->second).addInEdge( _nbEdges);
      }
      else
      {
         cerr << "compound " << (*itCompound) << " unknown: WAS IGNORED";
      }
   }//end products
   _edges.insert( pair<int, HEdge> (_nbEdges, myEdge) );
}

//! removeEdge:
/*!
 *\param id : id of the edge to remove
 *Remove the edges from the edges map.
 *Update nodes adjacency
 */
void HGraph::removeEdge (const int & idE)
{

   HEdge toto = _edges.at(idE);
   if(toto.getTailSize() > 0)
   {
      list<int> tailNodes = _edges.at(idE).getTailNodes();
      for(auto itH = tailNodes.begin(); itH != tailNodes.end();)
      {
         _nodes.at(*itH).removeOutEdge(idE);
         // if last edges connecting this node?
         ++itH;
      }}
   // From all head nodes remove idE:
   if(toto.getHeadSize()> 0 )
   {
      list<int> headNodes = _edges.at(idE).getHeadNodes();
      for(auto itH = headNodes.begin(); itH != headNodes.end(); )
      {
         _nodes.at(*itH).removeInEdge(idE);
         ++itH;
      }
   }

   _edges.erase(idE);
   --_nbEdges;
}



//! removeNode:
/*!
 *\param id : id of the node to remove
 *Remove the node from the node maps (namesnodes and _nodes )  and update edges
 */
void HGraph::removeNode(const int & id)
{
   HNode & tmpN = _nodes.at(id);
   namesNodes.erase(tmpN.getName());
   if (tmpN.isBlack())
   {
      _blackNodes.remove(id);
   }
   // updating adjacency (edges connections)
   if (tmpN.getInDegree() > 0)
   {
      list <int> inEdges = tmpN.getInEdges();

      for (auto itE = inEdges.begin(); itE != inEdges.end();)
      {

         HEdge & tmpE = _edges.at(*itE);
         tmpE.removeHeadNode(id);
         ++itE;
         if( tmpE.getHeadSize() == 0)
         {
            removeEdge(tmpE.getId());
         }
      }
   }
   if (tmpN.getOutDegree() > 0)
   {
      list <int> outEdges = tmpN.getOutEdges();
      for (auto itE = outEdges.begin(); itE != outEdges.end();)
      {
         HEdge & tmpE = _edges.at(*itE);
         tmpE.removeTailNode(id);
         ++itE;
         if( tmpE.getTailSize() == 0)
         {
            removeEdge(tmpE.getId());
         }
      }
   }
   _nodes.erase(id);
   --_nbNodes;
}

void HGraph::reIndex (void)
{
   reIndexNodes();
   reIndexEdges();
}

//! Re-index all the nodes to go from 0-N for their id
void HGraph::reIndexNodes (void)
{
   map <int, HNode> newNodes ;
   list<int> newBlackNodes;
   int newId = 0;
   for (auto it = _nodes.begin(); it != _nodes.end(); ++it)
   {
      HNode  & tmpN = it->second;
      list<int> outE = tmpN.getOutEdges();
      list<int> inE = tmpN.getInEdges();

      //========= Update Node
      newNodes.insert( pair< int, HNode> (newId, HNode (tmpN.getName(), newId, tmpN.isBlack(),tmpN.getLBConcentrationValue(), tmpN.getUBConcentrationValue(), outE, inE,tmpN.isBoundary()) ));
      namesNodes.at(tmpN.getName()) = newId;
      if(tmpN.isBlack())
      {
         newBlackNodes.push_front(newId);
      }
      //=======Update Edges
      for( auto outEit = outE.begin(); outEit != outE.end(); ++outEit )
      {
         HEdge &edgeToModify = _edges.at(*outEit);
         list<int>::const_iterator itCompound;
         list<double>::const_iterator itStoich;
         double nodeStoichInThisEdge = 0.0;
         for (itCompound = edgeToModify.getTailNodes().begin(), itStoich = edgeToModify.getTailStoichiometry().begin(); (itCompound != edgeToModify.getTailNodes().end()) && (itStoich != edgeToModify.getTailStoichiometry().end()); ++itCompound, ++itStoich )
         {
            if(*itCompound == tmpN.getId())
            {
               nodeStoichInThisEdge = *itStoich;
               break;
            }
         }
         edgeToModify.removeTailNode(tmpN.getId());
         edgeToModify.addTailNode(newId,nodeStoichInThisEdge);
      }
      for( auto inEit = inE.begin(); inEit != inE.end(); ++inEit )
      {
         HEdge &edgeToModify = _edges.at(*inEit);
         list<int>::const_iterator itCompound;
         list<double>::const_iterator itStoich;
         double nodeStoichInThisEdge = 0.0;
         for (itCompound = edgeToModify.getHeadNodes().begin(), itStoich = edgeToModify.getHeadStoichiometry().begin(); (itCompound != edgeToModify.getHeadNodes().end()) && (itStoich != edgeToModify.getHeadStoichiometry().end()); ++itCompound, ++itStoich )
         {
            if(*itCompound == tmpN.getId())
            {
               nodeStoichInThisEdge = *itStoich;
               break;
            }
         }
         edgeToModify.removeHeadNode(tmpN.getId());
         edgeToModify.addHeadNode(newId,nodeStoichInThisEdge);
      }
      newId++;
   }
   _blackNodes = newBlackNodes;
   _nodes = newNodes;
}





//! Re-index all the edges to go from 0 to their number for their id
void HGraph::reIndexEdges (void)
{
   map <int, HEdge> newEdges ;
   int newId = 0;
   for (auto itE = _edges.begin(); itE != _edges.end(); ++itE)
   {
      HEdge  & tmpE = itE->second;
      list<int> headN = tmpE.getHeadNodes();
      list<int> tailN = tmpE.getTailNodes();
      //========= Update edges
      newEdges.insert(pair<int, HEdge> (newId, HEdge( tmpE.getName(), tmpE.getDescription(), tmpE.getTailNodes(), tmpE.getTailStoichiometry(), tmpE.getHeadNodes(), tmpE.getHeadStoichiometry(), newId)  ));

      //=======Update Node
      for( auto headNit = headN.begin(); headNit != headN.end(); ++headNit )
      {
         HNode& node = _nodes.at(*headNit);
         node.removeInEdge(tmpE.getId());
         node.addInEdge(newId);
      }
      for( auto tailNit = tailN.begin(); tailNit != tailN.end(); ++tailNit )
      {
         HNode& node = _nodes.at(*tailNit);
         node.removeOutEdge(tmpE.getId());
         node.addOutEdge(newId);
      }
      newId++;
   }
   _edges = newEdges;
}

// ===========================================================================
//                                Protected Methods
// ===========================================================================

// ===========================================================================
//                              Non inline accessors
// ===========================================================================


//! fb : Forward bottleneck
/*!
 * Set the node identified by its compounds name to black
 * @param compoundName the name of the Node
 */
void HGraph::setNodeBlack(const string compoundName, const double lbvalue, const double ubvalue)
{
   assert(lbvalue <= ubvalue);
   try
   {
      int idN = namesNodes.at(compoundName);
      _nodes.at(idN).setBlack();
      _nodes.at(idN).setLBDeltaValue(lbvalue);
      _nodes.at(idN).setUBDeltaValue(ubvalue);
      _blackNodes.push_back(idN);
   }
   catch( const out_of_range & oor)
   {
      cout << compoundName << " not present in graph " << &oor << endl;
   }
}
boost::optional<HEdge> HGraph::getEdge( string edgeName) const
{
   boost::optional<HEdge> returnValue;
   for (auto it : this->_edges)
   {
     HEdge e = it.second;
     if (e.getName().compare(edgeName) == 0)
      {
         returnValue = e;
         return returnValue;
      }
   }
   returnValue = boost::none;
   return returnValue;
}
