/*
 * HNode.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: ajulien
 */





// ===========================================================================
//                               Include Libraries
// ===========================================================================



// ===========================================================================
//                             Include Project Files
// ===========================================================================
#include "HNode.h"



// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================




//############################################################################
//                                                                           #
//                               Class HNode.cpp                               #
//                                                                           #
//############################################################################

// ===========================================================================
//                               Static attributes
// ===========================================================================

// ===========================================================================
//                                  Constructors
// ===========================================================================
HNode::HNode( const string & tname, const int & tid)
{
   _name = tname;
   _id = tid;
   _black = false;
}

HNode::HNode( const string & tname, const int & tid, const  bool & ttype, const double lb, const double ub, const list<int> & toutEdges, const  list<int> & tinEdges, const bool isBoundary)
{
   _name = tname;
   _id = tid;
   _black = ttype;
   _outEdges = toutEdges;
   _inEdges = tinEdges;
   _lb = lb;
   _ub = ub;
   _isBoundary = isBoundary;
}


// ===========================================================================
//                                  Destructors
// ===========================================================================
HNode::~HNode(void )
{

}
// ===========================================================================
//                                   Operators
// ===========================================================================
bool HNode::operator==(const int & a) const
      {
   return(  a == _id );
      }

bool HNode::operator==(const string & a) const
      {
   return(  a == _name );
      }

// ===========================================================================
//                                 Public Methods
// ===========================================================================

void HNode::removeOutEdge(  int edge )
{
   _outEdges.remove( edge );
}
void HNode::removeInEdge(  int edge )
{
   _inEdges.remove( edge );
}
void HNode::addOutEdge(  int edge )
{
   _outEdges.push_front( edge );
   _outEdges.unique();
}
void HNode::addInEdge(  int edge )
{
   _inEdges.push_front( edge );
   _inEdges.unique();
}


void HNode::display( void ) const
{
   cout << _id << " node " << _name << "\t in edge: ";
   for(auto it = _inEdges.begin(); it != _inEdges.end(); ++it)
      cout << *it << " ";

   cout << "\t outEgdes";
   for(auto it = _outEdges.begin(); it != _outEdges.end(); ++it)
      cout << *it << " ";
   cout << endl;
}
// ===========================================================================
//                                Protected Methods
// ===========================================================================

// ===========================================================================
//                              Non inline accessors
// ===========================================================================
