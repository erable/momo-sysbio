/*
 * HNode.h
 *
 *  Created on: 30 juin 2014
 *      Author: ajulien
 */

#ifndef HNODE_H_
#define HNODE_H_

// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <list>

// ===========================================================================
//                             Include Project Files
// ===========================================================================

// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================
using namespace std;

class HNode
{
public :

   // =======================================================================
   //                                 Enums
   // =======================================================================

   // =======================================================================
   //                               Constructors
   // =======================================================================
   HNode(const string  & name, const int & id);
   HNode(const string & name, const int & id, const  bool & type, const double lbConcentration, const double ubConcentration, const list<int> & outEdges, const list<int> & inEdges, const bool isBoundary);
   // =======================================================================
   //                               Destructors
   // =======================================================================
   ~HNode( void );
   // =======================================================================
   //                            Accessors: getters
   // =======================================================================
   inline const int getId (void) const;
   //! return name of the node (compounds)
   inline const string getName (void) const;
   //! Reference to the out-edges
   inline const list <int>& getOutEdges (void) const;
   //! Reference to the in-edges
   inline const list <int>& getInEdges (void) const;

   inline const int getOutDegree (void) const;
   inline const int getInDegree (void) const;
   //! type of the node
   inline const bool isBlack (void) const;
   inline const double getLBConcentrationValue (void) const;
   inline const double getUBConcentrationValue (void) const;
   inline const bool isBoundary (void) const;


   // =======================================================================
   //                            Accessors: setters
   // =======================================================================
   inline void setBlack (void);
   inline void setBoundary (bool);
   inline void setLBDeltaValue (double d);
   inline void setUBDeltaValue (double d);

   // =======================================================================
   //                                Operators
   // =======================================================================

   bool operator==(const int & a) const ;
   bool operator==(const string & a) const ;
   // =======================================================================
   //                              Public Methods
   // =======================================================================
   void removeOutEdge( int edge );
   void removeInEdge(  int edge );
   void addOutEdge( int edge );
   void addInEdge(   int edge );

   void display() const;
   // =======================================================================
   //                             Public Attributes
   // =======================================================================



protected :

   // =======================================================================
   //                            Forbidden Constructors
   // =======================================================================
   HNode( void )
{	this->_id = -1;
printf( "%s:%d: error: call to forbidden constructor.\n", __FILE__, __LINE__ );
exit( EXIT_FAILURE );
};

   // =======================================================================
   //                              Protected Methods
   // =======================================================================

   // =======================================================================
   //                             Protected Attributes
   // =======================================================================
   //! Index of the node
   int _id;
   //! Name of the node (metabolites)
   string _name;
   //! Reference to the out-edges -going out the node
   list <int> _outEdges;
   //! Reference to the in-edges - going in the node
   list <int> _inEdges;
   //!type of node: True: Black, False: white
   bool _black = false;
   // The values for both conditions
   double _lb = 0.0;
   double _ub = 0.0;

   bool _isBoundary = false;

};


// ===========================================================================
//                              Getters' definitions
// ===========================================================================
inline const int HNode::getId (void) const
{
   return _id;
}
//! Name of the node (metabolites)
inline const string HNode::getName (void) const
{
   return _name;
}
//! Reference to the out-edges
inline const list <int>& HNode::getOutEdges (void) const
{
   return _outEdges;
}
//! Reference to the in-edges
const list <int> & HNode::getInEdges (void) const
{
   return _inEdges;
}

inline const bool HNode::isBlack (void) const
{
   return _black;
}

inline const double HNode::getLBConcentrationValue (void) const
{
   return _lb;
}

inline const double HNode::getUBConcentrationValue (void) const
{
   return _ub;
}

inline const int HNode::getOutDegree (void) const
{
   return _outEdges.size();
}

inline const int HNode::getInDegree(void) const
{
   return _inEdges.size();
}

inline const bool HNode::isBoundary(void) const
{
   return _isBoundary;
}

// ===========================================================================
//                              Setters' definitions
// ===========================================================================
//!Change type to true (ie: black node)
inline void HNode::setBlack (void)
{
   _black = true;
}

inline void HNode::setBoundary (bool b)
{
   _isBoundary = b;
}

inline void HNode::setLBDeltaValue (double lb)
{
   _lb = lb;
}

inline void HNode::setUBDeltaValue (double ub)
{
   _ub = ub;
}

// ===========================================================================
//                          Inline Operators' definitions
// ===========================================================================

// ===========================================================================
//                          Inline functions' definition
// ===========================================================================


#endif // ___CLASS_H___
