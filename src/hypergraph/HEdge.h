/*
 * HEdge.h
 *
 *  Created on: 30 juin 2014
 *      Author: ajulien
 */

#ifndef HEDGE_H_
#define HEDGE_H_

// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include <cstdio>
#include <cstdlib>
#include <list>
#include <string>
#include <memory>
// ===========================================================================
//                             Include Project Files
// ===========================================================================

// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================
using namespace std;


class HEdge
{
public :

   // =======================================================================
   //                                 Enums
   // =======================================================================

   // =======================================================================
   //                               Constructors
   // =======================================================================
   HEdge( const string nameReactions, const string description, const int idRe );
   HEdge( const string & nameReactions, const string & description, const  list <int>&  _nodesIdOut, const list <double>&  _nodesStoichOut, const list <int> & _nodesIdIn, const list <double>&  _nodesStoichIn, const int & idRe );
   HEdge( const HEdge &model );


   // =======================================================================
   //                               Destructors
   // =======================================================================
   ~HEdge( void );
   // =======================================================================
   //                            Accessors: getters
   // =======================================================================
   int getTailSize( void ) const;
   int getHeadSize( void ) const;

   inline const int & getId( void ) const;
   inline const string & getName( void ) const;
   inline const string & getDescription( void ) const;

   inline const list<int>& getTailNodes() const;
   inline const list<int>& getHeadNodes() const;
   inline const list<double>& getTailStoichiometry() const;
   inline const list<double>& getHeadStoichiometry() const;

   // =======================================================================
   //                            Accessors: setters
   // =======================================================================
   void addTailNode (int newNode, double stoichiometry);
   void addHeadNode (int newNode, double stoichiomery);
   void removeTailNode (int toRemoveNode);
   void removeHeadNode (int toRemoveNode);

   // =======================================================================
   //                                Operators
   // =======================================================================
   inline void addLabel( string label);

   bool operator==(const HEdge & a) const ;
   bool operator<(const HEdge & a) const;
   HEdge operator+(const HEdge & a);
   HEdge& operator=(const HEdge&);

   // =======================================================================
   //                              Public Methods
   // =======================================================================
   void display( void ) const;
   string to_str( void ) const;

   // =======================================================================
   //                             Public Attributes
   // =======================================================================

protected :

   // =======================================================================
   //                            Forbidden Constructors
   // =======================================================================
   HEdge( void ): id(-1)
{
      printf( "%s:%d: error: call to forbidden constructor.\n", __FILE__, __LINE__ );
      exit( EXIT_FAILURE );
};



   // =======================================================================
   //                              Protected Methods
   // =======================================================================

   // =======================================================================
   //                             Protected Attributes
   // =======================================================================
   //!Id of the reaction
   const int id; // unique, should never changed!
   //!Name of the reaction (could be removed)
   string name;
   //Name/description
   string desc;
   //!Out going nodes, the head
   list <int> nodesHead;
   //!In going nodes the tail
   list <int> nodesTail;
   //Stoichiometry for the head nodes
   list<double> stoichHead;
   //Stoichiometry for the tail nodes
   list<double> stoichTail;

};



// ===========================================================================
//                              Getters' definitions
// ===========================================================================
inline const string & HEdge::getName(void) const
{
   return name;
}

inline const string & HEdge::getDescription(void) const
{
   return desc;
}
inline const int & HEdge::getId(void) const
{
   return id;
}
// ===========================================================================
//                              Setters' definitions
// ===========================================================================

// ===========================================================================
//                          Inline Operators' definitions
// ===========================================================================
// Add a label to the name of the edge (will be used in compression)
inline void HEdge::addLabel( string label )
{
   name += label;
}
inline const list<int>& HEdge::getTailNodes() const
{
   return nodesTail;
}
inline const list<int>& HEdge::getHeadNodes() const
{
   return nodesHead;
}
inline const list<double>& HEdge::getTailStoichiometry() const
{
   return stoichTail;
}
inline const list<double>& HEdge::getHeadStoichiometry() const
{
   return stoichHead;
}
// ===========================================================================
//                          Inline functions' definition
// ===========================================================================



#endif /* HEDGE_H_ */
