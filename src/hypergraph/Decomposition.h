#ifndef Decomposition
#define Decomposition


// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include <cstdio>
#include <cstdlib>
#include <vector>


// ===========================================================================
//                             Include Project Files
// ===========================================================================
#include "HGraph.h"


// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================


using namespace std ;
// ===========================================================================
//                          Declare functions
// ===========================================================================

//!Will explore the graph
void explore (HGraph & h,  vector<bool> & visitedN, int v, vector<bool> & followedE);

list<HGraph> dfsDecomposition (HGraph & h);
//!Extract subgraph
HGraph extractSub( HGraph & H, vector<bool> & visited);



#endif // Decomposition
