// ===========================================================================
//                               Include Libraries
// ===========================================================================



// ===========================================================================
//                             Include Project Files
// ===========================================================================
#include "Decomposition.h"



// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================




// ===========================================================================
//                             Functions
// ===========================================================================
//!Will explore the graph
/*! Depth-first search recursive
 *��\brief Explore all nodes reachable from v
 * @param h :  hypergraph being traverse
 * @param visited : list
 * @param v
 */
void explore (HGraph & h,  vector<bool> & visitedN, int v, vector<bool> & followedE) // watchout decay
{

   visitedN[v] = true;
   //=== Try OUt Edge
   list<int> outEdges = h.getNode(v).getOutEdges();
   for(auto it = outEdges.begin(); it != outEdges.end(); ++it)
   {
      if( ! followedE[*it] ) // we did not handled this edge yet
      {
         followedE.at(*it) = true;
         HEdge beingUsed = h.getEdge(*it);
         // visiting
         list<int> headNodes = beingUsed.getHeadNodes();
         for(auto itNHead = headNodes.begin(); itNHead != headNodes.end(); ++itNHead)
         {
            if(! visitedN[*itNHead] )
            {
               explore(h, visitedN, *itNHead, followedE); // try to explore from h
            }
         }
         list<int> tailNodes = beingUsed.getTailNodes();
         for(auto itNTail = tailNodes.begin(); itNTail != tailNodes.end(); ++itNTail)
         {
            if( !  visitedN[*itNTail] )
            {
               explore( h, visitedN, *itNTail, followedE);
            }
         }
      }
   }
   //=== Try IN edges
   list<int> inEdges = h.getNode(v).getInEdges();
   for(auto it = inEdges.begin(); it != inEdges.end(); ++it)
   {
      if( ! followedE[*it] ) // we did not handled this edge yet
      {
         followedE.at(*it) = true;
         HEdge beingUsed = h.getEdge(*it);
         // visiting
         list<int> headNodes = beingUsed.getHeadNodes();
         for(auto itNHead = headNodes.begin(); itNHead != headNodes.end(); ++itNHead)
         {
            if(! visitedN[*itNHead] )
            {
               explore( h , visitedN, *itNHead, followedE); // try to explore from h
            }
         }
         list<int> tailNodes = beingUsed.getTailNodes();
         for(auto itNTail = tailNodes.begin(); itNTail != tailNodes.end(); ++itNTail)
         {
            if( !  visitedN[*itNTail] )
            {
               explore( h, visitedN, *itNTail, followedE);
            }
         }
      }
   }
}


//!Extract subgraph
/*!
 *\brief Decompose h into its connected components using a dfs algorithm
 *\param h : the hypergraph to decompose
 *\return the list of modules including only blackNodes
 */
list<HGraph> dfsDecomposition (HGraph &  h)
                  {
   list <HGraph> connectedParts;
   while (! (h.getBlackNodes().empty())) // all bn are in a component
   {
      //======= initializing
      int nbNodes = h.getNbNodes();
      vector<bool> visited (nbNodes, false);

      int nbEdges = h.getNbEdges();
      vector<bool> followedE (nbEdges, false);

      // end initialisation
      std::cout << "i";
      //===========do dfs
      explore(h, visited, h.getBlackNodes().front(), followedE );
      cout << "finished explore!"<< endl;
      //=========== extract sub & modify h
      connectedParts.push_front( extractSub(h, visited)) ;
   }
   return connectedParts;
                  }


// From h, remove nodes where visited is true, and create a new one with only those ones.
HGraph extractSub( HGraph & h, vector<bool> &  visited)
{
   int nbNodes = h.getNbNodes();
   HGraph newH = HGraph(h);
   // Updating h
   for(int i = 0; i != nbNodes; i++ )
   {
      if(visited[i])
      {
         h.removeNode(i);
      }
      else
      {
         newH.removeNode(i);
      }
   }
   h.reIndexNodes();
   newH.reIndexNodes();
   return newH;
}

