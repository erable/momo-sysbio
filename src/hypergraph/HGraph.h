/*
 * HGraph.h
 *
 *  Created on: 30 juin 2014
 *      Author: ajulien
 */

#ifndef HGRAPH_H_
#define HGRAPH_H_

using namespace std ;

// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <boost/optional.hpp>

// ===========================================================================
//                             Include Project Files
// ===========================================================================
#include "HNode.h"
#include "HEdge.h"

// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================



class HGraph
{
public :

   // =======================================================================
   //                                 Enums
   // =======================================================================

   // =======================================================================
   //                               Constructors
   // =======================================================================
   HGraph(void);
   HGraph (const HGraph &model);


   // =======================================================================
   //                               Destructors
   // =======================================================================
   ~HGraph (void);
   // =======================================================================
   //                            Accessors: getters
   // =======================================================================
   inline const int & getNbNodes( void ) const;
   inline const map<int,HNode>& getNodes( void ) const;
   inline const int & getNbEdges( void ) const;
   inline const map<int,HEdge>& getEdges( void ) const;
   inline const list <int>& getBlackNodes( void ) const;
   inline const HNode & getNode( int id ) const;
   inline const HEdge & getEdge( int id ) const;

   boost::optional<HNode> getNode( string compoundName) const;
   boost::optional<HEdge> getEdge( string edgeName) const;

   // =======================================================================
   //                            Accessors: setters
   // =======================================================================
   void setNodeBlack(const string compoundName, const double lbvalue, const double ubvalue);
   void addNode( HNode & node );
   void addNode( string name );
   void addNode( string name, bool isBoundary );
   // will be used during file"s lecture (filling the graph)
   void addEdge( string nameEdge, string description, list<string> substrates, list<double> substratesStoich, list<string> products,  list<double> productsStoich);

   void removeEdge (const int & idE);

   void removeNode(const int & id);
   // =======================================================================
   //                                Operators
   // =======================================================================


   // =======================================================================
   //                              Public Methods
   // =======================================================================
   void display( void ) const;
   void reIndex (void);
   void reIndexNodes (void);
   void reIndexEdges (void);

   // =======================================================================
   //                             Public Attributes
   // =======================================================================



protected :

   // =======================================================================
   //                            Forbidden Constructors
   // =======================================================================


   // =======================================================================
   //                              Protected Methods
   // =======================================================================

   // =======================================================================
   //                             Protected Attributes
   // =======================================================================
   //!Number of Nodes
   int _nbNodes;
   //!Number of Edges
   int _nbEdges;
   //! Map with key: compound Name, value: id of the compound (integer)
   map <string, int> namesNodes;
   //! Map with key: compound id (integer), value: HNode of the graph  (HNode)
   map <int, HNode> _nodes;
   //! Map with key: edge id (integer), value: Edge in the graph (HEdge)
   map <int, HEdge> _edges;
   //!Black Nodes
   list<int> _blackNodes;
};


// ===========================================================================
//                              Getters' definitions
// ===========================================================================

// ===========================================================================
//                              Setters' definitions
// ===========================================================================

// ===========================================================================
//                          Inline Operators' definitions
// ===========================================================================
const int & HGraph::getNbNodes ( void ) const
{
   return _nbNodes;
}

const map <int,HNode> & HGraph::getNodes( void ) const
{
   return _nodes;
}

const int & HGraph::getNbEdges ( void ) const
{
   return _nbEdges;
}

const map <int,HEdge>& HGraph::getEdges( void ) const
{
   return _edges;
}


const list <int>& HGraph::getBlackNodes( void ) const
{
   /*for(const int & blackid: _blackNodes)
  {
    cout << blackid << "\t";
  }*/
   return _blackNodes;
}

const HNode & HGraph::getNode( int id ) const
{
   return _nodes.at(id);
}

const HEdge & HGraph::getEdge( int id ) const
{
   return _edges.at(id);
}

// ===========================================================================
//                          Inline functions' definition
// ===========================================================================

#endif /* HGRAPH_H_ */
